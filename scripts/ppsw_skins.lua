local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	jellyfishp = {
		odd = {
			name = "odd",
			fname = "Odd Jellyfish",
			build = "jellyfish_shiny_build_01",
		},
		val = {
			name = "val",
			fname = "Valentine Jellyfish",
			build = "jellyfish_valentines",
		},
	},
	snakep = {
		odd = {
			name = "odd",
			fname = "Odd Snake",
			build = "snake_shiny_build_01",
		},
		pirate = {
			name = "pirate",
			fname = "Pirate Snake",
			build = "snake_shiny_build_02",
			scale = 1.2,
		},
	},
	snake_poisonp = {
		odd = {
			name = "odd",
			fname = "Odd Snake",
			build = "snake_yellow_shiny_build_01",
		},
		pirate = {
			name = "pirate",
			fname = "Pirate Snake",
			build = "snake_yellow_shiny_build_02",
			scale = 1.2,
		},
	},
	doydoyp = {
		odd = {
			name = "odd",
			fname = "Odd DoyDoy",
			build = "doydoy_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Doy Doy",
			build = "doydoy_adult_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	flupp = {
		odd = {
			name = "odd",
			fname = "Odd Flup",
			build = "flup_shiny_build_01",
		},
	},
	palmtreep = {
		odd = {
			name = "odd",
			fname = "Odd Treeguard",
			build = "treeguard_build",
			hue = 0.5,
		},
	},
	jungletreeguardp = {
		odd = {
			name = "odd",
			fname = "Odd Treeguard",
			build = "jungletreeguard_build",
			hue = 0.5,
		},
		blue = {
			name = "blue",
			fname = "Blue Treeguard",
			build = "jungletreeguard_build",
			hue = 0.3,
		},
		pink = {
			name = "pink",
			fname = "Pink Treeguard",
			build = "jungletreeguard_build",
			hue = 0.6,
		},
	},
	cactusp = {
		odd = {
			name = "odd",
			fname = "Odd Elephant Cactus",
			build = "cactus_volcano_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Elephant Cactus",
			build = "cactus_volcano_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ballphinp = {
		odd = {
			name = "odd",
			fname = "Odd Ballphin",
			build = "ballphin_shiny_build_01",
		},
	},
	merm2p = {
		odd = {
			name = "odd",
			fname = "Odd Fisher Merm",
			build = "merm_fisher_shiny_build_01",
		},
	},
	hogp = {
		odd = {
			name = "odd",
			fname = "Odd Hog",
			build = "wildbore_shiny_build_01",
		},
	},
	apep = {
		odd = {
			name = "odd",
			fname = "Odd Ape",
			build = "junglekiki_shiny_build_01",
		},
	},
	crabbitp = {
		odd = {
			name = "odd",
			fname = "Odd Crabbit",
			build = "crabbit_shiny_build_01",
		},
	},
	sharxp = {
		odd = {
			name = "odd",
			fname = "Odd Sea Hound",
			build = "sharx_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Sea Hound",
			build = "sharx_shiny_build_03",
		},
	},
	sharkittenp = {
		odd = {
			name = "odd",
			fname = "Odd Tigershark",
			build = "sharkitten_shiny_build_01",
			adult_build = "tigershark_shiny_build_01",
			adult_build2 = "tigershark_water_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Sharkitten",
			build = "sharkitten_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	tigersharkp = {
		odd = {
			name = "odd",
			fname = "Odd Tigershark",
			build = "tigershark_shiny_build_01",
			build2 = "tigershark_water_shiny_build_01",
		},
		rf = {
			name = "rf",
			fname = "Reforged Tigershark",
			build = "tigershark_shiny_build_08",
			build2 = "tigershark_water_shiny_build_08",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Tigershark",
			build = "tigershark_build",
			build2 = "tigershark_water",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	
	twisterp = {
		odd = {
			name = "odd",
			fname = "Odd Twister",
			build = "twister_shiny_build_01",
			build2 = "",
		},
	},
	
}

return SKINS