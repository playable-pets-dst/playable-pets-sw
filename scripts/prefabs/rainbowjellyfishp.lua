local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/rainbowjellyfish.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

-----------------------
--Stats--
local mob = 
{
	health = 100,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 20,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "rainbowjellyfish",
	build = "rainbowjellyfish",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGjellyfishp",
	minimap = "jellyfishp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
if SW_PP_MODE == true then
	SetSharedLootTable('rainbowjellyfishp',
	-----Prefab---------------------Chance------------
	{
		{'jellyfish_dead',             1.00}, 
   
	})
else
	SetSharedLootTable('rainbowjellyfishp',
	-----Prefab---------------------Chance------------
	{
		{'fish',			1.00},  
	})
end	

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker and data.attacker.components.combat and data.attacker.components.health and not data.attacker.components.health:IsDead() and inst:IsNear(data.attacker, mob.hit_range) then
	   data.attacker.components.combat:GetAttacked(inst, (TheNet:GetServerGameMode() == "lavaarena" and data.damage) and data.damage/2 or 15, nil, "electric")
    end
end

local function playshockanim(inst)
    if inst:HasTag("aquatic") then
        inst.AnimState:PlayAnimation("idle_water_shock")
        inst.AnimState:PushAnimation("idle_water", true)
        inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/jellyfish/electric_water")
    else
        inst.AnimState:PlayAnimation("idle_ground_shock")
        inst.AnimState:PushAnimation("idle_ground", true)
        inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/jellyfish/electric_land")
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

--rainbow light--
local INTENSITY = 0.65

local function swapColor(inst, Light)
	if inst.ispink then
		inst.ispink = false
		inst.isgreen = true
		inst.components.lighttweener:StartTween(Light, nil, nil, nil, {0/255, 180/255, 255/255}, 4, swapColor)
	elseif inst.isgreen then
		inst.isgreen = false
		inst.components.lighttweener:StartTween(Light, nil, nil, nil, {240/255, 230/255, 100/255}, 4, swapColor)
	else
		inst.ispink = true
		inst.components.lighttweener:StartTween(Light, nil, nil, nil, {251/255, 30/255, 30/255}, 4, swapColor)
	end
end

local function turnon(inst)
	
	if inst.Light and not inst.hidden then	
		inst.Light:Enable(true)
		local secs = 1+math.random()
		inst.components.lighttweener:StartTween(inst.Light, 0, nil, nil, nil, 0)
		inst.components.lighttweener:StartTween(inst.Light, INTENSITY, nil, nil, nil, secs, swapColor)		
	end
end


local function turnoff(inst)
	if inst.Light then
		inst.Light:Enable(false)
	end
end
--

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 30, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('rainbowjellyfishp')
	----------------------------------
	--Tags--
	inst.mobplayer = true --lets mod know that you're a mob.
	inst:AddTag("jellyfish")
	inst:AddTag("aquatic")
	inst.mobsleep = true
	inst.taunt = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "sharx_fin"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--SanityAura--
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, 1)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, false, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------	
	--Light and Character Functions. Don't Touch.--
    	
  	inst:AddComponent("lighttweener")
	local Light = inst.entity:AddLight()
	inst.Light:SetColour(251/255, 30/255, 30/255)
	inst.Light:Enable(true)
	inst.Light:SetIntensity(0.65)
	inst.Light:SetRadius(1.5)
	inst.Light:SetFalloff(.45)
	
	inst.ispink = true
	inst.components.lighttweener:StartTween(Light, nil, nil, nil, {0/255, 180/255, 255/255}, 4, swapColor)
	
	inst.components.grue:AddImmunity("mobplayer")

    ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
  	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true)
			inst:AddComponent("lighttweener")
			local Light = inst.entity:AddLight()
			inst.Light:SetColour(251/255, 30/255, 30/255)
			inst.Light:Enable(true)
			inst.Light:SetIntensity(0.65)
			inst.Light:SetRadius(1.5)
			inst.Light:SetFalloff(.45)
	
			inst.ispink = true
			inst.components.lighttweener:StartTween(Light, nil, nil, nil, {0/255, 180/255, 255/255}, 4, swapColor)

			end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("rainbowjellyfishp", prefabs, assets, common_postinit, master_postinit, start_inv)
