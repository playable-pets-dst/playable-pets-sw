--Note: this is NOT a playable prefab!

local assets =
{
    Asset("ANIM", "anim/quacken_tentacle.zip"),
	
    Asset("SOUND", "sound/tentacle.fsb"),
}

local prefabs =
{
    "monstermeat",
    "tentaclespike",
    "tentaclespots",
}

SetSharedLootTable('quaken_tentacle_p',
{
    {'tentaclespots', 0.10},
    {'tentaclespike', 0.05},
	{'meat', 1.00},
	{'meat', 1.00},
	{'meat', 1.00},
})

local function onupdate(inst)
	if inst.setbehaviour == 1 then
		if inst.minionlord == nil and not inst.sg:HasStateTag("busy") then
			--print(inst.minionlord)
			inst.sg:GoToState("despawn")
		end	
	end
end

local function onnewcombattarget(inst, data)
    if data.target and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("hit") and not inst.sg:HasStateTag("busy") and not inst.components.health:IsDead() then
        inst.sg:GoToState("attack")
    end
end

local function checkmaster(tar, inst)
    if inst.minionlord then
        return tar == inst.minionlord
    end
	
    if tar.minionlord and inst.minionlord then
        return tar.minionlord == inst.minionlord
    else
        return false
    end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	end
end

local function retargetfn(inst)
    return FindEntity(
        inst,
        5,
        function(guy) 
            return guy.prefab ~= inst.prefab and guy.prefab ~= "quackenp"
                and guy.entity:IsVisible()
                and not guy.components.health:IsDead()
				and not checkmaster(guy, inst)
                and (guy.components.combat.target == inst or
					guy:HasTag("player") or
                    guy:HasTag("character") or
                    guy:HasTag("monster") or
                    guy:HasTag("animal"))
        end,
        { "_combat", "_health" },
        GetExcludeTagsp(inst))
end

local function shouldKeepTarget(inst, target)
    return target ~= nil
        and target:IsValid()
        and target.entity:IsVisible()
        and target.components.health ~= nil
        and not target.components.health:IsDead()
        and target:IsNear(inst, 10)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddPhysics()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	--inst.isshiny = 0
	
	if inst.minionlord and inst.minionlord.isshiny ~= nil then
		inst.isshiny = inst.minionlord.isshiny
	end
	
	
	if inst.isshiny ~= nil and inst.isshiny > 0 then
		inst.AnimState:SetBank("quacken_tentacle")
		inst.AnimState:SetBuild("quacken_tentacle_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBank("quacken_tentacle")
		inst.AnimState:SetBuild("quacken_tentacle")
	end
    inst.AnimState:PlayAnimation("enter", true)

	MakeWaterObstaclePhysics(inst, 0.80, 2, 1.25)

    inst:AddTag("tentacle")
    inst:AddTag("monster")
    inst:AddTag("epic")
    inst:AddTag("hostile")
	inst:AddTag("healthinfo") --compability with health info mod.

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	
    inst._last_attacker = nil
    inst._last_attacked_time = nil

	inst.setbehaviour = 1 
	--2 is auto-attack until Quacken dies or goes underwater.
	--1 is auto-attack until it loses its target or the target is dead.
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(400)

    inst:AddComponent("combat")
    inst.components.combat:SetRange(5)
	--inst.components.combat:SetAreaDamage(8, 1)
    inst.components.combat:SetDefaultDamage(100 * MOBPVP)
    inst.components.combat:SetAttackPeriod(3)
    inst.components.combat:SetRetargetFunction(0.2, retargetfn)
    inst.components.combat:SetKeepTargetFunction(shouldKeepTarget)
	
	inst:ListenForEvent("newcombattarget", onnewcombattarget)

    --MakeLargeFreezableCharacter(inst)

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

    inst:AddComponent("inspectable")
    inst:AddComponent("lootdropper")
	inst:AddComponent("entitytracker")
    inst.components.lootdropper:SetChanceLootTable('quacken_tentacle_p')

	inst:AddComponent("locomotor")
	inst.components.locomotor:SetSlowMultiplier( 1 )
    inst.components.locomotor:SetTriggersCreep(false)
    inst.components.locomotor.pathcaps = { ignorecreep = true }
    inst.components.locomotor.walkspeed = 0
	inst.components.locomotor.runspeed = 0
	
	inst:DoPeriodicTask(3, onupdate)
	--onupdate(inst, 0)
	
	if inst.isshiny ~= nil and inst.isshiny > 0 then
		inst.AnimState:SetBank("quacken_tentacle")
		inst.AnimState:SetBuild("quacken_tentacle_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBank("quacken_tentacle")
		inst.AnimState:SetBuild("quacken_tentacle")
	end
	
    inst:SetStateGraph("SGquacken_tentacle_p")
	
	
	inst:DoTaskInTime(0, function(inst)
	if SW_PP_MODE ~= nil and SW_PP_MODE == true then
		if not IsOnWater(inst:GetPosition():Get()) then
			print("REMOVING TENTACLE")
			inst:Hide()
			inst.sg:GoToState("despawn")
		end	
	end
	end)

    --inst:ListenForEvent("attacked", OnAttacked)
	local brain = require("brains/krakententaclebrain")
    inst:SetBrain(brain)

    return inst
end

return Prefab("quacken_tentacle_p", fn, assets, prefabs)