local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------


local assets = 
{
	Asset("ANIM", "anim/kiki2_basic.zip"),
	Asset("SOUND", "sound/monkey.fsb"),
	Asset(	"ANIM", "anim/junglekiki_build.zip"),
	
}

getskins = {"1"}

local prefabs = 
{	

}
		
local start_inv = 
{
	"swmonster_wpn",
	"monkeyhouse2",
}
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 175,
	runspeed = 7,
	walkspeed = 7,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hit_range = 2,
	bank = "kiki",
	build = "junglekiki_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGapep",
	minimap = "apep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('apep',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00},   
})

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("monkey") and not dude.components.health:IsDead() end, 3)
    end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.APE)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

----Monkey only functions
local function DoFx(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")

    local x, y, z = inst.Transform:GetWorldPosition()
    local fx = SpawnPrefab("statue_transition_2")
    if fx ~= nil then
        fx.Transform:SetPosition(x, y, z)
        fx.Transform:SetScale(.8, .8, .8)
    end
    fx = SpawnPrefab("statue_transition")
    if fx ~= nil then
        fx.Transform:SetPosition(x, y, z)
        fx.Transform:SetScale(.8, .8, .8)
    end
end

local function IsMonsterWeapon(item)
	return item.prefab == "swmonster_wpn"
end

local function OnDrop(inst)
    local monsterwpn = inst.components.inventory:FindItem(IsMonsterWeapon)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	if monsterwpn == nil and not inst.components.inventory:IsFull() and not inst:HasTag("playerghost") then
	--inst:DoTaskInTime(1, function (inst)
	 if hands ~= nil and hands:HasTag("monsterwpn") then
	 --
	 else
		
		inst:DoTaskInTime(1, function(inst) 
		inst.components.inventory:GiveItem(SpawnPrefab("swmonster_wpn"))
		end)
	end	
	end
end

local function OnDropCheck(inst)
    
	inst:DoTaskInTime(1, function(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	if hands == nil or not hands:HasTag("monsterwpn") then
	OnDrop(inst)
	end
	end)
end
-------

local function IsPoop(item)
    return item.prefab == "poop"
end

local function onthrow(weapon, inst)
    if inst.components.inventory ~= nil and inst.components.inventory:FindItem(IsPoop) ~= nil then
        inst.components.inventory:ConsumeByName("poop", 1)
    end
end

local function hasammo(inst)
    return inst.components.inventory ~= nil and inst.components.inventory:FindItem(IsPoop) ~= nil
end

local function CheckAmmo(inst)
	if inst.HasAmmo ~= nil then
	inst.components.combat:SetRange(TUNING.MONKEY_RANGED_RANGE)
	
	elseif inst.HasAmmo == nil then
	inst.components.combat:SetRange(TUNING.MONKEY_MELEE_RANGE)
	--inst:RemoveComponent("weapon")
	end	
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local function OnEat(inst)
    --Eating food might cause pooping!
    if inst.components.inventory ~= nil then
        local maxpoop = 20
		local pooper = math.random()
        local poopstack = inst.components.inventory:FindItem(IsPoop)
        if poopstack == nil or poopstack.components.stackable.stacksize < maxpoop then
			if math.random() < 0.33 then
            inst.components.inventory:GiveItem(SpawnPrefab("poop"))
			end
        end
    end
end

local function Equip(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD) or nil
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
	if hands ~= nil and hands.components.weapon and hands:HasTag("monsterwpn") then
		hands.components.weapon:SetDamage(20*2)
        hands.components.weapon:SetRange(TUNING.MONKEY_RANGED_RANGE)
        hands.components.weapon:SetProjectile("monkeyprojectile")
        hands.components.weapon:SetOnProjectileLaunch(onthrow)
	end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.APE)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	inst.components.inventory:Equip(SpawnPrefab("poopweaponsw_forge"))
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"
	inst.soundtype = ""

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() --Disabled because might cause sliding. Remove this if I never slide again.
		if ThePlayer then
			ThePlayer:EnableMovementPrediction(false)
		end
	end)
	
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('apep')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("monster_user")
    inst:AddTag("monkey")
	inst:AddTag("animal")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.monster_user = true
	inst.isshiny = 0
	inst.getskins = getskins

	inst.soundtype = "dontstarve_DLC002/creatures/monkey_island"
	inst.HasAmmo = hasammo
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), VEGGIE, GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.GENERIC, FOODTYPE.VEGGIE }, { FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.25,1) --This might multiply food stats.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, .25)
    inst.DynamicShadow:SetSize(2, 1.25)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("equip", Equip) 
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("itemlose", OnDropCheck)
	---------------------------------
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("apep", prefabs, assets, common_postinit, master_postinit, start_inv)
