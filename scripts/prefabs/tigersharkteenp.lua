local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
	Asset("ANIM", "anim/tigershark_build.zip"),
    Asset("ANIM", "anim/tigershark_ground_build.zip"),
	Asset("ANIM", "anim/tigershark_ground.zip"),
    Asset("ANIM", "anim/tigershark_water_build.zip"),
	Asset("ANIM", "anim/tigershark_water.zip"),
    Asset("ANIM", "anim/tigershark.zip"),
	Asset("ANIM", "anim/tigershark_water_ripples_build.zip"),
}
local prefabs = {	
    }

--This doesn't matter as you shouldn't spawn as a Teenshark.	
local start_inv = {
	--"spear",
}

SetSharedLootTable('tigersharkteenp',
{
    {"meat", 1.00},
	{"meat", 1.00},
	{"meat", 1.00},
	{"meat", 1.00},
    
})

local function GetTarget(inst)
    --Used for logic of moving between land and water states
    local target = inst.components.combat.target and inst.components.combat.target:GetPosition()

    if not target and inst:GetBufferedAction() then
        target = (inst:GetBufferedAction().target and inst:GetBufferedAction().target:GetPosition()) or inst:GetBufferedAction().pos
    end

    --Returns a position
    return target
end

local function RestoreNightImmunity(inst)

	inst:DoTaskInTime(3, function(inst) 
	inst.Light:Enable(true)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(.5)
	inst.Light:SetIntensity(.6)
	inst.Light:SetColour(245/255,40/255,0/255)
	end, inst)
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns heals to max health upon reviving
end


local function setChar(inst)
    inst.AnimState:SetBank("tigershark")
    inst.AnimState:SetBuild("tigershark_ground_build")
    inst:SetStateGraph("SGtigersharkp")
	inst.components.hunger:Pause()
	inst.components.sanity.ignore = true
end

local NIGHTVISION_COLOURCUBES =
{
    day = "images/colour_cubes/insane_day_cc.tex",
    dusk = "images/colour_cubes/insane_dusk_cc.tex",
    night = "images/colour_cubes/purple_moon_cc.tex",
    full_moon = "images/colour_cubes/purple_moon_cc.tex",
}

local function SetNightVision(inst, enable)
    if TheWorld.state.isnight or TheWorld:HasTag("cave") then
        inst.components.playervision:ForceNightVision(true)
        inst.components.playervision:SetCustomCCTable(NIGHTVISION_COLOURCUBES)
    else
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)
    end
end

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function oncollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local common_postinit = function(inst) 
	inst.soundsname = "wilson"

	inst.MiniMapEntity:SetIcon( "tigersharkp.tex" )

	inst:DoTaskInTime(0, function()
   if ThePlayer then
      ThePlayer:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	
	SetNightVision(inst, true)
end

local function OnHitOther(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if hands and hands.components.finiteuses then
		hands.components.finiteuses:SetPercent(1)
	end 
end

local function Equip(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if hands then
	inst.sg:GoToState("taunt")
	inst:AddTag("runner")
	inst.components.combat:SetRange(4,4)
	end
	
end

local function UnEquip(inst)
    	--inst.sg:GoToState("matingcall")
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if hands then
	--inst.sg:GoToState("shake")
	else
	inst:RemoveTag("runner")
	--inst.components.combat:SetRange(40,8)
	--inst.components.combat:SetRange(4,4)
	--hands:AddComponent("finiteuses")
	end 
end	

local function SetWeaponDamage(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if hands.components.weapon then
	hands.components.weapon:SetDamage(75)
	--hands:RemoveComponent("finiteuses")
	end


end	

local function DontTriggerCreep(inst)
	inst.components.locomotor.walkspeed = (11)
    inst.components.locomotor.runspeed = (12)
	--inst.components.locomotor:SetSlowMultiplier( 1 )
    --inst.components.locomotor:SetTriggersCreep(false)
    --inst.components.locomotor.pathcaps = { ignorecreep = true }
end


local master_postinit = function(inst) 
    inst.components.health:SetMaxHealth(750)
	inst.Physics:SetCollisionCallback(oncollide)
	inst:AddComponent("healthRegenerate1")
	inst.components.combat.playerdamagepercent = .5
    inst.components.combat:SetAttackPeriod(3)
	inst.components.combat:SetRange(4,4)
	inst.components.locomotor.walkspeed = (5)
    inst.components.locomotor.runspeed = (10)
	inst.components.combat:SetDefaultDamage(125)
	--inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hit")
	--inst.components.temperature.hurtrate = 0
    inst.components.temperature.inherentinsulation = -TUNING.INSULATION_SMALL
	inst.AnimState:SetBank("tigershark")
	inst.AnimState:SetBuild("tigershark_ground_build")	
	inst.OnSetSkin = function(skin_name)
    inst.AnimState:SetBuild("tigershark_ground_build")
	inst:SetStateGraph("SGtigersharkp")
	
	inst:ListenForEvent("ms_respawnedfromghost", DontTriggerCreep)
    DontTriggerCreep(inst)

	
	end
	inst:SetStateGraph("SGtigersharkp")
	
	--inst:AddComponent("locomotor")
    --inst.components.locomotor:SetSlowMultiplier( 1 )
    --inst.components.locomotor:SetTriggersCreep(false)
    --inst.components.locomotor.pathcaps = { ignorecreep = true }
	
	
	inst.components.locomotor:SetShouldRun(true)
	inst.components.talker:IgnoreAll()
	inst.components.hunger:Pause()
	inst.components.sanity.ignore = true
	--inst.HUD.controls.status.stomach:Hide()
	
	inst:ListenForEvent("onhitother", OnHitOther)
	
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('tigersharkp')
	--inst.components.health.fire_damage_scale = 0 -- Take no damage from fire 
	
	 
	--inst:AddTag("aquatic")
    inst:AddTag("scarytoprey")
	inst:AddTag("monster")
	inst.mobplayer = true
    inst:AddTag("tigershark")
    inst:AddTag("largecreature")
	
	--Not even sure these matter but putting it here anyways just incase.
	inst.taunt = true
	inst.canrage = true
	
	inst.GetTarget = GetTarget
	
	--inst:ListenForEvent("attacked", OnAttacked)
	
	MakeCharacterPhysics(inst, 1000, 1.33)
    inst.DynamicShadow:SetSize( 6, 3 )
    inst.DynamicShadow:Enable(false)
    inst.Transform:SetFourFaced()
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetCanEatHorrible()
	
	inst.components.eater.strongstomach = true -- can eat monster meat!
	
	inst.components.temperature.maxtemp = 60
	inst.components.temperature.mintemp = 20
	inst.components.moisture:SetInherentWaterproofness(1)
	
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 3
    inst.components.groundpounder.destructionRings = 1
    inst.components.groundpounder.numRings = 3
    --inst.components.groundpounder.noTags = {"sharkitten"}
	
	inst:ListenForEvent("equip", Equip)
	inst:ListenForEvent("equip", SetWeaponDamage)
	inst:ListenForEvent("unequip", UnEquip)
    
	
	------------------
    
    ------------------

	
  	local light = inst.entity:AddLight() --Can't see at night but is immune to Grue
  	inst.Light:Enable(true)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.6)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,255/255)
	
	--No nightvision, makes a need for lightsources during the night to work.
  	--inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	
	--SetNightVision(inst, true)
	
	inst.components.talker.colour = Vector3(127/255, 0/255, 0/255)
	inst:ListenForEvent("respawnfromghost", RestoreNightImmunity)
	
	

    inst:DoTaskInTime(0, setChar)
    inst:ListenForEvent("respawnfromghost", function()
        inst:DoTaskInTime(5, setChar)
		inst:DoTaskInTime(6, DontTriggerCreep)
		inst:DoTaskInTime(6, RemovePenalty)
		inst:DoTaskInTime(10, RestoreNightImmunity)
    end)
	
	
    return inst
	
end




return MakePlayerCharacter("tigersharkp", prefabs, assets, common_postinit, master_postinit, start_inv)
