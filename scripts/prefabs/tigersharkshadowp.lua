local easing = require("easing")

local assets =
{
	Asset("ANIM", "anim/tigershark_shadow.zip")
}

local prefabs = {}

local function LerpIn(inst)
	local s = easing.inExpo( inst:GetTimeAlive(), inst.StartingScale, 1 - inst.StartingScale, inst.TimeToImpact)
	inst.Transform:SetScale(s,s,s)
	if s <= 1 then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end
end

local function LerpOut(inst)
	local s = easing.inExpo( inst:GetTimeAlive(), 0.3, 1, inst.TimeToImpact)
	inst.Transform:SetScale(s,s,s)
	if s >= inst.StartingScale then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end
end

local function SetUpScale(inst, start_scale, end_scale, scale_time, fade_type, ease_type)
	ease_type = ease_type or easing.inExpo
	local start_time = GetTime()
	inst.Transform:SetScale(start_scale,start_scale,start_scale)

	if inst.sizeTask then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end

	inst.sizeTask = inst:DoPeriodicTask(FRAMES, function(inst)
		local scale = ease_type(GetTime() - start_time, start_scale, end_scale - start_scale, scale_time)
		inst.Transform:SetScale(scale, scale, scale)
	end)

	if fade_type == "OUT" then
		inst.AnimState:SetMultColour(0,0,0,0.6)
		inst.components.colourtweener:StartTween({0,0,0,0}, scale_time)
	else
		inst.AnimState:SetMultColour(0,0,0,0)
		inst.components.colourtweener:StartTween({0,0,0,0.6}, scale_time)
	end
end

local function OnRemove(inst)
	if inst.sizeTask then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end

	if inst.removeTask then
		inst.removeTask:Cancel()
		inst.removeTask = nil
	end
end

local MAX_SCALE = 1.5
local MIN_SCALE = 0.5

local function Shrink(inst, time)
	if inst.sizeTask then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end

	if inst.removeTask then
		inst.removeTask:Cancel()
		inst.removeTask = nil
	end

	inst.StartingScale = inst.Transform:GetScale()
	inst.TimeToImpact = time or 1.8
	inst.components.colourtweener:StartTween({0,0,0,0.6}, inst.TimeToImpact)
	inst.sizeTask = inst:DoPeriodicTask(FRAMES, LerpIn)
	inst.removeTask = inst:DoTaskInTime(inst.TimeToImpact, function() inst:Remove() end)
end

local function Water_Jump(inst)
	--Small -> Big
	SetUpScale(inst, MIN_SCALE, MAX_SCALE, 91 * FRAMES)
	--Big -> Small
	inst:DoTaskInTime(91 * FRAMES, function() SetUpScale(inst, MAX_SCALE, MIN_SCALE, FRAMES * 15, "OUT", easing.outExpo) end)
	inst:DoTaskInTime(106 * FRAMES, inst.Remove)
end

local function Water_Fall(inst)
	--Big -> Small
	SetUpScale(inst, MAX_SCALE, MIN_SCALE, 1.8, "IN")
	inst:DoTaskInTime(1.8, inst.Remove)
end

local function Grow(inst, time)
	if inst.sizeTask then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end

	if inst.removeTask then
		inst.removeTask:Cancel()
		inst.removeTask = nil
	end

	inst.StartingScale = inst.Transform:GetScale()
	inst.TimeToImpact = time or 3
	inst.components.colourtweener:StartTween({0,0,0,0.6}, inst.TimeToImpact)
	inst.sizeTask = inst:DoPeriodicTask(FRAMES, LerpOut)
	inst.removeTask = inst:DoTaskInTime(inst.TimeToImpact, function() inst:Remove() end)
end

local function shadowfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	
	inst.entity:AddNetwork()

	anim:SetBank("tigershark_shadow")
	anim:SetBuild("tigershark_shadow")
	anim:PlayAnimation("air", true)

	anim:SetOrientation( ANIM_ORIENTATION.OnGround )
	anim:SetLayer( LAYER_BACKGROUND )
	anim:SetSortOrder( 3 )

	inst.persists = false

	local s = 2
	inst.StartingScale = s
	inst.Transform:SetScale(s,s,s)

	inst:AddComponent("colourtweener")
	inst.AnimState:SetMultColour(0,0,0,0)

	inst.grow = Grow
	inst.shrink = Shrink
	inst.Water_Jump = Water_Jump
	inst.Water_Fall = Water_Fall

	inst.OnRemoveEntity = OnRemove
	
	inst.entity:SetPristine()

	return inst
end

return Prefab("tigersharkshadowp", shadowfn, assets, prefabs)