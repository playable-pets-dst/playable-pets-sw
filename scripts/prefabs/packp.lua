local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--ToDo: Reduce attack time, Snake is too slow and too frail to even fight things.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/packim.zip"),
	Asset("ANIM", "anim/packim_build.zip"),
	Asset("ANIM", "anim/packim_fat_build.zip"),
	Asset("ANIM", "anim/packim_fire_build.zip"),

}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 10,
	walkspeed = 10,
	damage = 30,
	range = 14,
	hit_range = 18,
	attackperiod = 0,
	bank = "packim",
	build = "packim_build",
	scale = 1,
	build2 = "packim_fat_build",
	build3 = "packim_fire_build",
	stategraph = "SGpackp",
	minimap = "packp.tex",
	
}

local getskins = {"1"}

local normalsounds =
{
	close = "dontstarve_DLC002/creatures/packim/close",
	death = "dontstarve_DLC002/creatures/packim/death",
	hurt = "dontstarve_DLC002/creatures/packim/hurt",
	land = "dontstarve_DLC002/creatures/packim/land",
	open = "dontstarve_DLC002/creatures/packim/open",
	swallow = "dontstarve_DLC002/creatures/packim/swallow",
	transform = "dontstarve_DLC002/creatures/packim/transform",
	trasnform_stretch = "dontstarve_DLC002/creatures/packim/trasnform_stretch",
	transform_pop = "dontstarve_DLC002/creatures/packim/trasformation_pop",
	fly = "dontstarve_DLC002/creatures/packim/fly",
	fly_sleep = "dontstarve_DLC002/creatures/packim/fly_sleep",
	sleep = "dontstarve_DLC002/creatures/packim/sleep",
	bounce = "dontstarve_DLC002/creatures/packim/fly_bounce",

	-- only fat packim
	fat_death_spin = "dontstarve_DLC002/creatures/packim/fat_death_spin",
	fat_land_empty = "dontstarve_DLC002/creatures/packim/fat_land_empty",
	fat_land_full = "dontstarve_DLC002/creatures/packim/fat_land_full",
}

local fatsounds = 
{
	close = "dontstarve_DLC002/creatures/packim/fat_close",
	death = "dontstarve_DLC002/creatures/packim/fat_death",
	hurt = "dontstarve_DLC002/creatures/packim/fat_hurt",
	land = "dontstarve_DLC002/creatures/packim/land",
	open = "dontstarve_DLC002/creatures/packim/fat_open",
	swallow = "dontstarve_DLC002/creatures/packim/fat_swallow",
	transform = "dontstarve_DLC002/creatures/packim/transform",
	trasnform_stretch = "dontstarve_DLC002/creatures/packim/trasnform_stretch",
	transform_pop = "dontstarve_DLC002/creatures/packim/trasformation_pop",
	fly = "dontstarve_DLC002/creatures/packim/fly",
	fly_sleep = "dontstarve_DLC002/creatures/packim/fly_sleep",
	sleep = "dontstarve_DLC002/creatures/packim/sleep",
	bounce = "dontstarve_DLC002/creatures/packim/fly_bounce",
	
	-- only fat packim
	fat_death_spin = "dontstarve_DLC002/creatures/packim/fat_death_spin",
	fat_land_empty = "dontstarve_DLC002/creatures/packim/fat_land_empty",
	fat_land_full = "dontstarve_DLC002/creatures/packim/fat_land_full",
}

-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('packp',
-----Prefab---------------------Chance------------
{
    {'meat',             		 1.00}, 
   
})

SetSharedLootTable('packfatp',
-----Prefab---------------------Chance------------
{
    {'meat',             		 1.00}, 
	{'spoiled_food',             1.00}, 
	{'spoiled_food',             1.00}, 
	{'spoiled_food',             1.00}, 
   
})

SetSharedLootTable('packfirep',
-----Prefab---------------------Chance------------
{
    {'meat',             	    1.00}, 
	{'firestaff',               1.00}, 
   
})

local function MorphFirePackim(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	if inst.isshiny ~= nil and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("packim_fire_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild(mob.build3)
	end
    inst:AddTag("spoiler")
	inst:AddComponent("heater")
	inst.components.heater.heat = 100
	inst.components.health.fire_damage_scale = 0
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetChanceLootTable('packfirep')
	inst._ismonster = true

	inst.sounds = normalsounds
	inst.skinn = 1
	if inst.firestaffcheck then
		inst.firestaffcheck:Cancel()
		inst.firestaffcheck = nil
	end
end

local function MorphFatPackim(inst)
	if inst.isshiny ~= nil and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("packim_fat_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild(mob.build2)
	end
	inst.components.locomotor.runspeed = 6
	inst.sounds = fatsounds
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetChanceLootTable('packfatp')
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "feather_robin_winter" 
	inst.components.shedder.shedHeight = 0.5
	inst.components.shedder:StartShedding(480) --Note: 480 is 1 day. 480 x n = n amount of days.

    inst.skinn = 2
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local spit = SpawnPrefab("fire_projectile")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

local function IsStaff(item)
	if item then
		return item.prefab == "firestaff" or nil
	else
		return nil
	end
end

local function CheckForStaff(inst)
	local fuel = inst.components.inventory:FindItem(IsStaff) or IsStaff(inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS))
	local req = 1
	if inst.skinn == 0 then
		if inst.components.inventory ~= nil and fuel ~= nil then
			inst.components.inventory:ConsumeByName("firestaff", 1)
			inst.skinn = 1
			inst.sg:GoToState("transform")					
		end
		if IsStaff(inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)) then
			inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS):Remove()
		end
	elseif inst.skin == 1 and inst.firestaffcheck then
		inst.firestaffcheck:Cancel()
		inst.firestaffcheck = nil
	end	
end

local function OnEat(inst, food)
    if food and food.components.edible and inst.skinn == 0 then
		if food.prefab == "fish" or food.prefab == "fish_cooked" then
			inst.birdfood = inst.birdfood + 1
		end
    end
end

local function onhitother2(inst, other)
    if inst.skinn == 1 and other.components.burnable ~= nil and not other.components.burnable:IsBurning() then
        if other.components.freezable ~= nil and other.components.freezable:IsFrozen() then
            other.components.freezable:Unfreeze()
        elseif other.components.fueled == nil then
            other.components.burnable:Ignite(true)
        end
    end
end

local function SkinSet(inst)
	if inst.skinn ~= nil and inst.skinn == 0 then
		inst.AnimState:SetBuild(mob.build)
	elseif inst.skinn ~= nil and inst.skinn == 1 then
		--inst:StopWatchingWorldState("isfullmoon", OnTransform)
		inst._ismonster = true
		MorphFirePackim(inst)
	elseif inst.skinn ~= nil and inst.skinn == 2 then	
		--inst:StopWatchingWorldState("isfullmoon", OnTransform)
		MorphFatPackim(inst)
	end	
end

local function OnPreload(inst, data)
    if data ~= nil and data.sharkfood ~= nil then
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        --inst.components.sanity:DoDelta(0)
    end
end

 local function OnLoad(inst, data)
	if data ~= nil then
		inst.skinn = data.skinn or 0
		inst.isshiny = data.isshiny or 0
		SkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.skinn = inst.skinn or 0
	data.isshiny = inst.isshiny or 0
end

local function GetPackimBuild(inst)
	if not inst:HasTag("playerghost") then
		if inst.skinn and inst.skinn == 1 then
			return mob.build3
		elseif inst.skinn and inst.skinn == 2 then
			return mob.build2
		else
			return mob.build
		end
	else
		return nil
	end
end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
			if inst.skinn == 0 then
				inst.AnimState:SetBuild("packim_shiny_build_0"..num)
			elseif inst.skinn == 1 then
				inst.AnimState:SetBuild("packim_fire_shiny_build_0"..num)
			elseif inst.skinn == 2 then
				inst.AnimState:SetBuild("packim_fat_shiny_build_0"..num)
			end
	else
		PlayablePets.CommonSetChar(inst, mob, nil, nil, GetPackimBuild(inst))	
	end	
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20 ,1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('packp')
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	inst.components.temperature.mintemp = 20 --prevents freezing
	inst.components.moisture:SetInherentWaterproofness(1) --Prevents getting wet.
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("flying")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.skinn = 0
	inst.isshiny = 0
	inst.birdfood = 0
	inst.sounds = normalsounds
	--inst.canrun = true
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.userfunctions =
	{
		MorphFirePackim = MorphFirePackim,
		MorphFatPackim	= MorphFatPackim,
	}
	inst.FireProjectile = FireProjectile
	
	local body_symbol = "packim_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	---------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 75, 0.5)
    PlayablePets.SetAmphibious(inst, nil, nil, true)
	inst.components.locomotor:SetAllowPlatformHopping(false)
    inst.DynamicShadow:SetSize(1.5, .6)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("onpickupitem", OnTransform)
	inst.firestaffcheck = inst:DoPeriodicTask(0.5, CheckForStaff)
	if MOBFIRE== "Disable" then
		inst.components.combat.onhitotherfn = onhitother2
	end
	---------------------------------
	--Functions that saves and loads data.
	
    inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, GetPackimBuild(inst)) end)
	inst:DoTaskInTime(3, function(inst) SetSkinDefault(inst, inst.isshiny) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, GetPackimBuild(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) SetSkinDefault(inst, inst.isshiny) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("packp", prefabs, assets, common_postinit, master_postinit, start_inv)
