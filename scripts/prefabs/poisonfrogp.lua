local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--ToDo: Reduce attack time, Snake is too slow and too frail to even fight things.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{

}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 175,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 3 + 1,
	walkspeed = 3,
	damage = 50,
	damage2 = 25, --damage gets reduced to compensate for poison.
	range = 1.5,
	bank = "snake",
	build = "snake_build",
	scale = 1,
	build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGsnakep",
	minimap = "snakep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('snakep',
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
	{'venomglandp',             1.00},  
	--{'snakeoil',             0.01}, --SW only.
   
})

local function ShareTargetFn(dude)
    return dude:HasTag("snake") and not dude.components.health:IsDead()
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 3)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 30)
end

local function RandomSkinSet(inst)
	if inst.skinn ~= nil and inst.skinn == 0 then
		inst.skinn = math.random(1,100)
		if inst.skinn >= 60 then
			inst.AnimState:SetBuild(mob.build2)
			inst.components.combat:SetDefaultDamage(mob.damage2) 
			inst.components.combat.playerdamagepercent = MOBPVP
			inst.components.combat.pvp_damagemod = 1 --disables second pvp override.
		else
			inst.AnimState:SetBuild(mob.build)
		end	
	else
		if inst.skinn ~=nil and inst.skinn >= 60 then
			inst.AnimState:SetBuild(mob.build2)
			inst.components.combat:SetDefaultDamage(mob.damage2) 
			inst.components.combat.playerdamagepercent = MOBPVP
			inst.components.combat.pvp_damagemod = 1 --disables second pvp override.
		elseif inst.skinn ~= nil and inst.skinn <= 59 then
			inst.AnimState:SetBuild(mob.build)
		end		
	end	
end

 local function OnLoad(inst, data)
	if data ~= nil then
		inst.skinn = data.skinn or 0
		RandomSkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.skinn = inst.skinn or 0
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
end

local function OnHitOther(inst, other, damage)
    if other.components.inventory ~= nil then
		inst.components.thief:StealItem(other)
	end
end

--Posion v3: To keep track of what version this is.
--Might be using too many checks, remove some of these if it causes performance issues.
local function SetPoison(inst)
if inst.ispoisoned and not inst:HasTag("playerghost") then 
if inst and inst.components.health and not inst.components.health:IsDead() then
if inst:HasTag("player") and not (inst:HasTag("mobplayer") or 
inst:HasTag("mobplayersw") or 
inst:HasTag("mobplayercu") or 
inst:HasTag("mobplayercave")or inst.mobplayer == true) then
inst.components.health:DoDelta(-0.5, true, "poison")
inst.components.sanity:DoDelta(-1, true, "poison") --shouldn't need to check this since players will always have sanity.
else
inst.components.health:DoDelta(-4, true, "poison") --do more damage on mobs.
end
--Normal Poison: 0.5 health per 2 secs. 120 in one day.
--Strong Poison: 1 health per 2 secs. 240 in one day.


inst:DoTaskInTime(2, function(inst) if inst:HasTag("poisoned") then --check to make sure the colors aren't messed up when cured.
inst.AnimState:SetMultColour(0.5,1,0.2,1) end end) 

	
end
else --you're cured!
	--inst:RemoveTag("poisoned")
	inst.ispoisoned = nil
	inst.AnimState:SetMultColour(1,1,1,1)
		if inst.poisontask ~= nil then
			inst.poisontask = nil
		end
	end

end

local function OnHitOther3(inst, other) --Poison workaround.
if other.poisonimmune == nil then
 --other:AddTag("poisoned")
 other.ispoisoned = true
if other and other.components.health and not other.components.health:IsDead() then



if other.poisontask == nil then
other.AnimState:SetMultColour(0.5,1,0.2,1)
other.poisontask = other:DoPeriodicTask(2, SetPoison)
if not other:HasTag("player") or (other:HasTag("mobplayer") or 
other:HasTag("mobplayersw") or 
other:HasTag("mobplayercu") or 
other:HasTag("mobplayercave") or other.mobplayer) and not other.poisonable then --Players need to craft a cure, so mob players will have theirs cured intime.
other.poisonwearofftask = other:DoTaskInTime(30, function(inst) if inst.poisontask then inst.poisontask:Cancel() inst.poisontask = nil inst.AnimState:SetMultColour(1,1,1,1) 
--inst:RemoveTag("poisoned") 
inst.ispoisoned = nil
end end)
			end
		end
	end
	end
end	


local function Equip(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD) or nil
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
	if hands ~= nil and hands.components.weapon then
		hands.components.weapon:SetDamage(45*2)
        hands.components.weapon:SetRange(TUNING.BISHOP_ATTACK_DIST+2, TUNING.BISHOP_ATTACK_DIST+6)
        hands.components.weapon:SetProjectile("spider_web_spit")
	end
end

local NIGHTVISION_COLOURCUBES =
{
    day = "images/colour_cubes/ruins_light_cc.tex",
    dusk = "images/colour_cubes/ruins_dim_cc.tex",
    night = "images/colour_cubes/purple_moon_cc.tex",
    full_moon = "images/colour_cubes/purple_moon_cc.tex",
}

local function SetNightVision(inst, enable) --This should be obvious
    if TheWorld.state.isnight or TheWorld:HasTag("cave") then
        inst.components.playervision:ForceNightVision(true)
        inst.components.playervision:SetCustomCCTable(NIGHTVISION_COLOURCUBES)
    else
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)
    end
end

local function RestoreNightImmunity(inst) --Resets immunity to Grue

	inst:DoTaskInTime(3, function(inst) 
	inst.Light:Enable(true)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(.1)
	inst.Light:SetIntensity(.1)
	inst.Light:SetColour(245/255,40/255,0/255)
	end, inst)
end

local function setChar(inst) --sets character when the player loads.
	if not inst:HasTag("playerghost") then
    inst.AnimState:SetBank(mob.bank)
    inst.AnimState:SetBuild(mob.build)
	--RandomSkinSet(inst)
    inst:SetStateGraph(mob.stategraph)
	end
	if MONSTERHUNGER== "Disable" then
		inst.components.hunger:Pause()
	end
	inst.components.sanity.ignore = true
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	inst.components.locomotor.fasteronroad = false
	
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns health to max health upon reviving
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    inst.components.health:SetMaxHealth(mob.health)
	inst.components.hunger:SetMax(mob.hunger)
	inst.components.sanity:SetMax(mob.sanity)
	inst.components.hunger:SetRate(mob.hungerrate)
	------------------------------------------
	--Combat--
	--inst:AddComponent("healthSRegenerate")
	inst.components.combat.playerdamagepercent = 0.5
	inst.components.combat:SetAttackPeriod(1.0)
	inst.components.combat:SetRange(mob.range)
	inst.components.combat:SetDefaultDamage(mob.damage)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/snake/hurt")
	
	
	
    inst.components.temperature.inherentinsulation = -TUNING.INSULATION_SMALL
	inst.AnimState:SetBank(mob.bank)
	inst.AnimState:SetBuild(mob.build)
	
	inst.OnSetSkin = function(skin_name)
		inst.AnimState:SetBuild(mob.build)
		inst:SetStateGraph(mob.stategraph)
	
		inst:ListenForEvent("ms_respawnedfromghost", DontTriggerCreep)
		DontTriggerCreep(inst)	
	end
	inst:SetStateGraph(mob.stategraph)
	inst.components.talker:IgnoreAll()
	if MONSTERHUNGER== "Disable" then
		inst.components.hunger:Pause()
	end
	inst.components.sanity.ignore = true
	----------------------------------
	--Locomotor--
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	--inst.components.locomotor:SetSlowMultiplier( 1 )
    --inst.components.locomotor:SetTriggersCreep(false)
    --inst.components.locomotor.pathcaps = { ignorecreep = true }
	--inst.components.locomotor:EnableGroundSpeedMultiplier(false)
	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('snakep')
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	--inst.components.temperature.mintemp = 20 --prevents freezing
	inst.components.moisture:SetInherentWaterproofness(1) --Prevents getting wet.
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("hostile")
	inst:AddTag("snake")
    inst:AddTag("monster")
	inst:AddTag("mobsleepsw")
	inst.mobplayer = true --lets mod know that you're a mob.
	
	--inst.mobsleepsw = true
	inst.taunt = true
	inst.skinn = 0
	inst.poisonimmune = true
	--inst.canrun = true
	
	----------------------------------
	--SanityAura--
	--inst:AddComponent("sanityaura")
	--inst.components.sanityaura.aura = -TUNING.SANITYAURA_TINY
	----------------------------------
	--Poison--
	--inst.components.poisonable = true
	--inst.components.combat.poisonous = true
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
    --inst.components.eater:SetCanEatGears() --self explanatory.
    --inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, 0.5)
	--inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
    --inst.Physics:ClearCollisionMask()
    --inst.Physics:CollidesWith(COLLISION.WORLD)
    --inst.Physics:CollidesWith(COLLISION.FLYERS)
	--MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    --inst.DynamicShadow:SetSize(.8, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Shedder--
	
	--inst:AddComponent("shedder")
	--inst.components.shedder.shedItemPrefab = "honey"
	--inst.components.shedder.shedHeight = 0.1
	--inst.components.shedder:StartShedding(960) --Note: 480 is 1 day. 480 x n = n amount of days.
	---------------------------------
	--Periodic Spawner--
	
	--inst:AddComponent("periodicspawner")
    --inst.components.periodicspawner:SetOnSpawnFn(OnSpawnFuel)
    --inst.components.periodicspawner.prefab = "glommerfuel"
    --inst.components.periodicspawner.basetime = TUNING.TOTAL_DAY_TIME * 2
    --inst.components.periodicspawner.randtime = TUNING.TOTAL_DAY_TIME * 2
    --inst.components.periodicspawner:Start()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("equip", Equip) --Enables spitting.
	inst:ListenForEvent("attacked", onattacked)
	--inst:ListenForEvent("onpickupitem", OnTransform)
	--inst:ListenForEvent("itemlose", OnTransform)
	--inst:ListenForEvent("onhitother", onhitother2)
	if MOBPOISON== "Enable" then
		inst.components.combat.onhitotherfn = OnHitOther3 
	end
	--inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	local light = inst.entity:AddLight() --Just added to make immune to grue. No visible or useful light.
  	inst.Light:Enable(true)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.9)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,150/255)
	
	inst.components.talker.colour = Vector3(127/255, 0/255, 0/255) --Text color when player Examines.
	inst:ListenForEvent("respawnfromghost", RestoreNightImmunity)

    inst:DoTaskInTime(0, setChar) --Sets Character.
    inst.ghostbuild = "ghost_monster_build"
    inst:ListenForEvent("respawnfromghost", function()
		inst:DoTaskInTime(3, function()  inst.components.health:SetInvincible(false)
            if inst.components.playercontroller ~= nil then
				inst.components.playercontroller:EnableMapControls(true)
				inst.components.playercontroller:Enable(true)
			end
			inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst:SetCameraDistance()
			inst.sg:RemoveStateTag("busy")
            SerializeUserSession(inst) 
		end)
        inst:DoTaskInTime(5, setChar)
		inst:DoTaskInTime(6, RemovePenalty)
		inst:DoTaskInTime(6, RestoreNightImmunity)
    end)
	
    return inst
end

return MakePlayerCharacter("snakep", prefabs, assets, common_postinit, master_postinit, start_inv)
