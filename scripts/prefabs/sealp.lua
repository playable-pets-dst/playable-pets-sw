local assets =
{
    Asset("ANIM", "anim/twister_seal.zip"),
}

local prefabs = {}

SetSharedLootTable('twister_sealp',
{
	{'meat', 1.00},	
	{'meat', 1.00},	
	{'meat', 1.00},	
	{'meat', 1.00},
	--{'magic_seal', 1.00},
	--Drop an item here too?
})

local function OnEntitySleep(inst)
	--This means the player let the seal live.
	--Let the seal escape & leave a gift of some sort behind.
	--local seal = SpawnPrefab("magic_seal")
	--seal.Transform:SetPosition(inst:GetPosition():Get())
	inst:Remove()
end

local function fn()
     local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddPhysics()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()
	--inst.DynamicShadow:SetSize(2.5, 1.5)

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	--MakePoisonableCharacter(inst)
	MakeCharacterPhysics(inst, 1000, 1)

	inst.Transform:SetTwoFaced()

	inst.AnimState:SetBank('twister')
	inst.AnimState:SetBuild('twister_build')
	inst.AnimState:PlayAnimation('seal_idle', true)

	inst:AddComponent("inspectable")

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetChanceLootTable(SW_PP_MODE == true and 'twister_seal' or 'twister_sealp')

	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(20)

	inst:AddComponent("combat")

	inst:SetStateGraph("SGtwister_sealp")
    local brain = require("brains/twistersealpbrain")
    inst:SetBrain(brain)

    inst:DoTaskInTime(1*FRAMES, function()
    	inst:ListenForEvent("entitysleep", OnEntitySleep)
    end)

	return inst
end

return Prefab("sealp", fn, assets, prefabs)
