local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/stinkray.zip"),
	
	Asset("SOUND", "sound/bat.fsb"),
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 250,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 35,
	range = 3,
	hit_range = 4,
	attackperiod = 2.5,
	bank = "stinkray",
	build = "stinkray",
	scale = 1.05,
	stategraph = "SGstinkrayp",
	minimap = "stinkrayp.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stinkrayp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
	{'venomglandp',             1.00},    
})

local function OnHitOther(inst, other) 
	PlayablePets.SetPoison(inst, other)
end	

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.STINKRAYP)
	
	inst.mobsleep = false
	inst.taunt2 = false
	
	inst.components.combat.onhitotherfn = nil
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.5, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('stinkrayp')
	----------------------------------
	--Tags--
	inst:AddTag("monster")
	inst:AddTag("stungray")
	inst:AddTag("flying")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.getskins = getskins
	inst.scale_water = 1
	inst.scale_flying = mob.scale

	inst.poisonimmune = true
	
	local body_symbol = "ray_face"
	inst.poisonsymbol = body_symbol
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeMediumBurnableCharacter(inst, body_symbol)	
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--

	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.DynamicShadow:SetSize(1.75, .6)
    inst.Transform:SetFourFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("stinkrayp", prefabs, assets, common_postinit, master_postinit, start_inv)
