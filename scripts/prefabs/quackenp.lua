local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--WARNING: If you're reading this then note that this mob might be VERY UNSTABLE.
--They are only here for SW port.
--Use at your own risk.
---------------------------

local getskins = {"1", "8"}

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/quacken.zip"),
}

local prefabs = 
{	
	"quacken_tentacle_p",
}
	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 3000,
	hunger = 500,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 14,
	walkspeed = 0,
	damage = 75*2,
	range = 35,
	hit_range = 0,
	attackperiod = 3,
	bank = "quacken",
	build = "quacken",
	scale = 1,
	stategraph = "SGquackenp",
	minimap = "quackenp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('quackenp',
-----Prefab---------------------Chance------------
{
    {'fishmeat',             1.00}, 
	{'fishmeat',             1.00}, 
	{'fishmeat',             1.00}, 
	{'fishmeat',             1.00}, 
	{'fishmeat',             1.00}, 
	{'fishmeat',             1.00}, 
	-------Extra Loot Drops--------	
	--{'quacken_chest',             1.00}, --SW Only
   
})



----------Quacken Functions--------------------
local function RemoveMinions(inst)
	if inst.components.minionspawner.numminions > 0 then
		inst.components.minionspawner:DespawnAll()	
	end
end

local RND_OFFSET = 1

local function OnAttack(inst, data)
	--if inst.canspit == true then
	--print("DEBUG: OnAttack Ran")
    local numshots = 1
	local def_numshots = 3
    if data.target then
        for i = 1, numshots do
            local offset = Vector3(math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET))
            inst.components.throwerp:Throw(data.target:GetPosition())
        end
    end
	for i = 1, def_numshots do
            local offset = Vector3(math.random(-RND_OFFSET, RND_OFFSET) + 0.5, math.random(-RND_OFFSET, RND_OFFSET) + 0.5, math.random(-RND_OFFSET, RND_OFFSET) + 0.5)
            inst.components.throwerp:ThrowDefensive(inst:GetPosition() + offset)
        end
	--end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
		inst.swmodeignore = data.swmode or nil
		--SkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
	data.swmode = inst.swmodeignore or nil
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20 ,1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.25) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.health:StartRegen(5, 5)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('quackenp')
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	inst.components.temperature.mintemp = 20 --prevents freezing
	inst.components.moisture:SetInherentWaterproofness(1) --Prevents getting wet.
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("epic")
    inst:AddTag("quacken")
    inst:AddTag("aquatic") --this might be important for SW.
	
	inst.getskins = getskins
	inst.Physics:SetMass(99999)
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.isshiny = 0
	inst._isdiving = false
	inst.swmodeignore = false
	inst.canspit = true
	inst.getskins = getskins
	inst.watermob = true --Putting this here incase its needed.
	
	inst:AddComponent("minionspawner")
    --inst.components.minionspawner.validtiletypes = {GROUND.OCEAN_SHALLOW, GROUND.OCEAN_MEDIUM, GROUND.OCEAN_DEEP, GROUND.OCEAN_CORAL, GROUND.MANGROVE, GROUND.OCEAN_SHIPGRAVEYARD}
    inst.components.minionspawner.miniontype = "quacken_tentacle_p"
    inst.components.minionspawner.distancemodifier = 35
    inst.components.minionspawner.maxminions = 45
	inst.components.minionspawner.validtiletypes = {GROUND.RIVER, GROUND.OCEAN_SHALLOW, GROUND.OCEAN_MEDIUM, GROUND.OCEAN_DEEP, GROUND.OCEAN_SHIPGRAVEYARD, GROUND.MANGROVE, GROUND.CORAL, GROUND.OCEAN_COASTAL_SHORE, GROUND.OCEAN_BRINEPOOL_SHORE, GROUND.OCEAN_COASTAL, GROUND.OCEAN_BRINEPOOL, GROUND.OCEAN_SWELL, GROUND.OCEAN_ROUGH, GROUND.OCEAN_HAZARDOUS}
	inst.components.minionspawner:RegenerateFreePositions()
	inst.components.minionspawner.shouldspawn = false
	
	inst:AddComponent("throwerp")
	inst.components.throwerp.throwable_prefab = "kraken_projectilep"
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeGiantCharacterPhysics(inst, 1000, 0.5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	inst.DynamicShadow:Enable(false) --Disables shadows.
	inst.components.freezable:SetResistance(9999)
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("onremove", RemoveMinions)
	inst:ListenForEvent("onmissother", OnAttack)

	--inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true) inst.components.locomotor:SetAllowPlatformHopping(false) inst.components.hunger:Pause() end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
end

return MakePlayerCharacter("quackenp", prefabs, assets, common_postinit, master_postinit, start_inv)
