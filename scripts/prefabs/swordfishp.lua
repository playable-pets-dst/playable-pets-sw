local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------

---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/fish_dogfish.zip"),
	Asset("ANIM", "anim/fish_swordfish01.zip"),
	
	Asset("ANIM", "anim/fish_med01.zip"),
	Asset("ANIM", "anim/fish_swordfish.zip"),
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 325,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 8,
	walkspeed = 8,
	damage = 50*2,
	range = 3.5,
	hit_range = 4,
	bank = "swordfish",
	build = "fish_swordfish",
	scale = 1,
	attackperiod = 2,
	--build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGswordfishp",
	minimap = "swordfishp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
if SW_PP_MODE == true then
		SetSharedLootTable('swordfishp',
	-----Prefab---------------------Chance------------
	{
		{'swordfish_dead',             1.00}, 
   
	})
else
	SetSharedLootTable('swordfishp',
	-----Prefab---------------------Chance------------
	{
		{'fishmeat',             1.00}, 
		{'fishmeat',             1.00}, --SW only.
   
	})
end


local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
		inst.swmodeignore = data.swmode or nil
		--SkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
	data.swmode = inst.swmodeignore or nil
end

local function SetLocoState(inst, state)
    --"above" or "below"
    inst.LocoState = string.lower(state)
end

local function IsLocoState(inst, state)
    return inst.LocoState == string.lower(state)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('swordfishp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
    inst:AddTag("aquatic") --this might be important for SW.
	inst.mobplayer = true --lets mod know that you're a mob.
	
	inst.isshiny = 0
	inst.mobsleep = true
	inst.swmodeignore = false
	inst.taunt2 = true
	inst.issurfaced = false

	inst.watermob = true --Putting this here incase its needed.
	inst.getskins = getskins
	
	inst.AnimState:PlayAnimation("shadow", true)
	inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround )

	inst.AnimState:SetLayer(LAYER_BACKGROUND )
	inst.AnimState:SetSortOrder( 3 )
	
	--IA
	inst:DoTaskInTime(0, function(inst)
		if SW_PP_MODE and SW_PP_MODE == true then
			inst.components.health:SetInvincible(true)
			SendToSea(inst)
		end
	end)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 5, 1.25)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, true)
	
	SetLocoState(inst, "below")
    inst.SetLocoState = SetLocoState
    inst.IsLocoState = IsLocoState
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
  	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("swordfishp", prefabs, assets, common_postinit, master_postinit, start_inv)
