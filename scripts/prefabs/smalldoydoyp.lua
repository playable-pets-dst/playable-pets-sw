local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
	Asset("ANIM", "anim/doydoy.zip"),
	Asset("ANIM", "anim/doydoy_baby.zip"),
	Asset("ANIM", "anim/doydoy_baby_build.zip"),
	
	Asset("ANIM", "anim/doydoy_teen_build.zip"),
	
	Asset("ANIM", "anim/doydoy_adult_build.zip"),
}
local prefabs = 
{	
    
}
	
local start_inv = 
{

}

local mob = 
{
	health = 50,
	hunger = 50,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 50,
	runspeed = 6,
	walkspeed = 6,
	damage = 40,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	
	bank = "doydoy_baby",
	build = "doydoy_baby_build",
	scale = 1,
	stategraph = "SGsmalldoydoyp",
	minimap = "smalldoydoyp.tex",
}

local mob_teen = 
{
	health = 125,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 3,
	walkspeed = 3,
	damage = 40,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	
	bank = "doydoy",
	build = "doydoy_teen_build",
	scale = 1,
	stategraph = "SGdoydoyp",
	minimap = "smalldoydoyp.tex",
}

local mob_adult = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 4,
	walkspeed = 4,
	damage = 40,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	
	bank = "doydoy",
	build = "doydoy_adult_build",
	scale = 1,
	stategraph = "SGdoydoyp",
	minimap = "doydoyp.tex",
}

if SW_PP_MODE == true then
	SetSharedLootTable('smalldoydoyp',
	{
		{'smallmeat',             1.00},
		{'doydoyfeather',         1.00},   
	})
else
	SetSharedLootTable('smalldoydoyp',
	{
		{"smallmeat", 1.00},
    
	})
end
--local birdfood = 1

local babysounds = 
{
	eat_pre = "dontstarve_DLC002/creatures/baby_doy_doy/eat_pre",
	swallow = "dontstarve_DLC002/creatures/baby_doy_doy/swallow",
	hatch = "dontstarve_DLC002/creatures/baby_doy_doy/hatch",
	death = "dontstarve_DLC002/creatures/baby_doy_doy/death",
	jump = "dontstarve_DLC002/creatures/baby_doy_doy/jump",
	peck = "dontstarve_DLC002/creatures/teen_doy_doy/peck",
}

local teensounds = 
{
	idle = "dontstarve_DLC002/creatures/teen_doy_doy/idle",
	eat_pre = "dontstarve_DLC002/creatures/teen_doy_doy/eat_pre",
	swallow = "dontstarve_DLC002/creatures/teen_doy_doy/swallow",
	hatch = "dontstarve_DLC002/creatures/teen_doy_doy/hatch",
	death = "dontstarve_DLC002/creatures/teen_doy_doy/death",
	jump = "dontstarve_DLC002/creatures/baby_doy_doy/jump",
	peck = "dontstarve_DLC002/creatures/teen_doy_doy/peck",
}

foodprefs = {"VEGGIE", "SEEDS"}
adultfoodprefs = {"MEAT", "VEGGIE", "SEEDS", "ELEMENTAL", "WOOD", "ROUGHAGE", "GENERIC"}

local function SetAdult(inst)
    inst.AnimState:SetBank("doydoy")
	if inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("doydoy_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("doydoy_adult_build")
	end
    inst:SetStateGraph("SGdoydoyp")
	inst.components.lootdropper:SetLoot("doydoyp")
	inst:RemoveTag("baby")
	inst:RemoveTag("teen")
	
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/doy_doy/hit")
	inst.components.eater:SetAbsorptionModifiers(4,1,1)
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	inst.components.eater.caneat = adultfoodprefs
	inst.components.eater.preferseating = adultfoodprefs
	
	inst.components.eater:SetCanEatHorrible()
	inst.components.eater.strongstomach = true -- can eat monster meat!
	
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	inst.taunt = true
	
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(2.75, 1)
end

local function SetTeen(inst)

	inst.sounds = teensounds
	
    inst.AnimState:SetBank("doydoy")
	if inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("doydoy_teen_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("doydoy_teen_build")
	end
    inst:SetStateGraph("SGteendoydoyp")
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/doy_doy/hit")
	
	PlayablePets.SetCommonStats(inst, mob_teen) --mob table, ishuman, ignorepvpmultiplier
	inst.components.lootdropper:SetLoot(SW_PP_MODE == true and {"drumstick", "doydoyfeather", "doydoyfeather"} or {"drumstick", "drumstick"})
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
	inst.components.eater:SetAbsorptionModifiers(4,1,1)
	inst:AddTag("teen")
	inst:RemoveTag("baby")
	inst.AnimState:PlayAnimation("idle")

	MakeCharacterPhysics(inst, 10, 0.25)
    inst.DynamicShadow:SetSize(1.5, 0.5)
	
	inst.components.temperature.maxtemp = 60
	inst.components.temperature.mintemp = 20
	inst.components.moisture:SetInherentWaterproofness(1)
end

local function OnAdult(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.sg:GoToState("peck")
	SetAdult(inst)
end

local function OnTeen(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.sg:GoToState("taunt")
	SetTeen(inst)
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.birdfood == 0 and inst.birdfood < 50 then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger
	end
	
    if inst.birdfood >= 50 and inst.birdfood < 130 then
		inst.components.health.maxhealth = mob_teen.health
		inst.components.hunger.max = mob_teen.hunger
	
		inst.components.health:SetPercent(health_percent)
		OnTeen(inst)	
    end
	
	if inst.birdfood >= 130 then
		inst.components.health.maxhealth = mob_adult.health
		inst.components.hunger.max = mob_adult.hunger
	
		inst.components.health:SetPercent(health_percent)
		OnAdult(inst)
	
    end

    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
	if MOBGROW== "Enable" then
		if food and food.components.edible then
			if food.components.edible.foodtype == FOODTYPE.MEAT or food.components.edible.foodtype == FOODTYPE.VEGGIE or food.components.edible.foodtype == FOODTYPE.SEEDS and MOB_DOYDOY == "Enable" then
				--food helps you grow!
				inst.birdfood = inst.birdfood + 1
				if inst.birdfood >= 50 and inst:HasTag("baby") then
					--inst.sg:GoToState("transform")
					OnTeen(inst)
					applyupgrades(inst)
				end
		
				if inst.birdfood >= 130 and inst:HasTag("teen") then
					--inst.sg:GoToState("transform2")
					OnAdult(inst)
					applyupgrades(inst)
				end
			end
		end
	end
end

local function ondeath(inst)
    inst.birdfood = 0
	inst:RemoveTag("teen")
	inst:AddTag("baby")
	inst.sounds = babysounds
    applyupgrades(inst)
end

local function OnPreload(inst, data)
    if data ~= nil and data.birdfood ~= nil then
        inst.birdfood = data.birdfood
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        --inst.components.sanity:DoDelta(0)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.birdfood = data.birdfood or 0	
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.birdfood = inst.birdfood > 0 and inst.birdfood or nil
	data.isshiny = inst.isshiny or 0
end

local function setChar(inst)
	if not inst:HasTag("playerghost") then
		if inst.birdfood < 50 then
			inst.AnimState:SetBank("doydoy_baby")
			inst.AnimState:SetBuild("doydoy_baby_build")
			inst:SetStateGraph("SGsmalldoydoyp")
		end
		if inst.birdfood >= 50 and inst.birdfood < 130 then
			SetTeen(inst)
			--inst.components.hunger:Pause()
			inst.components.sanity.ignore = true
		end
		if inst.birdfood >= 130 then
			SetAdult(inst)
			inst.components.hunger:Pause()
			inst.components.sanity.ignore = true
		end
	end
end

local common_postinit = function(inst) 
	inst.soundsname = "wilson"

	inst.MiniMapEntity:SetIcon( "smalldoydoyp.tex" )
	
	inst.AnimState:PlayAnimation("idle")
	
	inst:DoTaskInTime(0, function()
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		--print("DEBUG:SetSkinDefault shinizer ran")
			if inst:HasTag("baby") then
				inst.AnimState:SetBuild("doydoy_baby_shiny_build_0"..num)
			elseif inst:HasTag("teen") then
				inst.AnimState:SetBuild("doydoy_teen_shiny_build_0"..num)
			else
				inst.AnimState:SetBuild("doydoy_shiny_build_0"..num)
			end
	else
		--print("DEBUG:SetSkinDefault normalizer ran")
		setChar(inst)		
	end	
end

local function GetMobTable(inst)
	if inst.birdfood and inst.birdfood >= 50 and inst.birdfood < 130 then
		return mob_teen
	elseif inst.birdfood and inst.birdfood >= 130 then
		return mob_adult
	else
		return mob
	end
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/baby_doy_doy/hit")
    ----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('smalldoydoyp')
	----------------------------------
	--Tags--
	inst:AddTag("baby")
	
	inst.birdfood = 0
	inst.setskin_defaultfn = SetSkinDefault
	inst.mobsleep = true
	inst.isshiny = 0
	
	inst.sounds = babysounds
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
	inst.components.eater:SetOnEatFn(OnEat)
	
	--inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
	inst.components.eater.caneat = foodprefs
	inst.components.eater.preferseating = foodprefs
	inst.components.eater:SetAbsorptionModifiers(4,3,3)
	
	--inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, 0.25)
    inst.DynamicShadow:SetSize( 1.5, .5 )
    inst.Transform:SetFourFaced()	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)    
    ------------------
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	inst.OnPreLoad = OnPreload
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) ondeath(inst) end)
    end)
	
	
    return inst
	
end

return MakePlayerCharacter("smalldoydoyp", prefabs, assets, common_postinit, master_postinit, start_inv)
