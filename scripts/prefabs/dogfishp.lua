local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------

---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/fish_dogfish.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 150,
	hunger = 150,
	hungerrate = 0.10, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 8,
	walkspeed = 5,
	damage = 35,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	
	bank = "dogfish",
	build = "fish_dogfish",
	scale = 1,
	stategraph = "SGdogfishp",
	minimap = "dogfishp.tex",
}
-----------------------
--Loot that drops when you die, duh.
if SW_PP_MODE == true then
	SetSharedLootTable('dogfishp',
		-----Prefab---------------------Chance------------
	{
		{'solofish_dead',             1.00}, 
	})
else
	SetSharedLootTable('dogfishp',
		-----Prefab---------------------Chance------------
	{
		{'fishmeat',             1.00}, 
	})
end	

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local function SetLocoState(inst, state)
    --"above" or "below"
    inst.LocoState = string.lower(state)
end

local function IsLocoState(inst, state)
    return inst.LocoState == string.lower(state)
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	inst.AnimState:PlayAnimation("shadow", true)
	inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround )

	inst.AnimState:SetLayer(LAYER_BACKGROUND )
	inst.AnimState:SetSortOrder( 3 )
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('dogfishp')
	----------------------------------
	--Tags--
    inst:AddTag("aquatic") 
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.swmodeignore = false
	inst.taunt2 = true
	inst.issurfaced = false
	inst.watermob = true --Putting this here incase its needed.
	inst.getskins = getskins
	
	local body_symbol = "dogfish_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
	
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, 0.5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, true)
	
	SetLocoState(inst, "below")
    inst.SetLocoState = SetLocoState
    inst.IsLocoState = IsLocoState
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
   ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("dogfishp", prefabs, assets, common_postinit, master_postinit, start_inv)
