local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------


local assets = 
{
	Asset("ANIM", "anim/cactus_volcano.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
}

local start_inv2 = 
{

}

-----------------------
--Stats--
local mob = 
{
	health = 400,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 7,
	walkspeed = 7,
	damage = 50,
	attackperiod = 1,
	hit_range = 3,
	range = 3,
	bank = "cactus_volcano",
	build = "cactus_volcano",
	scale = 1,
	stategraph = "SGcactusp",
	minimap = "cactusp.tex",
	
}

-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('cactusp',
-----Prefab---------------------Chance------------
{
		{'cactus_meat',			1.00},
		{'cactus_meat',			1.00},
		{'cactus_meat',			0.50},
		{'cactus_flower',		0.25},
		{'boneshard',			1.00},
		{'boneshard',			0.50},
})

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local function OnAttacked_Forge(inst, data)
	if data.attacker and data.attacker:IsNear(inst, PPSW_FORGE.CACTUSP.HIT_RANGE) and data.attacker ~= inst then
		data.attacker.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(data.attacker)*0.6)
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.CACTUSP)
	
	inst.mobsleep = false
	inst.healmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:ListenForEvent("attacked", OnAttacked_Forge)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
	inst:AddComponent("sizetweener") --has to be added before setting the stategraph!
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20 , 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.health:StartRegen(5, 5)
	inst.components.combat:SetAreaDamage(5, 1.0)
	inst.components.hunger:Pause()
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('cactusp')
	----------------------------------
	--Tags--
	inst:AddTag("thorny")
	inst:AddTag("elephantcactus")
	
	inst.mobsleep = true
	inst.poisonimmune = true
	inst.mobplayer = true
	inst.taunt2 = true
	inst._isdiving = false
	inst.isshiny = 0
	inst.hit_recovery = 3
	inst.getskins = getskins
	
	local body_symbol = "bush_berry_build"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, 0.75)
	inst.Physics:SetMass(99999)
	inst.components.combat.notags = {"elephantcactus"}
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetNoFaced()	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("cactusp", prefabs, assets, common_postinit, master_postinit, start_inv)
