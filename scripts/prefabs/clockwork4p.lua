local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/knightboat.zip"),
	Asset("ANIM", "anim/knightboat_death.zip"),
    Asset("ANIM", "anim/knightboat_build.zip"),
	
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

--if MOBHOUSE ~= nil and MOBHOUSE == ("Enable1" or "Enable3") then
	--start_inv = start_inv2
--end
-----------------------
--Stats--
local mob = 
{
	health = 600,
	hunger = 150,
	hungerrate = 0, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 4,
	damage = 0,
	range = 30,
	hit_range = 0,
	attackperiod = 2,
	bank = "knightboat",
	build = "knightboat_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGclockwork4p",
	minimap = "clockwork4p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('clockwork4p',
-----Prefab---------------------Chance------------
{
    {'gears',			1.00},  
	{'gears',			0.66}, 
	{'gears',			0.33}, 
})

local function OnAttack(inst, data)
    local numshots = 2
	local accnumshots = 1
    if data.target then
        for i = 0, numshots - 1 do
            local offset = Vector3(math.random(-5, 5), math.random(-5, 5), math.random(-5, 5))
            inst:DoTaskInTime(FRAMES * 10 * i, function()
				if data.target ~= nil then
                inst.components.throwerp:Throw(data.target:GetPosition() + offset)
				end
            end)
        end
    end
	if data.target ~= nil then
        for i = 0, accnumshots - 1 do
            local offset = Vector3(0,0, 0)
			if data.target ~= nil then
            inst.components.throwerp:Throw(data.target:GetPosition() + offset)			
			end
        end
    end
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("chess") and not dude.components.health:IsDead() end, 3)
    end
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
end

local function Unequip(inst) --If a hat removes the head, this fixes that.
    inst.AnimState:Show("HAT")
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.KNIGHTBOAT)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end


local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.75, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/knight_steamboat/hit")
	inst.components.combat.hiteffectsymbol = "spring"
	inst.components.hunger:Pause()
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('clockwork4p')
	----------------------------------
	--Tags--
	inst:AddTag("knight")
	inst:AddTag("chess")
	inst:AddTag("aquatic")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.mobplayer = true
	inst.poisonimmune = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	inst.AnimState:Show("HAT")
	local body_symbol = "spring"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, 1)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, true)
	
	inst:AddComponent("throwerp")
    inst.components.throwerp.throwable_prefab = "knightboat_cannonshotp"
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("onmissother", OnAttack)
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end)
		inst.components.hunger:Pause()
    end)
	
    return inst
	
end

return MakePlayerCharacter("clockwork4p", prefabs, assets, common_postinit, master_postinit, start_inv)
