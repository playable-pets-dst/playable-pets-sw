local MakePlayerCharacter = require "prefabs/player_common"

--ToDo: Replace code involving Runner tags. I'm sure you'll need to replace the equip functions too.
local prefabname = "sharkittenp"
local assets = 
{
	Asset("ANIM", "anim/sharkitten_basic.zip"),
	Asset("ANIM", "anim/sharkitten_build.zip"),
	Asset("SOUND", "sound/hound.fsb"),	
}

local getskins = {"1"}

local prefabs = {	
    }
	
local start_inv = {

}

local start_inv2 = 
{
	"sharkhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end

local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 10,
	walkspeed = 10,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "sharkitten",
	build = "sharkitten_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGsharkittenp",
	minimap = "sharkittenp.tex",
}

local mob_teen = 
{
	health = TUNING.TIGERSHARK_TEENP_HEALTH,
	hunger = TUNING.TIGERSHARK_TEENP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = TUNING.TIGERSHARK_TEENP_SANITY,
	runspeed = 12,
	walkspeed = 4,
	runspeed_water = 12,
	walkspeed_water = 5,
	damage = 60*2,
	attackperiod = 1.5,
	range = 2,
	hit_range = 3,
	bank = "tigershark",
	build = "tigershark_ground_build",
	build2 = "tigershark_water_build",
	scale = 0.5,
	--build2 = "alternate build here",
	stategraph = "SGtigersharkteenp",
	minimap = "tigersharkp.tex",
}

local mob_adult = 
{
	health = TUNING.TIGERSHARKP_HEALTH,
	hunger = TUNING.TIGERSHARKP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = TUNING.TIGERSHARKP_SANITY,
	runspeed = 9,
	walkspeed = 4,
	runspeed_water = 12,
	walkspeed_water = 6,
	damage = 100*2,
	attackperiod = 3,
	range = 30,
	hit_range = 0,
	bank = "tigershark",
	build = "tigershark_ground_build",
	build2 = "tigershark_water_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGtigersharkp",
	minimap = "tigersharkp.tex",
}

SetSharedLootTable('sharkittenp',
{
    {"fishmeat", 1.00},
	{"fishmeat", 1.00},  
})

SetSharedLootTable('tigersharkteenp',
{
    {"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00}, 
	{"fishmeat", 1.00},
})

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function oncollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local function SetWater(inst)
    inst:ClearStateGraph()
	if inst:HasTag("sharkitten") and inst.isteen then
		inst:SetStateGraph("SGtigersharkteenp_water")
		inst.components.locomotor.walkspeed = mob_teen.walkspeed_water
		inst.components.locomotor.runspeed = mob_teen.runspeed_water
	else
		inst:SetStateGraph("SGtigersharkp_water")
		inst.components.locomotor.walkspeed = mob_adult.walkspeed_water
		inst.components.locomotor.runspeed = mob_adult.runspeed_water
	end
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
    inst.AnimState:AddOverrideBuild("tigershark_water_ripples_build")
    inst:AddTag("aquatic")
    inst.DynamicShadow:Enable(false)
end

local function SetGround(inst)
    if inst:HasTag("sharkitten") and inst.isteen then
		inst:SetStateGraph("SGtigersharkteenp")
		inst.components.locomotor.walkspeed = mob_teen.walkspeed
		inst.components.locomotor.runspeed = mob_teen.runspeed
	else
		inst:SetStateGraph("SGtigersharkp")
		inst.components.locomotor.walkspeed = mob_adult.walkspeed
		inst.components.locomotor.runspeed = mob_adult.runspeed
	end
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
    inst:RemoveTag("aquatic")
    inst.DynamicShadow:Enable(true)
end

local function OnWaterChange(inst, onwater)
	if not inst.sg:HasStateTag("specialattack") and not inst:HasTag("playerghost") then
		if onwater then
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
			SetWater(inst)
		else
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
			SetGround(inst)
		end 
		local splash = SpawnPrefab("splash_green")
		local pos = inst:GetPosition()
		splash.Transform:SetPosition(pos.x, pos.y, pos.z)
	end 
end

local function SetAdult(inst, isfromcommand)
    --print("smallbird - SpawnAdult")
	inst.isadult = true
	inst.isteen = true --blegh
	if not isfromcommand then
		MakeCharacterPhysics(inst, 1000, 1.33)
		inst.Transform:SetScale(1.0, 1.0, 1.0)
	end
	inst.components.combat:EnableAreaDamage(true)
	inst.components.combat:SetAreaDamage(5, 0.8)
	inst:SetStateGraph("SGtigersharkp")
	
	inst.hit_recovery = 0.75
	inst.Physics:SetCollisionCallback(oncollide)
	inst:RemoveTag("sharkitten")
	
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	inst.components.combat.playerdamagepercent = MOBPVP
	inst.altattack = true
	inst.sg:GoToState("special_atk1")
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/bearger/hurt")
	
	inst.mob_scale = 2 -- doesn't do anything other than fails resize checks
	inst.Transform:SetScale(1.0, 1.0, 1.0)
	
	if inst:HasTag("swimming") then
		SetWater(inst)
	end
	
	inst.SetWater = SetWater
	inst.SetGround = SetGround
	PlayablePets.SetAmphibious(inst, mob_adult.bank, mob_adult.bank, nil, nil, true)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					OnWaterChange(inst, true)
				end	
			end)
		inst.components.amphibiouscreature:SetExitWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					OnWaterChange(inst, false)
				end
			end)
	end
		
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 1
    inst.components.groundpounder.numRings = 3
	inst.components.groundpounder.groundpounddamagemult = 0
	inst.components.groundpounder.noTags = {"sharkitten"}

	inst.components.eater.eatwholestack = true
	
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(SW_PP_MODE == true and 'tigershark' or 'tigersharkp')
	
	
    inst.DynamicShadow:SetSize( 6, 3 )
    inst.DynamicShadow:Enable(false)
	
	local body_symbol = "tshark_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
end

local function SetTeen(inst, isfromcommand)
    --print("smallbird - SpawnAdult")
	inst.isteen = true
    inst.AnimState:SetBank("tigershark")
	if not isfromcommand then
		if not inst.mob_scale or inst.mob_scale > 1 then
			inst.mob_scale = 0.5
		end	
		inst.Transform:SetScale(0.5, 0.5, 0.5)
		MakeCharacterPhysics(inst, 500, 0.66)
	end
	inst:SetStateGraph("SGtigersharkteenp")
	inst:AddTag("amphibious")
	inst.components.combat.playerdamagepercent = MOBPVP	
	
	PlayablePets.SetCommonStats(inst, mob_teen) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	inst.sg:GoToState("special_atk1")
	
	if inst:HasTag("swimming") then
		SetWater(inst)
	end
	
	inst.SetWater = SetWater
	inst.SetGround = SetGround
	PlayablePets.SetAmphibious(inst, mob_teen.bank, mob_teen.bank)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
        function(inst)
			if not inst:HasTag("playerghost") then
				OnWaterChange(inst, true)
			end	
        end)
		inst.components.amphibiouscreature:SetExitWaterFn(
		function(inst)
			if not inst:HasTag("playerghost") then
				OnWaterChange(inst, false)
			end
		end)
	end
	
    inst.DynamicShadow:SetSize(3, 1.5)
    inst.DynamicShadow:Enable(false)
	
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('tigersharkteenp')
	
	local body_symbol = "tshark_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
end

local function OnAdult(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	--inst.sg:GoToState("transform2")
	
    inst:SetStateGraph("SGtigersharkp")
	SetAdult(inst)
end

local function OnTeen(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	SetTeen(inst)
end

local function applyupgrades(inst)
    --local max_upgrades = 15
    --inst.level = math.min(inst.level, max_upgrades)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.sharkfood == 0 and inst.sharkfood < 100 then
	inst.components.health.maxhealth = 175
	inst.components.hunger.max = 150
	
	end
	
    if inst.sharkfood >= 100 and inst.sharkfood < 250 then
		inst.components.health.maxhealth = 750
		inst.components.hunger.max = 250
		inst.components.health:SetPercent(health_percent)
		OnTeen(inst)
    end
	
	if inst.sharkfood >= 250 then
		inst.components.health.maxhealth = 5500 --bonus HP
		inst.components.hunger.max = 500
	
		inst.components.health:SetPercent(health_percent)
		OnAdult(inst)	
    end

    inst.components.health:SetPercent(health_percent)
end



local function OnEat(inst, food)
--if MOBGROW== "Enable" then
    if food and food.components.edible and food.components.edible.foodtype == FOODTYPE.MEAT and MOB_TIGERSHARK == "Enable" and inst.sharkfood < 255 then
        --give an upgrade!
		if food.prefab == "mysterymeat" and inst:HasTag("sharkitten") and not inst.isteen then
			inst.sharkfood = inst.sharkfood + 3
			inst.components.health:DoDelta(20, false)
			inst.components.hunger:DoDelta(50, false)
			if inst.mob_scale and inst.mob_scale < 2  and inst.sharkfood < 250 then
				inst.mob_scale = inst.mob_scale + 0.003*3
			end	
		elseif food.prefab == "mysterymeat" then
			inst.sharkfood = inst.sharkfood + 0.5
			inst.components.health:DoDelta(10, false)
			inst.components.hunger:DoDelta(15, false)
			if inst.mob_scale and inst.mob_scale < 2 and inst.sharkfood < 250 then
				inst.mob_scale = inst.mob_scale + 0.003*0.5
			end	
		else
			inst.sharkfood = inst.sharkfood + 1
			if inst.mob_scale and inst.mob_scale < 2 and inst.sharkfood < 250 then
				inst.mob_scale = inst.mob_scale + 0.003
			end	
		end		
		if inst.mob_scale and inst.mob_scale < 2 then
			inst.Transform:SetScale(inst.mob_scale, inst.mob_scale, inst.mob_scale)
		end
		
		if inst.teen and inst.teen == true then
			inst.components.combat:SetRange(3 + inst.mob_scale, 3 + inst.mob_scale)
		end
		
		if inst.sharkfood >= 100 and not inst.isteen and inst.sharkfood < 250 then
			--inst.sg:GoToState("transform")
			OnTeen(inst)
			applyupgrades(inst)
		end
		
		if inst.sharkfood >= 250 and inst:HasTag("sharkitten") then
			--inst.sg:GoToState("transform2")
			OnAdult(inst)
			applyupgrades(inst)
		end
   -- end
end	

	if SW_PP_MODE == true then
		if food and food.components.edible and food.components.edible.foodtype == FOODTYPE.MEAT and not inst:HasTag("sharkitten") then
			local count = food.components.stackable and food.components.stackable:StackSize() or 1
			for i = 1, count or 1 do
				if math.random(1,100) >= 50 then
					inst.components.inventory:GiveItem(SpawnPrefab("mysterymeat"))
				end
			end
		end
	end
end

local function ondeath(inst)
	inst.amphibious = nil
	inst.altattack = nil
	inst.isadult = nil
	inst.isteen = nil
	inst.components.drownable.enabled = true
	inst.sharkfood = 0
	inst.mob_scale = 0.8
	inst.components.combat:EnableAreaDamage(false)
	inst.components.locomotor:SetAllowPlatformHopping(true)
	inst:RemoveComponent("amphibiouscreature")
	inst:RemoveTag("aquatic")
	inst:RemoveTag("swimming")
	inst:AddTag("sharkitten")
	inst.Physics:CollidesWith(COLLISION.LIMITS)
	inst.DynamicShadow:SetSize( 2.5, 1.5 )
    applyupgrades(inst)		
end

local function grow(inst, dt)
    if inst.components.scaler.scale < 0.75 then
        local new_scale = math.min(inst.components.scaler.scale + TUNING.ROCKY_GROW_RATE*dt, 0.75)
        inst.components.scaler:SetScale(new_scale)
    else
        if inst.growtask then
            inst.growtask:Cancel()
            inst.growtask = nil
        end
    end
	
	inst.charge_time = math.max(0, inst.charge_time - dt)
end

local function SetSkinDefault(inst, data)
	if data then
		if inst:HasTag("sharkitten") and not (inst.isteen or inst.isadult) then
			inst.AnimState:SetBuild(data.build)
		else
			if inst:HasTag("swimming") then
				inst.AnimState:SetBuild(data.adult_build2)
			else
				inst.AnimState:SetBuild(data.adult_build)
			end
		end
	else
		if inst:HasTag("sharkitten") and not (inst.isteen or inst.isadult) then
			inst.AnimState:SetBuild(mob.build)
		else
			if inst:HasTag("swimming") then
				inst.AnimState:SetBuild(mob_adult.build2)
			else
				inst.AnimState:SetBuild(mob_adult.build)
			end
		end
	end
end


local function applyscale(inst, scale)
	inst.DynamicShadow:SetSize(2.5 * scale, 1.5 * scale)
end


local function OnPreload(inst, data)
    if data ~= nil and data.sharkfood ~= nil then
        inst.sharkfood = data.sharkfood
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.sharkfood = data.sharkfood or 0
		inst.mob_scale = data.mob_scale or 1
		if data.charge_time ~= nil then
			 StartTimer(inst, data.charge_time)
		end	 
	end
end

local function OnSave(inst, data)
	data.sharkfood = inst.sharkfood > 0 and inst.sharkfood or nil
	data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
	data.mob_scale = inst.mob_scale or 1
end

local function setChar(inst, isfromcommand)
	if not inst:HasTag("playerghost") then
		PlayablePets.CommonSetChar(inst, mob)
		if inst.mob_scale and inst.mob_scale < 2 then
			inst.Transform:SetScale(inst.mob_scale, inst.mob_scale, inst.mob_scale)
		end
		if inst.sharkfood >= 100 and inst.sharkfood < 250 then
			SetTeen(inst, isfromcommand and isfromcommand or nil)
		end
		if inst.sharkfood >= 250 then
			SetAdult(inst, isfromcommand and isfromcommand or nil)
		end
	end
end

local common_postinit = function(inst) 
	inst.soundsname = "wilson"

	inst.MiniMapEntity:SetIcon( "tigersharkp.tex" )

	inst:DoTaskInTime(0, function() --This needs to be updated.
   if ThePlayer then
      ThePlayer:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function GetMobTable(inst)
	if inst.sharkfood and inst.sharkfood >= 100 and inst.sharkfood < 250 then
		return mob_teen
	elseif inst.sharkfood and inst.sharkfood >= 250 then
		return mob_adult
	else
		return mob
	end
end

local master_postinit = function(inst) 
    ------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(SW_PP_MODE == true and 'sharkitten' or 'sharkittenp')

	inst:AddTag("monster")
    inst:AddTag("sharkitten")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.sharkfood = 0
	inst.charge_task = nil
    inst.charge_time = 0	
	inst.mob_scale = 0.8
	
	inst.SetWater = SetWater
	inst.SetGround = SetGround
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
    inst.components.eater:SetCanEatHorrible()
	inst.components.eater:SetOnEatFn(OnEat)
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--IA's tile tracker--
	inst:DoTaskInTime(3, function(inst)
		if SW_PP_MODE == true then		
			--IA no longer uses tiletracker
		end
	end)
	
	---------------------------------
	--Physics and Scale--	
	MakeCharacterPhysics(inst, 10, 0.5)
    inst.DynamicShadow:SetSize( 2.5, 1.5 )
    inst.Transform:SetFourFaced()
	
	inst:DoTaskInTime(0, function(inst)
		if inst.mob_scale and inst.mob_scale < 2 then
			inst.Transform:SetScale(inst.mob_scale, inst.mob_scale, inst.mob_scale)
		end
	end)
	
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	inst.OnPreLoad = OnPreload
	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)    
    -----------------
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(0, function(inst)
			if inst.mob_scale and inst.mob_scale < 2 then
				inst.Transform:SetScale(inst.mob_scale, inst.mob_scale, inst.mob_scale)
			end
		end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) ondeath(inst) end)
    end)
	
	
    return inst
	
end

return MakePlayerCharacter("sharkittenp", prefabs, assets, common_postinit, master_postinit, start_inv)
