local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/whale.zip"),
	Asset("ANIM", "anim/whale_blue_build.zip"),
	Asset("ANIM", "anim/whale_moby_build.zip"),
	
	--Asset("ANIM", "anim/whale_shiny_build_01.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

--if MOBHOUSE ~= nil and MOBHOUSE == ("Enable1" or "Enable3") then
	--start_inv = start_inv2
--end
-----------------------
--Stats--
local mob = 
{
	health = 2000,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 5,
	walkspeed = 5,
	damage = 50,
	range = 3,
	hit_range = 4,
	attackperiod = 3,
	bank = "whale",
	build = "whale_blue_build",
	scale = 1,
	stategraph = "SGwhalep",
	minimap = "whalep.tex",
	
}

local bluesounds = 
{
	death = "dontstarve_DLC002/creatures/blue_whale/death",
	hit = "dontstarve_DLC002/creatures/blue_whale/hit",
	idle = "dontstarve_DLC002/creatures/blue_whale/idle",
	breach_swim = "dontstarve_DLC002/creatures/blue_whale/beach_swim",
	sleep = "dontstarve_DLC002/creatures/blue_whale/sleep",
	rear_attack = "dontstarve_DLC002/creatures/blue_whale/rear_attack",
	mouth_open = "dontstarve_DLC002/creatures/blue_whale/mouth_open",
	bite_chomp = "dontstarve_DLC002/creatures/blue_whale/bite_chomp",
	bite = "dontstarve_DLC002/creatures/blue_whale/bite",
}

local whitesounds = 
{
	death = "dontstarve_DLC002/creatures/white_whale/death",
	hit = "dontstarve_DLC002/creatures/white_whale/hit",
	idle = "dontstarve_DLC002/creatures/white_whale/idle",
	breach_swim = "dontstarve_DLC002/creatures/white_whale/breach_swim",
	sleep = "dontstarve_DLC002/creatures/white_whale/sleep",
	rear_attack = "dontstarve_DLC002/creatures/white_whale/rear_attack",
	mouth_open = "dontstarve_DLC002/creatures/white_whale/mouth_open",
	bite_chomp = "dontstarve_DLC002/creatures/white_whale/bite_chomp",
	bite = "dontstarve_DLC002/creatures/white_whale/bite",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('whalep',
-----Prefab---------------------Chance------------
{
   -- {'whale_corpse',			1.00},   
})

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, 0.5, 0, 50) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('whalep')
	----------------------------------
	--Tags--
	inst:RemoveTag("scarytoprey") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("aquatic")
	inst.mobsleep = true
	inst.mobplayer = true --lets mod know that you're a mob.
	inst.getskins = getskins
	--inst.taunt = true
	inst.poisonimmune = true
	inst.isshiny = 0
	inst.hit_recovery = 3
	inst.sounds = bluesounds
	inst.carcass = "whale_blue_carcassp"
	inst.components.combat:SetHurtSound(inst.sounds.hit)
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 100, .75)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, true)
	
	--IA
	inst:DoTaskInTime(0, function(inst)
		if SW_PP_MODE and SW_PP_MODE == true then
			inst.components.health:SetInvincible(true)
			SendToSea(inst)
		end
	end)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("whalep", prefabs, assets, common_postinit, master_postinit, start_inv)
