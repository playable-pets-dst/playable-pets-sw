local assets=
{
	Asset("ANIM", "anim/coconade.zip"),
	Asset("ANIM", "anim/swap_coconade.zip"),

	Asset("ANIM", "anim/coconut_cannon.zip"),
	
	Asset("ANIM", "anim/coconade_obsidian.zip"),
	Asset("ANIM", "anim/swap_coconade_obsidian.zip"),
}

local prefabs =
{
	--"explode_large",
	--"explodering_fx",
}

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	end
end

local function projectile_onland(inst)
	if inst.onhitfn then
		inst.onhitfn(inst)
	else
		local fx = SpawnPrefab("collapse_small")
    	fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	end	
	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, 3, nil, GetExcludeTagsp(inst)) or {}
	for k, v in pairs(ents) do
		if inst.thrower and v.components.combat and v.components.health and not v.components.health:IsDead() and v ~= inst.thrower then
			v.components.combat:GetAttacked(inst.thrower, 50, nil, "explosive")
		end
	end	

	inst:Remove()
end

local function commonfn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("coconut_cannon")
	inst.AnimState:SetBuild("coconut_cannon")
	inst.AnimState:PlayAnimation("throw", true)
	
	MakeInventoryPhysics(inst)

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	inst:AddTag("NOCLICK")

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(50)

	inst:AddComponent("complexprojectile")
    inst.components.complexprojectile:SetHorizontalSpeed(30)
    inst.components.complexprojectile:SetGravity(-50)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0, 3, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)
	
	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
	
	inst:DoTaskInTime(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/coconadepp_throw") end)
	
	inst:DoTaskInTime(10, function(inst) inst:Remove() end)

	return inst
end

local function bannana_fn()
	local inst = commonfn()

	inst.AnimState:OverrideSymbol("coconut_cannon01", "jungleTreeGuard_build", "banana_cannon01")

	if not TheWorld.ismastersim then
        return inst
    end

	inst.onhitfn = function(inst)
		--[[if math.random() > 0.8 then
			local fx = SpawnPrefab("cave_banana")
    		fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
		else]]
			local fx = SpawnPrefab("collapse_small")
    		fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
			fx.Transform:SetScale(0.5, 0.5, 0.5)
		--end
	end

	return inst
end
return Prefab("common/inventory/treeguard_coconutp", commonfn, assets, prefabs),
	Prefab("common/inventory/treeguard_bananap", bannana_fn, assets)
