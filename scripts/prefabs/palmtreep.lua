local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/treeguard_walking.zip"),
	Asset("ANIM", "anim/treeguard_actions.zip"),
	Asset("ANIM", "anim/treeguard_attacks.zip"),
	Asset("ANIM", "anim/treeguard_idles.zip"),
	Asset("ANIM", "anim/treeguard_build.zip"),
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 1750,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 2,
	walkspeed = 2,
	damage = 150,
	range = 20,
	hit_range = 4,
	bank = "treeguard",
	build = "treeguard_build",
	attackperiod = 2,
	scale = 1,
	stategraph = "SGpalmtreep",
	minimap = "palmtreep.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('palmtreep',
-----Prefab---------------------Chance------------
{
    {"livinglog",   1.0},
    {"livinglog",   1.0},
    {"livinglog",   1.0},
    {"livinglog",   1.0},
    {"livinglog",   1.0},
    {"livinglog",   1.0},
    {"monstermeat", 1.0},
    --{"coconut",     1.0},
    --{"coconut",     1.0},
   
})

local function ShareTargetFn(dude)
    return dude:HasTag("snake") and not dude.components.health:IsDead()
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 3)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 30)
end

local function OnAttack(inst, target)
    if target ~= nil then
        for i = 0, 1 do
            local offset = Vector3(math.random(-5, 5), math.random(-5, 5), math.random(-5, 5))
			local spit = SpawnPrefab("treeguard_coconutp")
            spit.Transform:SetPosition(inst:GetPosition():Get())
			spit.thrower = inst
			spit.components.complexprojectile:Launch(target:GetPosition() + offset, inst)
        end
    end
	if target ~= nil then
		local spit = SpawnPrefab("treeguard_coconutp")
        spit.Transform:SetPosition(inst:GetPosition():Get())
		spit.thrower = inst
		spit.components.complexprojectile:Launch(target:GetPosition(), inst)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 2, nil, 0) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('palmtreep')
	----------------------------------
	--Tags--
	inst:AddTag("epic")
    inst:AddTag("monster")
	inst.mobsleep = true
	inst.mobplayer = true --lets mod know that you're a mob.
	
	inst.getskins = getskins
	inst.hit_recovery = 1
	
	local body_symbol = "marker"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1000, 0.5)
	inst.Physics:SetMass(99999)
    inst.DynamicShadow:SetSize(4, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.OnAttack = OnAttack
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
end

return MakePlayerCharacter("palmtreep", prefabs, assets, common_postinit, master_postinit, start_inv)
