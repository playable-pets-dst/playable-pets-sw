local MakePlayerCharacter = require "prefabs/player_common"

--Fix weird graphical quirks with this hound.

local assets = 
{
	Asset("ANIM", "anim/hound_basic.zip"),
	Asset("ANIM", "anim/hound.zip"),
    Asset("ANIM", "anim/hound_red.zip"),
    Asset("ANIM", "anim/hound_ice.zip"),
	Asset("ANIM", "anim/hound_venom_build.zip"),
    Asset("SOUND", "sound/hound.fsb"),
	--Asset("ANIM", "anim/ghost_waxwell_build.zip"),
}
local prefabs = {
	"houndstooth",
	"monstermeat",	
    }
local start_inv = {
	"houndstooth",
	"houndstooth",
}

local start_inv2 = 
{
	"houndstooth",
	"houndstooth",
	--"houndhome",
}

--if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	--start_inv = start_inv2
--end

SetSharedLootTable( 'houndvenom',
{
	{'monstermeat',			 1.00},
	{'houndstooth',			 0.50},
	{'houndstooth',			 0.50},
	
})

local function SetInspect(inst)
	local other = data.target
	if other and other:HasTag("inspectable") then
		return BufferedAction(inst, self, ACTIONS.TAUNT)
	end
end

local function RestoreNightImmunity(inst)

	inst:DoTaskInTime(3, function(inst) 
	inst.Light:Enable(true)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(.1)
	inst.Light:SetIntensity(.1)
	inst.Light:SetColour(245/255,40/255,0/255)
	end, inst)
end

local function onhitotherfn(inst, other)
	PlayablePets.SetPoison(inst, other)
end

local function setChar(inst)
	if not inst:HasTag("playerghost") then
    inst.AnimState:SetBank("hound")
    inst.AnimState:SetBuild("hound_venom_build")
    inst:SetStateGraph("SGhoundvenomp")
	end
	if MONSTERHUNGER== "Disable" then
		inst.components.hunger:Pause()
	end
	inst.components.sanity.ignore = true
	inst.components.locomotor.runspeed = (TUNING.HOUND_SPEED)
	inst.components.locomotor.fasteronroad = false
	
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns health to max health upon reviving
end

local common_postinit = function(inst) 
	inst.soundsname = "wilson"

	inst.MiniMapEntity:SetIcon( "houndvenomp.tex" )

	inst:DoTaskInTime(0, function()
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (TUNING.HOUND_SPEED)
end


local master_postinit = function(inst) 
    inst.components.health:SetMaxHealth(150)
	inst.components.hunger:SetMax(150)
	--inst:AddComponent("healthRegenerate")
	inst.components.hunger:SetRate(0.15)
	inst.components.combat.playerdamagepercent = MOBPVP
	inst.components.combat.pvp_damagemod = 1 --disables second pvp override.
    --inst.components.combat:SetAttackPeriod(TUNING.HOUND_ATTACK_PERIOD)
	inst.components.combat:SetRange(TUNING.WORM_ATTACK_DIST)
    inst.components.locomotor.runspeed = (TUNING.HOUND_SPEED)
	inst.components.combat:SetDefaultDamage(25)
	inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hit")
	--inst.components.temperature.hurtrate = 0
    inst.components.temperature.inherentinsulation = -TUNING.INSULATION_SMALL
	inst.AnimState:SetBank("hound")
	inst.AnimState:SetBuild("hound_venom_build")	
	inst.OnSetSkin = function(skin_name)
    inst.AnimState:SetBuild("hound_venom_build")
	inst:SetStateGraph("SGhoundvenomp")
	
	inst:ListenForEvent("ms_respawnedfromghost", DontTriggerCreep)
    DontTriggerCreep(inst)
	
	
	end
	inst:SetStateGraph("SGhoundvenomp")
	inst.components.locomotor:SetShouldRun(true)
	inst.components.talker:IgnoreAll()
	if MONSTERHUNGER== "Disable" then
		inst.components.hunger:Pause()
	end
	inst.components.sanity.ignore = true
	
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('houndvenom')
	
	
	inst:AddTag("hound")
	inst:AddTag("monster")  
	inst:AddTag("character")
	inst.taunt2 = true
	inst.mobplayer = true
	inst.mobsleep = true
	inst.taunt = true
	inst.poisonimmune = true
	
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()


    inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetCanEatHorrible()
	inst.components.eater:SetAbsorptionModifiers(1,1.3,1.3)
	
	inst.components.eater.strongstomach = true -- can eat monster meat!
	
	--MakeMediumFreezableCharacter(inst, "hound_body")
	MakeMediumBurnableCharacter(inst, "hound_body")
    
	
	------------------
    inst.components.combat.onhitotherfn = onhitotherfn
    ------------------

	
  	local light = inst.entity:AddLight()
  	inst.Light:Enable(true)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.9)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,150/255)
	
	inst.components.talker.colour = Vector3(127/255, 0/255, 0/255)
	inst:ListenForEvent("respawnfromghost", RestoreNightImmunity)

    inst:DoTaskInTime(0, setChar)
    inst.ghostbuild = "ghost_monster_build"
    inst:ListenForEvent("respawnfromghost", function()
		inst:DoTaskInTime(3, function()  inst.components.health:SetInvincible(false)
            if inst.components.playercontroller ~= nil then
				inst.components.playercontroller:EnableMapControls(true)
				inst.components.playercontroller:Enable(true)
			end
			inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst:SetCameraDistance()
			inst.sg:RemoveStateTag("busy")
            SerializeUserSession(inst) 
		end)
        inst:DoTaskInTime(5, setChar)
		inst:DoTaskInTime(6, RemovePenalty)
		inst:DoTaskInTime(6, RestoreNightImmunity)
    end)

	
    return inst
	
end




return MakePlayerCharacter("houndvenomp", prefabs, assets, common_postinit, master_postinit, start_inv)
