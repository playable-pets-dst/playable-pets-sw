local MakePlayerCharacter = require "prefabs/player_common"

local prefabname = "tigersharkp"

local assets = 
{
	Asset("ANIM", "anim/tigershark_build.zip"),
    Asset("ANIM", "anim/tigershark_ground_build.zip"),
	Asset("ANIM", "anim/tigershark_ground.zip"),
    Asset("ANIM", "anim/tigershark_water_build.zip"),
	Asset("ANIM", "anim/tigershark_water.zip"),
    Asset("ANIM", "anim/tigershark.zip"),
	Asset("ANIM", "anim/tigershark_water_ripples_build.zip"),
}

local prefabs = 
{
	
}
	
local start_inv = {
	--"sharkhome",
}

local start_inv2 = 
{
	"sharkhome",
}

local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 9,
	walkspeed = 4,
	runspeed_water = 12,
	walkspeed_water = 6,
	damage = 100*2,
	attackperiod = 3,
	range = 30, --jump range, normal attack range is 5, 5
	hit_range = 0,
	bank = "tigershark",
	build = "tigershark_ground_build",
	build2 = "tigershark_water_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGtigersharkp",
	minimap = "tigersharkp.tex",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end

SetSharedLootTable('tigersharkp',
{
    {"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
	{"fishmeat", 1.00},
    
})

-------------Forge------------------------------------------

------------------------------------------------------------

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local function SetWater(inst)
    inst:ClearStateGraph()
	local pos = inst:GetPosition()
	local _isfalling = pos.y > 5
    inst:SetStateGraph("SGtigersharkp_water")
	if _isfalling then
		inst.sg:GoToState("fallwarn")
	end
	inst.components.locomotor.walkspeed = mob.walkspeed_water
    inst.components.locomotor.runspeed = mob.runspeed_water
    inst:AddTag("aquatic")
	inst.components.ppskin_manager:LoadSkin(mob, true)
	inst.AnimState:AddOverrideBuild("tigershark_water_ripples_build")
    inst.DynamicShadow:Enable(false)
end

local function SetGround(inst)
    inst:ClearStateGraph()
	local pos = inst:GetPosition()
	local _isfalling = pos.y > 5
    inst:SetStateGraph("SGtigersharkp")
	if _isfalling then
		inst.sg:GoToState("fallwarn")
	end
	inst.components.locomotor.walkspeed = mob.walkspeed
    inst.components.locomotor.runspeed = mob.runspeed
    inst:RemoveTag("aquatic")
	inst.components.ppskin_manager:LoadSkin(mob, true)
    inst.DynamicShadow:Enable(true)
end

local function OnWaterChange(inst, onwater)
	if not inst.sg:HasStateTag("specialattack") and not inst:HasTag("playerghost") then
		if onwater then
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
			SetWater(inst)
		else
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
			SetGround(inst)
		end 
		local splash = SpawnPrefab("splash_green")
		local pos = inst:GetPosition()
		splash.Transform:SetScale(4, 4, 4)
		splash.Transform:SetPosition(pos.x, pos.y, pos.z)
	end 
end

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function oncollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

----------Client Side Stuff-------------
local function ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() then
            return {ACTIONS.TIGERSHARK_JUMP}
        end
    end
    return {}
end

local function RightClickOverride(inst, target, position)
	return {actions = {ACTIONS.TIGERSHARK_JUMP}, usedefault = false}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.TIGERSHARKP)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "staves", "books"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end


local function SetSkinDefault(inst, data)
	if data then
		if inst:HasTag("aquatic") then
			inst.AnimState:SetBuild(data.build2)
		else
			inst.AnimState:SetBuild(data.build)
		end
	else
		if inst:HasTag("aquatic") then
			inst.AnimState:SetBuild(mob.build2)
		else
			inst.AnimState:SetBuild(mob.build)
		end	
	end
end
-------------------------------------------

local common_postinit = function(inst) 
	inst.soundsname = "wilson"

	inst.MiniMapEntity:SetIcon( "tigersharkp.tex" )

	inst:DoTaskInTime(0, function()
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	--[[
	inst.can_aoe_anywhere = true
	inst:ListenForEvent("setowner", OnSetOwner)
	
	inst:AddComponent("reticule")
	inst.components.reticule.ispassableatallpoints = true
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true]]
end

local function OnEat(inst, food)
	if food and food.components.edible and food.components.edible.foodtype == FOODTYPE.MEAT and not inst:HasTag("sharkitten") then
		local count = food.components.stackable and food.components.stackable:StackSize() or 1
		for i = 1, count or 1 do
			if math.random(1,100) > 50 then
				inst.components.inventory:GiveItem(SpawnPrefab("mysterymeat"))
			end
		end
	end
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/bearger/hurt")
	inst.components.combat:SetAreaDamage(5, 0.8)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(SW_PP_MODE == true and 'tigershark' or 'tigersharkp')
	----------------------------------
	--Tags--
	
	inst:AddTag("monster")
    inst:AddTag("tigershark")
    inst:AddTag("epic")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.altattack = true
	inst.shouldwalk = false
	inst.isshiny = 0
	inst.watermob = true
	inst.mobplayer = true
	
	inst.SetWater = SetWater
	inst.SetGround = SetGround
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 0 --damage is done via SG now for consistency
    inst.components.groundpounder.destructionRings = 2
	inst.components.groundpounder.groundpounddamagemult = 0
    inst.components.groundpounder.numRings = 3
	inst.components.groundpounder.noTags = {"sharkitten"}
	
	local body_symbol = "tshark_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	-------------------------------------------
	inst:DoTaskInTime(3, function(inst)
		if SW_PP_MODE == true then		
			--IA no longer uses tiletracker
		end
	end)
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible()
	if SW_PP_MODE == true then
		inst.components.eater:SetOnEatFn(OnEat)
	end
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true
	---------------------------------
	--Physics and Scale--
	inst:SetPhysicsRadiusOverride(1.33)
	MakeCharacterPhysics(inst, 1000, 1.33)
    inst.DynamicShadow:SetSize( 6, 3 )
    inst.DynamicShadow:Enable(false)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, nil, nil, true)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
        function(inst)
			if not inst:HasTag("playerghost") then
				OnWaterChange(inst, true)
			end	
        end)
		inst.components.amphibiouscreature:SetExitWaterFn(
		function(inst)
			if not inst:HasTag("playerghost") then
				OnWaterChange(inst, false)
			end
		end)
	end
	
	inst.Physics:SetCollisionCallback(oncollide)
    ---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function()
		PlayablePets.RevRestore(inst, mob)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("tigersharkp", prefabs, assets, common_postinit, master_postinit, start_inv)
