local MakePlayerCharacter = require "prefabs/player_common"



local assets = 
{
	Asset("ANIM", "anim/twister_build.zip"),
    Asset("ANIM", "anim/twister_basic.zip"),
    Asset("ANIM", "anim/twister_actions.zip"),
    Asset("ANIM", "anim/twister_seal.zip"),
}
local prefabs = 
{	
   
}
local start_inv = 
{
	
}

local getskins = {"1"}

-----------------------
--Stats--
local mob = 
{
	health = 6000,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 13,
	walkspeed = 4,
	damage = 75*2,
	range = 6,
	hit_range = 6,
	attackperiod = 3,
	bank = "twister",
	build = "twister_build",
	scale = 1,
	stategraph = "SGtwisterp",
	minimap = "twisterp.tex",
}
-----------------------

if SW_PP_MODE == true then
	SetSharedLootTable('twisterp',
	{
		{"turbine_blades",   1.00},
   
	})
else
	SetSharedLootTable('twisterp',
	{
		-- {'livingwood',             1.00},
   
	})
end	

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 and not other:HasTag("frozen") then
		if not other.prefab == "beequeenhive" and not other.prefab == "toadstool_cap" then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
		else
		other.components.workable:Destroy(inst)
		end
	end
end

local function oncollide(inst, other)
	if other == nil --[[or not other:HasTag("tree")]] then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

-------------------------------------------------------
--Knockback--

local function OnHitOther(inst, other) --knockback
	if inst.sg:HasStateTag("specialattack") then
		--PlayablePets.KnockbackOther(inst, other, -5)
	end
end
------------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon( "twisterp.tex" )

	inst:DoTaskInTime(0, function()
		if ThePlayer then
			ThePlayer:EnableMovementPrediction(false)
		end
	end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.TWISTERP)
	
	inst.mobsleep = false
	inst.components.health:StopRegen()
	
	if inst.components.vacuump then
		inst.components.vacuump:TurnOff()
	end
	
	inst.acidimmune = true
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "staves", "books"})
	end)
	
	inst.components.combat:SetDamageType(TUNING.FORGE.DAMAGETYPES.MAGIC)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	inst.CanVacuum = true
	inst:AddComponent("vacuump")
    inst.components.vacuump:TurnOn()
    inst.components.vacuump.vacuumradius = 5
	inst.components.vacuump.ignoreplayer = true
	inst.vacuumhit = 0	
    ------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetAreaDamage(6, 0.8)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------	
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('twisterp')  
    ------------------
	inst:AddTag("epic")
    inst:AddTag("monster")

	inst.userstorm = false
	
	
	inst.hit_recovery = 0.75
	inst.isshiny = 0
	inst.getskins = getskins
	inst.shouldwalk = true
	inst.poisonimmune = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/twister_active", "wind_loop")
    inst.SoundEmitter:SetParameter("wind_loop", "intensity", 0)

    inst.AnimState:Hide("twister_water_fx")
	
	
	inst.isshiny = 0
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)	
	-------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	MakeGhostPhysics(inst, 1000, .5)
	inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank)
	inst.components.locomotor:SetAllowPlatformHopping(false)
	if inst.components.amphibiouscreature then
	inst.components.amphibiouscreature:SetEnterWaterFn(
        function(inst)
			inst.AnimState:Show("twister_water_fx")
        end)
	inst.components.amphibiouscreature:SetExitWaterFn(
		function(inst)
			inst.AnimState:Hide("twister_water_fx")
		end)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("twisterp", prefabs, assets, common_postinit, master_postinit, start_inv)
