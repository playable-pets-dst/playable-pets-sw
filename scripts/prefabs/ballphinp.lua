local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/ballphin.zip"),
	--Asset("ANIM", "anim/whale_shiny_build_01.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

--if MOBHOUSE ~= nil and MOBHOUSE == ("Enable1" or "Enable3") then
	--start_inv = start_inv2
--end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 200,
	runspeed = 6,
	walkspeed = 6,
	damage = 30,
	attackperiod = 0,
	hit_range = 1.5,
	range = 1.5,
	bank = "ballphin",
	build = "ballphin",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGballphinp",
	minimap = "ballphinp.tex",
	
}


-----------------------
if SW_PP_MODE == true then
	--Loot that drops when you die, duh.
	SetSharedLootTable('ballphinp',
	-----Prefab---------------------Chance------------
	{
		{'fish_small',			1.00},
		{'fish_small',			1.00},
		{'dorsalfin',			1.00},
		{'messagebottleempty',	0.33},  
	})
else
	SetSharedLootTable('ballphinp',
	-----Prefab---------------------Chance------------
	{
		{'smallmeat',			1.00},
		{'smallmeat',			1.00},  
	})	
end	

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("ballphin") and not dude.components.health:IsDead() end, 3)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.BALLPHIN)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 30, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('ballphinp')
	----------------------------------
	--Tags--
	inst:RemoveTag("scarytoprey") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("ballphin")
	inst:AddTag("aquatic")
	
	inst.taunt = true
	inst.mobplayer = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "sharx_fin"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--SanityAura--
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, 0.5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, false, true)
	
	--IA
	inst:DoTaskInTime(0, function(inst)
		if SW_PP_MODE and SW_PP_MODE == true then
			inst.components.health:SetInvincible(true)
			SendToSea(inst)
		end
	end)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
  	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

if SKIN_RARITY_COLORS.ModMade ~= nil and SKIN_RARITY_COLORS.SWSkins ~= nil then
AddModCharacterSkin("ballphinp", 1, {normal_skin = "bat_shiny_build_01", ghost_skin = "ghost_monster_build"}, {"deerclops_shiny_build_01", "ghost_monster_build"}, {"DEERPLAYER", "FORMAL"})
end

return MakePlayerCharacter("ballphinp", prefabs, assets, common_postinit, master_postinit, start_inv)
