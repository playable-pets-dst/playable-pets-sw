local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This was written on an outdated source of mandrake. Check with updated prefab before running!
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/toucan_build.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}
-----------------------
--Stats--
local mob = 
{
	health = 50,
	hunger = 100,
	hungerrate = 0.075, --I think .15 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 10,
	range = 1.25,
	hit_range = 2,
	attackperiod = 0,
	bank = "crow",
	build = "toucan_build",
	scale = 1,	
	stategraph = "SGbirdswp",
	minimap = "toucanp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('parrotp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00}, --we want the "mandrake" corpse. So we have him spawn at the exact location of death instead of dropping as loot. 
})

local sounds = 
	{
		takeoff = "dontstarve_DLC002/creatures/toucan/takeoff",
		chirp = "dontstarve_DLC002/creatures/toucan/chirp",
		flyin = "dontstarve/birds/flyin",
		--land = land_sound,
	}

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      ThePlayer:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('parrotp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:RemoveTag("scarytoprey")
	inst:AddTag("birdwhisperer")
    --inst:AddTag("smallcreature")	
	
	inst.mobsleep = true
	inst.taunt = true
	inst.mobplayer = true
	inst.taunt2 = true
	inst._isflying = false
	inst.skinn = 0
	inst.isshiny = 0
	inst.getskins = getskins
	
	inst.sounds = sounds	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,5,5) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)

    inst.DynamicShadow:SetSize(1, .75)
    inst.DynamicShadow:Enable(true)
    inst.Transform:SetTwoFaced()
	
	local body_symbol = "crow_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.components.locomotor:SetAllowPlatformHopping(false)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
     ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("toucanp", prefabs, assets, common_postinit, master_postinit, start_inv)
