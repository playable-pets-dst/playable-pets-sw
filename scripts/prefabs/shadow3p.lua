local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	 Asset("ANIM", "anim/shadow_insanity_water1.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

--if MOBHOUSE ~= nil and MOBHOUSE == ("Enable1" or "Enable3") then
	--start_inv = start_inv2
--end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 10,
	runspeed = 5,
	walkspeed = 5,
	damage = 50,
	attackperiod = 3,
	range = 2,
	hit_range = 3,
	bank = "shadowseacreature",
	build = "shadow_insanity_water1",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGshadow3p",
	minimap = "shadow3p.tex",
	
}

local data =
    {
        name="swimminghorror",
        build = "shadow_insanity_water1",
        bank = "shadowseacreature",
        num = 1,
        speed = TUNING.CRAWLINGHORROR_SPEED,
        health=TUNING.CRAWLINGHORROR_HEALTH,
        damage=TUNING.CRAWLINGHORROR_DAMAGE,
        attackperiod = TUNING.CRAWLINGHORROR_ATTACK_PERIOD,
        sanityreward = TUNING.SANITY_MED
    }

local sounds = 
    {
        attack = "dontstarve/sanity/creature"..data.num.."/attack",
        attack_grunt = "dontstarve/sanity/creature"..data.num.."/attack_grunt",
        death = "dontstarve/sanity/creature"..data.num.."/die",
        idle = "dontstarve/sanity/creature"..data.num.."/idle",
        taunt = "dontstarve/sanity/creature"..data.num.."/taunt",
        appear = "dontstarve/sanity/creature"..data.num.."/appear",
        disappear = "dontstarve/sanity/creature"..data.num.."/dissappear",
    }
	

-------------------
--Loot that drops when you die, duh.
SetSharedLootTable('shadow3p',
-----Prefab---------------------Chance------------
{
    {'nightmarefuel',			1.00},   
	{'nightmarefuel',			1.00}, 
})

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 99999) --fire, acid, poison, freeze (flat value, not a multiplier)	
	inst.components.health:StartRegen(5, 4)
	----------------------------------
	inst.components.hunger:Pause()
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('shadow3p')
	----------------------------------
	--Tags--
	inst.mobplayer = true --lets mod know that you're a mob.
	
	inst:AddTag("amphibious")
	inst:AddTag("monster")
	inst.taunt = true
	inst.poisonimmune = true
	inst.acidimmune = true
	inst.inkimmune = true
	inst.debuffimmune = true
	inst.isshiny = 0
	inst.data = data
	inst.sounds = sounds
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, 1.5)
	RemovePhysicsColliders(inst)
	inst.Physics:SetCollisionGroup(COLLISION.SANITY)
	inst.Physics:CollidesWith(COLLISION.SANITY)
	inst.Physics:CollidesWith(COLLISION.WORLD)
	PlayablePets.SetAmphibious(inst, nil, nil, true)

    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst.components.sanity.ignore = true
    ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) inst.AnimState:SetMultColour(1, 1, 1, 0.5) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) inst.components.hunger:Pause() end)
    end)
	
    return inst
	
end




return MakePlayerCharacter("shadow3p", prefabs, assets, common_postinit, master_postinit, start_inv)
