local assets =
{
	Asset("ANIM", "anim/whale_carcass.zip"),
	Asset("ANIM", "anim/whale_carcass_build.zip"),
	
	Asset("ANIM", "anim/whale_moby_carcass_build.zip"),
	
	Asset("MINIMAP_IMAGE", "whale_carcass"),
}

--NOTE: You were doing the explosion loot stuff. ruinsbat is an invalid prefab name, check the prefab names. Once thats done it should be good!.

local prefabs =
{
	"fish_raw",
	"boneshard",
	"coconadep",
	"tophat",
	"sail",
	"gashat",
	"blowdart_sleep",
	"blowdart_poison",
	"blowdart_fire",
	"cutlass",
	"clothsail",
	"lobster_dead",
	"spear_launcher",
	"coconut",
	"boat_lantern",
	"bottlelantern",
	"telescope",
	"captainhat",
	"piratehat",
	"spear",
	"seatrap",
	"machete",
	"messagebottleempty",
	"fish_raw",
	"boneshard",
	"seaweed",
	"seashell",
	"jellyfish",
	"coral",
	"harpoon",
	"blubber",
	"bamboo",
	"vine",
}

local alwaysloot_blue = {"fishmeat","fishmeat","fishmeat","fishmeat"}
local alwaysloot_white = {"fishmeat","fishmeat","fishmeat","fishmeat", "spear"}

local WHALE_ROT_TIME =
	    {
	        {base=2*300, random=0.5*300}, -- bloat1
	        {base=2*300, random=0.5*300}, -- bloat2
	        {base=1*300, random=0.5*300}, -- bloat3
	    }
		--dunno how to add things to tuning because im dumb.
		--day_time 's default value is 300, just using that until TUNING.WHALE_ROT_TIME becomes a thing I guess.

local loots = {
	{
		-- low % items, 2 of these are picked
		"coconadep",
		--"raft", -- flies away
		"tophat",
		"sail",
		"gashat",
		--"boatcannon",
		"blowdart_sleep",
		"blowdart_poison",
		"blowdart_fire",
		"cutlass",
	},
	{
		--MEDIUM % ITEMS, 3 of these
		"clothsail",
		"lobster_dead",
		"spear_launcher",
		"coconut",
		"boat_lantern",
		"bottlelantern",
		"telescope",
		"captainhat",
		"piratehat",
		"spear",
		"seatrap",
		"machete",
		"messagebottleempty",
	},
	{
		--HIGH % ITEMS, 4 of these
		"blubber",
		"fish_raw",
		"boneshard",
		"seaweed",
		-- "seashell",
		"jellyfish",
		"coral",
		"vine",
		"bamboo",
	},
}

local lootsp = {
	{
		-- low % items, 2 of these are picked
		"gunpowder",
		--"raft", -- flies away
		"tophat",
		"cane",
		"ruinshat",
		--"boatcannon",
		"blowdart_sleep",
		--"blowdart_poison",
		"blowdart_fire",
		"ruins_bat",
	},
	{
		--MEDIUM % ITEMS, 3 of these
		"boards",
		"fish",
		"hambat",
		"pinecone",
		"lantern",
		"minerhat",
		"trinket_13",
		"trinket_11",
		"trinket_7",
		"spear",
		"axe",
		"pickaxe",
		"fish",
	},
	{
		--HIGH % ITEMS, 4 of these
		--"blubber",
		"fish",
		"berries",
		"cutreeds",
		"eel",
		"rocks",
		"cutgrass",
		"twigs",
	},
}

local bluesounds = 
{
	stinks = "dontstarve_DLC002/creatures/blue_whale/bloated_stinks",
	bloated1 = "dontstarve_DLC002/creatures/blue_whale/bloated_plump_1",
	bloated2 = "dontstarve_DLC002/creatures/blue_whale/bloated_plump_2",
	explosion = "dontstarve_DLC002/creatures/blue_whale/whale_explosion",
	hit = "dontstarve_DLC002/creatures/blue_whale/blubber_hit",
}

local whitesounds = 
{
	stinks = "dontstarve_DLC002/creatures/white_whale/bloated_stinks",
	bloated1 = "dontstarve_DLC002/creatures/white_whale/bloated_plump_1",
	bloated2 = "dontstarve_DLC002/creatures/white_whale/bloated_plump_2",
	explosion = "dontstarve_DLC002/creatures/white_whale/whale_explosion",
	hit = "dontstarve_DLC002/creatures/white_whale/blubber_hit",
}

local growth_stages

local function workcallback(inst, worker, workleft)
	inst.SoundEmitter:PlaySound(inst.sounds.hit)
	inst.AnimState:PlayAnimation("idle_trans2_3")
	inst.AnimState:PushAnimation("idle_bloat3",true)
end

local function OnSave(inst, data)
    data.isshiny = inst.isshiny or 0
   -- data.CanCharge = inst.CanCharge
    
end

local function OnLoad(inst, data)
    if data then
        inst.isshiny = data.isshiny or 0
        --inst.CanCharge = data.CanCharge
    end
end

local function workfinishedcallback(inst, worker)
	-- inst.components.growable:SetStage(#growth_stages)
	inst.components.growable:DoGrowth()
	--inst.sg:GoToState("explode")
end

growth_stages =
{
	{
		name = "bloat1",
		time = function(inst)
			return GetRandomWithVariance(WHALE_ROT_TIME[1].base, WHALE_ROT_TIME[1].random)
		end,
		fn = function (inst)
			inst.sg:GoToState("bloat1_pre")
			inst.components.workable:SetWorkable(false)
		end,
		growfn = function(inst)
		end,
	},
	{
		name = "bloat2",
		time = function(inst)
			return GetRandomWithVariance(WHALE_ROT_TIME[1].base, WHALE_ROT_TIME[1].random)
		end,
		fn = function (inst)
			inst.sg:GoToState("bloat2_pre")
			inst.components.workable:SetWorkable(false)
		end,
		growfn = function(inst)
		end,
	},
	{
		name = "bloat3",
		time = function(inst)
			return GetRandomWithVariance(WHALE_ROT_TIME[2].base, WHALE_ROT_TIME[2].random)
		end,
		fn = function (inst)
			inst.sg:GoToState("bloat3_pre")
			inst.components.workable:SetWorkable(true)
		end,
		growfn = function(inst)
		end,
	},
	{
		name = "explode",
		time = function(inst)
			return GetRandomWithVariance(WHALE_ROT_TIME[2].base, WHALE_ROT_TIME[2].random)
		end,
		fn = function (inst)
			inst.components.workable:SetWorkable(false)
		end,
		growfn = function(inst)
			-- guarding against ending up here multiple times due to F9 testing
			if not inst.alreadyexploding then
				inst.alreadyexploding = true
				inst.AnimState:PlayAnimation("explode", false)
				inst.SoundEmitter:PlaySound(inst.sounds.explosion)
				
				inst:DoTaskInTime(57*FRAMES, function (inst)
					--print("check 1 passed")
					--inst.components.explosive:OnBurnt()
				end )

				inst:DoTaskInTime(58*FRAMES, function(inst)
					--print("check 2 passed")

					local i = 1
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 3 + (math.random() * 8)
						local loot = GetRandomItem(lootsp[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 20+(math.random() * 5), vz)
					end
				end)
				inst:DoTaskInTime(60*FRAMES, function(inst)
					--print("check 3 passed")
					local i = 2
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 4 + (math.random() * 8)
						local loot = GetRandomItem(lootsp[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 25+(math.random() * 5), vz)
					end
				end)
				inst:DoTaskInTime(63*FRAMES, function(inst)
					--print("check 4 passed")
					local i = 3
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 6 + (math.random() * 8)
						local loot = GetRandomItem(lootsp[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 30+(math.random() * 5), vz)
					end

					inst.components.lootdropper:DropLoot(inst:GetPosition())
					--inst:Remove()
				end)

				inst:ListenForEvent("animqueueover", function (inst)
					inst:Remove()
				end)
			end
		end,
	},
}

local function fn(Sim)
	local inst = CreateEntity()	
	local trans = inst.entity:AddTransform()
	local sound = inst.entity:AddSoundEmitter()
	local minimap = inst.entity:AddMiniMapEntity()
	inst.entity:AddPhysics()
	inst.entity:AddNetwork()
	minimap:SetIcon("whale_carcass.png")

	MakeObstaclePhysics(inst, .7)

	inst.entity:AddAnimState()
	inst.AnimState:SetBank("whalecarcass")
	inst.AnimState:SetBuild("whale_carcass_build")
	
	inst.entity:SetPristine()

    --if not TheWorld.ismastersim then
        --return inst
    --end
	
	if inst.isshiny == nil then
	inst.isshiny = 0
	end

	inst:AddComponent("inspectable")
	inst:AddComponent("lootdropper")

	inst:AddComponent("growable")
	inst.components.growable.stages = growth_stages

	inst:AddComponent("explosive")
	inst.components.explosive.lightonexplode = false
	inst.components.explosive.noremove = true

	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.CHOP)

	inst.components.workable:SetOnWorkCallback(workcallback)
	inst.components.workable:SetOnFinishCallback(workfinishedcallback)
	inst.components.workable:SetWorkable(false)

	inst:SetStateGraph("SGwhalecarcassp")

	return inst
end

local function bluefn(Sim)
	local inst = fn(Sim)

	if inst.isshiny ~= nil and inst.isshiny > 0 then
	inst.AnimState:SetBuild("whale_carcass_shiny_build_0"..inst.isshiny)
	else
	inst.AnimState:SetBuild("whale_carcass_build")
	end

	inst.components.lootdropper:SetLoot(alwaysloot_blue)

	inst.sounds = bluesounds

	inst.components.growable.stages = growth_stages
	inst.components.growable:SetStage(1)
	inst.components.growable:StartGrowing()

	inst.components.workable:SetWorkLeft(8)
	inst.components.workable:SetWorkable(false)
	inst.components.explosive.explosivedamage = 25
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	return inst
end

local function whitefn(Sim)
	local inst = fn(Sim)

	inst.Transform:SetScale(1.25, 1.25, 1.25)
	if inst.isshiny ~= nil and inst.isshiny > 0 then
	inst.AnimState:SetBuild("whale_moby_carcass_shiny_build_0"..inst.isshiny)
	else
	inst.AnimState:SetBuild("whale_moby_carcass_build")
	end

	inst.components.lootdropper:SetLoot(alwaysloot_white)

	inst.sounds = whitesounds
	

	inst.components.growable.stages = growth_stages
	inst.components.growable:SetStage(1)
	inst.components.growable:StartGrowing()

	inst.components.workable:SetWorkLeft(10)
	inst.components.workable:SetWorkable(false)
	inst.components.explosive.explosivedamage = 35
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	return inst
end


return Prefab( "common/objects/whale_blue_carcassp", bluefn, assets, prefabs),
	   Prefab( "common/objects/whale_white_carcassp", whitefn, assets, prefabs)
