local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--ToDo: Reduce attack time, Snake is too slow and too frail to even fight things.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/crocodog_basic.zip"),
	Asset("ANIM", "anim/crocodog.zip"),
	Asset("ANIM", "anim/crocodog_poison.zip"),
	Asset("ANIM", "anim/crocodog_water.zip"),
    Asset("ANIM", "anim/crocodog_basic_water.zip"),
	Asset("ANIM", "anim/watercrocodog.zip"),
	Asset("ANIM", "anim/watercrocodog_poison.zip"),
	Asset("ANIM", "anim/watercrocodog_water.zip"),
}

local getskins = {}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 225,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = TUNING.HOUND_SPEED,
	walkspeed = TUNING.HOUND_SPEED,
	damage = 50,
	damage2 = 25, --damage gets reduced to compensate for poison.
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "crocodog",
	build = "crocodog_poison",
	build2 = "watercrocodog_poison",
	scale = 1,
	stategraph = "SGcrocodogp",
	minimap = "poisoncrocodogp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('crocodogp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',            	 1.00}, 
	{'houndstooth',				 0.50},
	{'houndstooth',				 0.50},
	{'venomglandp',             1.00},  
   
})

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("hound") and not dude.components.health:IsDead() end, 3)
    end
end

local function OnHitOther(inst, other)
	PlayablePets.SetPoison(inst, other)
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
    ------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, nil, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/crocodog/hit")	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('crocodogp')
	----------------------------------
	--Tags--
	inst:AddTag("hound")
	inst:AddTag("crocodog")
    inst:AddTag("monster")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.getskins = getskins
		

	local body_symbol = "Crocodog_Body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, 0.5)
    inst.DynamicShadow:SetSize(3, 1.5)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank.."_water")
	inst.components.locomotor:SetAllowPlatformHopping(false)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					if inst.DynamicShadow then
						inst.DynamicShadow:Enable(false)
					end
					local splash = SpawnPrefab("splash_green")
					local ent_pos = Vector3(inst.Transform:GetWorldPosition())
					splash.Transform:SetPosition(ent_pos.x, ent_pos.y, ent_pos.z)
				
					inst.AnimState:SetBuild(mob.build2)
				
					inst.sg:GoToState("idle")
				end	
			end)
		inst.components.amphibiouscreature:SetExitWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					if inst.DynamicShadow then
						inst.DynamicShadow:Enable(true)
					end
					
					inst.AnimState:SetBuild(mob.build)
				
					local splash = SpawnPrefab("splash_green")
					local ent_pos = Vector3(inst.Transform:GetWorldPosition())
					splash.Transform:SetPosition(ent_pos.x, ent_pos.y, ent_pos.z)
				
					inst.sg:GoToState("idle")
				end
			end)	
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("poisoncrocodogp", prefabs, assets, common_postinit, master_postinit, start_inv)
