require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/monkey_barrel_tropical.zip"),
   Asset("ANIM", "anim/ui_chester_shadow_3x4.zip"),
}

local prefabs =
{
    "collapse_small",
}

local function onopen(inst, data)
    if not inst:HasTag("burnt") and (data.doer and data.doer:HasTag("monkey")) then
        --inst.AnimState:PlayAnimation("move1")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
	else
		inst.components.container:Close()
    end
end 

local function onclose(inst)
    if not inst:HasTag("burnt") then
        --inst.AnimState:PlayAnimation("move2")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
    end
end

local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst.components.lootdropper:DropLoot()
    if inst.components.container ~= nil then
        inst.components.container:DropEverything()
    end
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        --inst.AnimState:PlayAnimation("hit")
        --inst.AnimState:PushAnimation("closed", false)
        if inst.components.container ~= nil then
            inst.components.container:DropEverything()
            inst.components.container:Close()
        end
    end
end

local function onbuilt(inst)
	inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle")
    inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/barrel_rattle")
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function fn()
    
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        inst.MiniMapEntity:SetIcon("monkey_barrel.png")
		
		MakeObstaclePhysics(inst, 1)
		
        inst:AddTag("structure")
        inst:AddTag("chest")
		inst:AddTag("monkeychest")
        inst.AnimState:SetBank("barrel_tropical")
        inst.AnimState:SetBuild("monkey_barrel_tropical")
        inst.AnimState:PlayAnimation("idle", true)

        --MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")
        inst:AddComponent("container")
        inst.components.container:WidgetSetup("monkeybarrel2_p")
		
			inst.components.container.onopenfn = onopen
			inst.components.container.onclosefn = onclose

        inst:AddComponent("lootdropper")
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(2)
        inst.components.workable:SetOnFinishCallback(onhammered)
        inst.components.workable:SetOnWorkCallback(onhit) 

        AddHauntableDropItemOrWork(inst)

        inst:ListenForEvent("onbuilt", onbuilt)
        MakeSnowCovered(inst)   

        MakeSmallBurnable(inst, nil, nil, true)
        MakeMediumPropagator(inst)

        inst.OnSave = onsave 
        inst.OnLoad = onload

        return inst
    end

return Prefab("monkeybarrel2_p", fn, assets, prefabs),
MakePlacer("monkeybarrel2_p_placer", "barrel_tropical", "monkey_barrel_tropical", "idle")
    
