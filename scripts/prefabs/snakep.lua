local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--ToDo: Reduce attack time, Snake is too slow and too frail to even fight things.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/snake_build.zip"),
	
	Asset("ANIM", "anim/snake_yellow_build.zip"),
	
	Asset("ANIM", "anim/snake_basic.zip"),
	
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 3 + 1.5,
	walkspeed = 3,
	damage = 40,
	range = 1.5,
	hit_range = 2.5,
	attackperiod = 0,
	bank = "snake",
	build = "snake_build",
	scale = 1,
	shiny = "snake",
	--build3 = "chester_snow_build",
	stategraph = "SGsnakep",
	minimap = "none.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('snakep',
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
})

local function ShareTargetFn(dude)
    return dude:HasTag("snake") and not dude.components.health:IsDead()
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 3)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local function SetSkinDefault(inst, data)
	if data then
		if data.scale then
			inst.AnimState:SetScale(data.scale, data.scale)
		end
		inst.AnimState:SetBuild(data.build)
	else
		inst.AnimState:SetScale(mob.scale, mob.scale)
		inst.AnimState:SetBuild(mob.build)
	end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.5, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/snake/hurt")
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('snakep')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("noplayerindicator")
	inst:AddTag("snake")
    inst:AddTag("monster")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeMediumBurnableCharacter(inst, body_symbol)	
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(4,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, 0.5)
	inst.DynamicShadow:Enable(false) 
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("snakep", prefabs, assets, common_postinit, master_postinit, start_inv)
