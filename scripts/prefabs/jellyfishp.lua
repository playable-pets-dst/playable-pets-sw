local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/jellyfish.zip"),
	Asset("ANIM", "anim/jellyfish_shiny_build_01.zip"),
	Asset("ANIM", "anim/jellyfish_valentines.zip"),
   -- Asset("ANIM", "anim/meat_rack_food.zip"),
   -- Asset("INV_IMAGE", "jellyJerky"),	
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

-----------------------
--Stats--
local mob = 
{
	health = 100,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 35,
	range = 2,
	hit_range = 3,
	attackperiod = 3,
	bank = "jellyfish",
	build = "jellyfish",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGjellyfishp",
	minimap = "jellyfishp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
if SW_PP_MODE == true then
	SetSharedLootTable('jellyfishp',
	-----Prefab---------------------Chance------------
	{
		{'jellyfish_dead',             1.00}, 
   
	})
else
	SetSharedLootTable('jellyfishp',
	-----Prefab---------------------Chance------------
	{
		{'fish',			1.00},  
	})
end	

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker and data.attacker.components.combat and data.attacker.components.health and not data.attacker.components.health:IsDead() and inst:IsNear(data.attacker, mob.hit_range) and data.attacker ~= inst then
	   data.attacker.components.combat:GetAttacked(inst, (TheNet:GetServerGameMode() == "lavaarena" and data.damage) and data.damage/2 or 15, nil, "electric")
    end
end

local function playshockanim(inst)
    if inst:HasTag("aquatic") then
        inst.AnimState:PlayAnimation("idle_water_shock")
        inst.AnimState:PushAnimation("idle_water", true)
        inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/jellyfish/electric_water")
    else
        inst.AnimState:PlayAnimation("idle_ground_shock")
        inst.AnimState:PushAnimation("idle_ground", true)
        inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/jellyfish/electric_land")
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 30, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('jellyfishp')
	----------------------------------
	--Tags--
	inst.mobplayer = true --lets mod know that you're a mob.
	inst:AddTag("jellyfish")
	inst:AddTag("aquatic")
	inst.mobsleep = true
	inst.taunt = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "jelly"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--SanityAura--
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, 1)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank, false, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------	
	--Light and Character Functions. Don't Touch.--
    	
  	local light = inst.entity:AddLight() --Just added to make immune to grue. No visible or useful light.
  	inst.Light:Enable(false)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.9)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,150/255)
	
	inst.components.grue:AddImmunity("mobplayer")

    ------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
  	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("jellyfishp", prefabs, assets, common_postinit, master_postinit, start_inv)
