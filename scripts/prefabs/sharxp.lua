local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--WARNING: If you're reading this then note that this mob might be VERY UNSTABLE.
--They are only here for SW port.

--ToDo: Make a function that check whether user is on the ground or not. Obviously you want it as close
--to the starting island as possible.

--Might as well make a variant of that function in the chance that SW won't be ported over, and maybe make it compatible with SW port mod.

---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/sharx.zip"),
	Asset("ANIM", "anim/sharx_build.zip"),
}

local getskins = {"1", "3"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 175,
	hunger = 100,
	hungerrate = 0.13, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = TUNING.HOUND_SPEED,
	walkspeed = 5,
	damage = 35,
	attackperiod = 0,
	hit_range = 3,
	range = 2,
	bank = "sharx",
	build = "sharx_build",
	scale = 1,
	stategraph = "SGsharxp",
	minimap = "sharxp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('sharxp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
	--{'sharkfin',             0.1}, --SW only.
   
})

local function getrandomposition(inst)
		local pt = inst:GetPosition()
        local result_offset = FindValidPositionByFan(50, 50, 50, function(offset)
            local test_point = pt + offset
			print("Test Points: "..test_point.x..", "..test_point.z)
            if IsOnWater(test_point.x, test_point.y, test_point.z) then
              return true
            end
            return false 
          end)
        local newpt = result_offset and pt + result_offset or nil
		print(newpt)
		return newpt
end

local function SendToSea(inst)
	local ground = TheWorld
	
	if not inst:GetIsOnWater() then
    local pt = getrandomposition(inst)--Point(inst.Transform:GetWorldPosition())
		if pt ~= nil then
            pt.y = 0
            inst.Physics:Stop()
				
            inst.Physics:Teleport(pt.x,pt.y,pt.z)
			inst:RemoveComponent("sailor")
				
        else    --inst:Hide()
			print("PP SW: Couldn't find an ocean position for a mobplayer!")
		end	
	end
	inst.components.health:SetInvincible(false)
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("sharx") and not dude.components.health:IsDead() end, 3)
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
		inst.swmodeignore = data.swmode or nil
		--SkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
	data.swmode = inst.swmodeignore or nil
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 30, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('sharxp')
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("sharx")
    inst:AddTag("aquatic") --this might be important for SW.
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.isshiny = 0
	inst.swmodeignore = false
	inst.taunt2 = true
	
	inst.shouldwalk = false
	inst.getskins = getskins
	
	local body_symbol = "sharx_fin"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	--Leaving this for IA for right now.
	inst:DoTaskInTime(0, function(inst)
		if SW_PP_MODE and SW_PP_MODE == true then
			inst.components.health:SetInvincible(true)
			SendToSea(inst)
		end
	end)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, 0.5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, "sharx", "sharx", false, true)

	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
  	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
end

return MakePlayerCharacter("sharxp", prefabs, assets, common_postinit, master_postinit, start_inv)
