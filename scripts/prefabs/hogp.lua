local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This one was a pain to port over. Will have to update how this character
--charges when SW comes around.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
    Asset("ANIM", "anim/ds_pig_charge.zip"),
    Asset("ANIM", "anim/wildbore_build.zip"),
	
	Asset("ANIM", "anim/pigspotted_build.zip"),
    Asset("ANIM", "anim/werepig_wildbore_build.zip"),
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"swmonster_wpn",
}
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 3,
	damage = 50,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "pigman",
	build = "wildbore_build",
	scale = 1,
	--build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGhogp",
	minimap = "hogp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('hogp',
-----Prefab---------------------Chance------------
{
    {'meat',             1.00}, 
	{'pigskin',			 0.50},
	--{'snakeoil',             0.01}, --SW only.
   
})

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local function ontalk(inst, script)
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/wild_boar/grunt")
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "ghost"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "ghost"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "ghost"}
	end
end

local function NotExcluded(inst)
	local passcheck = true
	local table = GetExcludeTags(inst)
	for i, v in pairs(table) do
		if inst:HasTag(v) then
			passcheck = false
		end
	end
	print(passcheck)
	return passcheck
end

local function onothercollide(inst, other)
    if not other:IsValid() then
        return	
    else
		if not inst.recentlycharged[other] and other.components.health ~= nil and not other.components.health:IsDead() then
			inst.recentlycharged[other] = true
			inst:DoTaskInTime(3, ClearRecentlyCharged, other)
			inst.sg:GoToState("charge_attack", other)
		end
    end
end

local function oncollide(inst, other)
	if inst.sg:HasStateTag("charging") and NotExcluded(other) then
		inst:DoTaskInTime(2 * FRAMES, onothercollide, other)
	end	
end

local function SetNormalPig(inst)
    inst:RemoveTag("werepig")
    inst:RemoveTag("guard")

    inst:SetStateGraph("SGhogp")
	inst.sg:GoToState("transformNormal")
	
	inst.components.sanity:DoDelta(-75, false)
    inst.components.combat:SetDefaultDamage(mob.damage)

    inst.components.locomotor.runspeed = (mob.runspeed)
    inst.components.locomotor.walkspeed = (mob.walkspeed)

    inst.components.health:SetMaxHealth(mob.health)
    inst.components.talker:StopIgnoringAll("becamewerepig")
	inst:StopWatchingWorldState("startday")
end

local function SetWerePig(inst)
	if TheWorld.state.isnight and TheWorld.state.isfullmoon then
		inst:AddTag("werepig")
		--inst:RemoveTag("guard")
	
		inst:SetStateGraph("SGwerepigp")
		inst.sg:GoToState("transformWere")
		--inst.AnimState:SetBuild("werepig_build")
		--inst.components.sleeper:SetResistance(3)
		if inst.components.inventory ~= nil then
			inst.components.inventory:DropEverything(true)
		end

		inst.components.combat:SetDefaultDamage(100)
		--inst.components.combat:SetAttackPeriod(TUNING.WEREPIG_ATTACK_PERIOD)
		inst.components.locomotor.runspeed = 9
		inst.components.locomotor.walkspeed = 9

		--inst.components.sleeper:SetSleepTest(WerepigSleepTest)
		--inst.components.sleeper:SetWakeTest(WerepigWakeTest)

		--inst.components.lootdropper:SetLoot({ "meat", "meat", "pigskin" })
		--inst.components.lootdropper.numrandomloot = 0

		inst.components.health:SetMaxHealth(500)
		inst.components.health:DoDelta(20000, false)
		inst.components.talker:IgnoreAll("becamewerepig")
		inst:WatchWorldState("startday", SetNormalPig)
	end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.75) --fire, acid, poison, freeze (flat value, not a multiplier)		
	------------------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('hogp')
	----------------------------------
	--Tags--
	inst:WatchWorldState("isfullmoon", SetWerePig)
	
    inst:AddTag("pig")
	inst:AddTag("hogplayer")
	inst:AddTag("monster_user")
	
	inst.recentlycharged = {}
	inst.mobsleep = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.taunt = true
	inst.taunt2 = true
	inst.ischarger = false
	inst.getskins = getskins
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, 0.5)
	inst.Physics:SetCollisionCallback(oncollide)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .75)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("hogp", prefabs, assets, common_postinit, master_postinit, start_inv)
