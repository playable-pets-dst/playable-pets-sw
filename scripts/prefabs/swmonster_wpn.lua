local assets =
{
	Asset( "IMAGE", "images/inventoryimages/swmonster_wpn.tex" ),
    Asset( "ATLAS", "images/inventoryimages/swmonster_wpn.xml" ),
}

--If this is already here then its probably alright.
--Wouldn't hurt to check later though.

local function Remove(inst)
inst:DoTaskInTime(1.2, function() inst:Remove() end) --Give it time to prevent crashes.
end


local function onequip(inst, owner)
    if not owner:HasTag("monster_user") then --uh maybe should try using variables instead.
		inst:Remove()
	end
	--if not owner.monsteruser ~= nil then --This might cause a crash though so use wisely.
		--inst:Remove()
	--end	
end

local function onunequip(inst, owner)
    inst.components.weapon:SetDamage(0)
	inst.components.weapon:SetRange(1)
	inst.components.weapon:SetProjectile(nil)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("nightmarefuel")
    inst.AnimState:SetBuild("nightmarefuel")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:SetMultColour(1, 1, 1, 0.5)

    inst:AddTag("monsterwpn")
	inst:AddTag("nosteal") --prevents frogs from "stealing" it.

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(0)

    -------

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/swmonster_wpn.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	

	--inst.components.inventoryitem:SetOnPickupFn(onequip)
	inst.components.inventoryitem:SetOnDroppedFn(Remove)
	--inst.components.inventoryitem.canbepickedup = false
	--inst.components.inventoryitem.cangoincontainer = false
	--inst:ListenForEvent("forgetiteminventory", Remove(inst))
    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("swmonster_wpn", fn, assets)