local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This was written on an outdated source of mandrake. Check with updated prefab before running!
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/seagull.zip"),
	Asset("ANIM", "anim/seagull_build.zip"),
	Asset("ANIM", "anim/seagull_water.zip"),
	
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}
-----------------------
--Stats--
local mob = 
{
	health = 75,
	hunger = 80,
	hungerrate = 0.14, --I think .15 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 10,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "seagull",
	build = "seagull_build",
	--build2 = "parrot_pirate_build",
	scale = 1,	
	stategraph = "SGbirdswp",
	minimap = "seagullp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('seagullp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00}, --we want the "mandrake" corpse. So we have him spawn at the exact location of death instead of dropping as loot. 
	--{'feather_crow',        0.50},
})

local sounds = 
	{
		takeoff = "dontstarve_DLC002/creatures/seagull/takeoff_seagull",
		chirp = "dontstarve_DLC002/creatures/seagull/chirp_seagull",
		flyin = "dontstarve/birds/flyin",
		--land = land_sound,
	}
	
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      ThePlayer:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
		inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('seagullp')
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	inst.components.temperature.mintemp = 20 --prevents freezing
	inst.components.moisture:SetInherentWaterproofness(0.5) --Prevents getting wet.
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("birdwhisperer")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst._isflying = false
	inst.isshiny = 0
	inst.getskins = getskins
	
	inst.sounds = sounds	
	
	local body_symbol = "crow_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,5,5) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)

    inst.DynamicShadow:SetSize(1, .75)
    inst.DynamicShadow:Enable(true)
    inst.Transform:SetTwoFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank.."_water")
	
	inst.components.locomotor:SetAllowPlatformHopping(false)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Character Functions. Don't Touch.--
    	
	inst:AddTag("flying") -- to prevent dying if reloaded on water.
	inst:DoTaskInTime(2, function(inst)
		if SW_PP_MODE == true then
			inst:RemoveComponent("sailor")
			inst:RemoveTag("flying")
		end
	end)	
		
  	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) inst.components.locomotor:SetAllowPlatformHopping(false) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("seagullp", prefabs, assets, common_postinit, master_postinit, start_inv)
