local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--Replace Monsterweapon with Merm's own fishing spear.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/merm_fisherman_build.zip"),
	Asset("ANIM", "anim/merm_fishing.zip"),
	--Asset("ANIM", "anim/dragonfly_fx.zip"), --??? why was this in here.
	--Asset("SOUND", "sound/hound.fsb"),
}

local getskins = {"1"}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	"fish",
	"fish",
	"merm_spear",
	--"swmonster_wpn",
}
-----------------------
--Stats--
local mob = 
{
	health = 150,
	hunger = 150,
	hungerrate = 0.12, ----WILSON_HUNGER_RATE = .15625
	sanity = 200,
	runspeed = 7.5,
	walkspeed = 3,
	damage = 34,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "pigman",
	build = "merm_fisherman_build",
	scale = 1,
	--build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGhogp",
	minimap = "merm2p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('merm2p',
-----Prefab---------------------Chance------------
{
    {'froglegs',             1.00}, 
	{'fish',             1.00}, 
	--{'snakeoil',             0.01}, --SW only.
   
})

local function ontalk(inst, script) --I might not bother enabling this. I know he talks but is it worth an entire speechfile.
    inst.SoundEmitter:PlaySound("dontstarve/creatures/merm/idle")
end

local function ShareTargetFn(dude)
    return dude:HasTag("merm") and not dude.components.health:IsDead()
end

local function IsFish(item)
    return item.prefab == "fish" or item.prefab == "fish_cooked"
end

local function OnEat(inst, food)
    if food and food.components.edible then
		if food.prefab == "fish" or food.prefab == "fish_cooked" then
			inst.components.sanity:DoDelta(5, false)
			inst.components.health:DoDelta(5, false)
			inst.components.hunger:DoDelta(10, false)
		end
    end
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("merm") and not dude.components.health:IsDead() end, 3)
    end
end

local function onhitother(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("merm") and not dude.components.health:IsDead() end, 30)
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison, freeze (flat value, not a multiplier)		
	------------------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('merm2p')
	----------------------------------
	--Tags--
    inst:AddTag("merm")
    inst:AddTag("mermfisher")
    inst:AddTag("wet")
	
	--inst.mobsleepsw = true
	inst.mobplayer = true
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, 0.5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = onhitother
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Functions that saves and loads data.	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    inst.components.talker.ontalk = ontalk
	inst.components.talker.fontsize = 28
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("merm2p", prefabs, assets, common_postinit, master_postinit, start_inv)
