local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/ox_basic.zip"),
	Asset("ANIM", "anim/ox_actions.zip"),
	Asset("ANIM", "anim/ox_build.zip"),
	

	Asset("ANIM", "anim/ox_basic_water.zip"),
	Asset("ANIM", "anim/ox_actions_water.zip"),

	Asset("ANIM", "anim/ox_heat_build.zip"),
}


local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{

}
-----------------------
--Stats--
local mob = 
{
	health = 1000,
	hunger = 250,
	hungerrate = 0.13, --WILSON_HUNGER_RATE = .15625
	sanity = 100,
	runspeed = 7.5,
	walkspeed = 1,5,
	damage = 45*2,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "ox",
	build = "ox_build",
	scale = 1,
	stategraph = "SGoxp",
	minimap = "oxp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable( 'oxp',
{
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'beefalowool',     1.00},
    {'beefalowool',     1.00},
    {'beefalowool',     1.00},
    {'horn',            0.33},
	
	
})

local sounds = 
{
	angry = "dontstarve_DLC002/creatures/OX/angry",
	curious = "dontstarve_DLC002/creatures/OX/curious",
	
	attack_whoosh = "dontstarve_DLC002/creatures/OX/attack_whoosh",
	chew = "dontstarve_DLC002/creatures/OX/chew",
	grunt = "dontstarve_DLC002/creatures/OX/bellow",
	hairgrow_pop = "dontstarve_DLC002/creatures/OX/hairgrow_pop",
	hairgrow_vocal = "dontstarve_DLC002/creatures/OX/hairgrow_vocal",
	sleep = "dontstarve_DLC002/creatures/OX/sleep",
	tail_swish = "dontstarve_DLC002/creatures/OX/tail_swish",
	walk_land = "dontstarve_DLC002/creatures/OX/walk_land",
	walk_water = "dontstarve_DLC002/creatures/OX/walk_water",

	death = "dontstarve_DLC002/creatures/OX/death",
	mating_call = "dontstarve_DLC002/creatures/OX/mating_call",
	yell = "dontstarve_DLC002/creatures/OX/mating_call",

	emerge = "dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med",
	submerge = "dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med",
}

local function OnWaterChange(inst, onwater)
  if not inst:HasTag("playerghost") then
	if onwater then
		inst.sg:GoToState("submerge")
	else
		inst.sg:GoToState("emerge")
	end
  end
end

local function ShareTargetFn(dude)
    return dude:HasTag("beefalo") and not dude.components.health:IsDead()
end

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("beefalo") and not dude.components.health:IsDead() end, 3)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("beefalo") and not dude.components.health:IsDead() end, 30)
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20 ,1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Locomotor--
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('oxp')
	----------------------------------
	--Tags--
	inst:AddTag("ox")
	inst:AddTag("animal")
	inst:AddTag("largecreature")
	inst:AddTag("amphibious")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.taunt2 = true
	inst.skinn = 0
	inst.isshiny = 0
	inst.shouldwalk = false
	inst.sounds = sounds
	inst.getskins = getskins
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE }, { FOODTYPE.ROUGHAGE }) 
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 100, 0.5)
    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank.."_water")
	inst.components.locomotor:SetAllowPlatformHopping(false)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
        function(inst)
			if not inst:HasTag("playerghost") then
				inst.sg:GoToState("submerge")
			end
        end)
		inst.components.amphibiouscreature:SetExitWaterFn(
		function(inst)
			if not inst:HasTag("playerghost") then
				inst.sg:GoToState("emerge")
			end
		end)
	end
	---------------------------------
	--Shedder--
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "poop"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(120)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--IA's tile tracker--
	inst:DoTaskInTime(3, function(inst)
		if SW_PP_MODE == true then		
			inst:AddComponent("tiletracker")
			inst.components.tiletracker:SetOnWaterChangeFn(OnWaterChange)
			inst.components.tiletracker:Start()
		end
	end)
	
	inst.components.grue:AddImmunity("mobplayer")
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
end

return MakePlayerCharacter("oxp", prefabs, assets, common_postinit, master_postinit, start_inv)
