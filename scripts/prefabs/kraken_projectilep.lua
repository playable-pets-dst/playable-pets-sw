local assets=
{
	Asset("ANIM", "anim/ink_projectile.zip"),
	Asset("ANIM", "anim/ink_puddle.zip"),
}

local prefabs = {}

local function onthrown(inst, thrower, pt, time_to_target)
    inst.Physics:SetFriction(.2)
	--print("DEBUG: We spawned!")
    -- local shadow = SpawnPrefab("warningshadow")
    -- shadow.Transform:SetPosition(pt:Get())
    -- shadow:shrink(time_to_target, 1.75, 0.5)

	inst.TrackHeight = inst:DoPeriodicTask(FRAMES, function()
		local pos = inst:GetPosition()

		if pos.y <= 1 then
		    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, 2, nil, {"FX", "NOCLICK", "DECOR", "INLIMBO"})

		    for k,v in pairs(ents) do
	            if v.components.combat and v ~= inst and v.prefab ~= "quacken_tentacle_p" then
	                v.components.combat:GetAttacked(thrower, 50)
	            end
		    end

			local pt = inst:GetPosition()
			--if inst:GetIsOnWater() then
				--local splash = SpawnPrefab("kraken_ink_splatp")
				--splash.Transform:SetPosition(pos.x, pos.y, pos.z)

				
			
				local ink = SpawnPrefab("kraken_inkpatchp")
				ink.Transform:SetPosition(pos.x, pos.y, pos.z)
			--end

			inst:Remove()
		end
	end)
end

local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
	end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("ink")
	inst.AnimState:SetBuild("ink_projectile")
	inst.AnimState:PlayAnimation("fly_loop", true)
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")

	inst:AddComponent("throwablep")
	inst.components.throwablep.onthrown = onthrown
	inst.components.throwablep.random_angle = 0
	inst.components.throwablep.max_y = 20
	inst.components.throwablep.yOffset = 7
	inst.components.throwablep.speed = 10
	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local lerp_time = 30

local function OnNotifyNearbyPlayers(inst, self, rangesq)
    local x, y, z = inst.Transform:GetWorldPosition()
    for i, v in ipairs(AllPlayers) do
        if not v:HasTag("playerghost") and v:GetDistanceSqToPoint(x, y, z) < rangesq then
            v:PushEvent("unevengrounddetected", { inst = inst, radius = self.radius, period = self.detectperiod })
        end
    end
end

local function OnRemove(inst)
	inst:Remove()
end

local function OnTimeOut(inst)
	--inst.isdying = true
	--inst.components.sizetweener:StartTween(0.3, 1)
	--inst:DoTaskInTime(2, OnRemove)	
end

local function DoAreaSlow(inst, ents)
	--if inst.ink_scale > 0.40 then
	if inst.isdying == false then 
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, inst.ink_scale * 3.66, nil, nil, { "player", "monster", "character" })
    for i, v in ipairs(ents) do
		if v.prefab ~= "quackenp" and v.prefab ~= "quacken_tentacle_p" and v.inkimmune == nil and not v.spddebuffed then
        v.AnimState:SetMultColour(101/255, 70/255, 163/255, 1)
		if v.components.locomotor ~= nil and v.isinked == nil then
		v.isinked = true
		local mobrun = v.components.locomotor.runspeed
		local mobwalk = v.components.locomotor.walkspeed
		if mobrun ~= nil then
		v.components.locomotor.runspeed = mobrun * 0.3
		end
		if mobwalk ~= nil then
		v.components.locomotor.walkspeed = mobwalk * 0.3
		end
		v:DoTaskInTime(5, function(inst)
			if mobrun ~= nil then
			v.components.locomotor.runspeed = mobrun 
			end
			if mobwalk ~= nil then
			v.components.locomotor.walkspeed = mobwalk 
			end
			v.AnimState:SetMultColour(1, 1, 1, 1)
			v.isinked = nil
		end)
		end		 
    end
	end
	end
end

local function ink_update(inst, dt)
	if inst.isdying == false then
	--if inst.ink_scale > 0.40 then
	--inst.ink_timer = inst.ink_timer - dt
	--inst.ink_scale = Lerp(0, 1, inst.ink_timer/lerp_time) --lerp seems to cause freezes with world saving.
	--inst.Transform:SetScale(inst.ink_scale, inst.ink_scale, inst.ink_scale)
	DoAreaSlow(inst)
	end
	--if inst.ink_scale <= 0.33 then
		--inst:Remove()
		--return
	--end
	
	
	--[[
	local pos = inst:GetPosition()
	local dist = pos:Dist(GetPlayer():GetPosition())
	if not inst.slowing_player and dist <= inst.ink_scale * 3.66 then
		inst.slowing_player = true
		GetPlayer().components.locomotor:AddSpeedModifier_Mult("INK", -0.7)
	elseif inst.slowing_player and dist > inst.ink_scale * 3.66 then
		inst.slowing_player = false
		GetPlayer().components.locomotor:RemoveSpeedModifier_Mult("INK")
	end]]
	--end
end

local function inkpatch_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBuild("ink_puddle")
    inst.AnimState:SetBank("ink_puddle")
    inst.AnimState:PlayAnimation("idle", true)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)

	--Note: Theres an issue with using tweeners to get to a point to remove itself.
	--Saving and seemingly just being near the prefab will cause client crashes and freezes.
	--Seems to be some sort of conflict with Periodic Task and the sizetweener?
	--Can we even disable periodic task.
	
	inst.entity:SetPristine()
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/cannonball_impact")
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
	--inst:AddComponent("sizetweener")
	
	inst.isdying = false
	inst.ink_timer = 10
	inst.ink_scale = 1
	inst.Transform:SetScale(inst.ink_scale,inst.ink_scale,inst.ink_scale)
	local dt = 0.30

	inst.slowing_player = false
	
	--inst.components.sizetweener:StartTween(.33, inst.ink_timer, inst.Remove)

	inst:DoPeriodicTask(dt, function() ink_update(inst, dt) end)
	
	inst:DoTaskInTime(inst.ink_timer, OnRemove)

	return inst
end

return Prefab("kraken_projectilep", fn, assets, prefabs),
Prefab("kraken_inkpatchp", inkpatch_fn)