local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/mosquito.zip"),
	Asset("ANIM", "anim/mosquito_build.zip"),
	Asset("ANIM", "anim/mosquito_yellow_build.zip"),
	
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}
-----------------------
--Stats--
local mob = 
{
	health = 75,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = TUNING.MOSQUITO_RUNSPEED,
	walkspeed = TUNING.MOSQUITO_RUNSPEED,
	damage = 20,
	range = 1.5,
	hit_range = 2,
	attackperiod = 0,
	bank = "mosquito",
	build = "mosquito_yellow_build",
	scale = 1.18,
	--build2 = "chester_shadow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGfly2p",
	minimap = "fly2p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('fly2p',
-----Prefab---------------------Chance------------
{
    {'mosquitosack',             0.50}, 
	--{'mosquitosack',             0.50}, 
   
})

local SHARE_TARGET_DIST = 30
local MAX_TARGET_SHARES = 10

local sounds =
{
    takeoff = "dontstarve/creatures/mosquito/mosquito_takeoff",
    attack = "dontstarve/creatures/mosquito/mosquito_attack",
    buzz = "dontstarve/creatures/mosquito/mosquito_fly_LP",
    hit = "dontstarve/creatures/mosquito/mosquito_hurt",
    death = "dontstarve/creatures/mosquito/mosquito_death",
    explode = "dontstarve/creatures/mosquito/mosquito_explo",
}

local function ShareTargetFn(dude)
    return dude:HasTag("mosquito") and not dude.components.health:IsDead()
end

local function onhitother2(inst, other)
	PlayablePets.SetPoison(inst, other)
	inst.components.combat:SetTarget(other)
	inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("mosquito") and not dude.components.health:IsDead() end, 30)
	inst.components.hunger:DoDelta(30, false)
	inst.components.health:DoDelta(TheNet:GetServerGameMode() == "lavaarena" and 5 or 10, false)
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPSW_FORGE.FLY2P)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "staves", "books"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function SwapBelly(inst, size)
    for i = 1, 4 do
        if i == size then
            inst.AnimState:Show("body_"..tostring(i))
        else
            inst.AnimState:Hide("body_"..tostring(i))
        end
    end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 0) --fire, acid, poison, freeze (flat value, not a multiplier)		
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('fly2p')
	----------------------------------
	--Tags--
	inst:AddTag("mosquito")
	inst:AddTag("insect")
    inst:AddTag("flying")
    inst:AddTag("smallcreature")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	SwapBelly(inst, 1)
	inst.sounds = sounds
	
	inst.AnimState:Show("wing")
	inst.AnimState:Show("inner wing")
	inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")	
	
	local body_symbol = "body_1"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--SanityAura--
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = -TUNING.SANITYAURA_TINY
	----------------------------------
	--Poison--
	inst.poisonimmune = true
	----------------------------------
	--Eater--
	
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, 0.5)
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.FLYERS)
	inst.AnimState:SetRayTestOnBB(true)

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	
    inst.DynamicShadow:SetSize(.8, .5)

    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
    return inst
end

return MakePlayerCharacter("fly2p", prefabs, assets, common_postinit, master_postinit, start_inv)
