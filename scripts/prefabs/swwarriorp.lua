local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/spider_tropical_build.zip"),
	
	Asset("SOUND", "sound/spider.fsb"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = 
{
	--"spiderhome",
}

--if MOBHOUSE ~= nil and MOBHOUSE == ("Enable1" or "Enable3") then
	--start_inv = start_inv2
--end
-----------------------
--Stats--
local mob = 
{
	health = 350,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 45,
	range = 2.25,
	hit_range = 3.25,
	attackperiod = 2,
	bank = PP_SPIDER_HATS and "spider_hats" or "spider",
	build = "spider_tropical_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGSWwarriorp",
	minimap = "swwarriorp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('swwarriorp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'spidergland',			1.00},
})

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("spider") and not dude.components.health:IsDead() end, 3)
    end
end

local function OnHitOther(inst, other)
    PlayablePets.SetPoison(inst, other)
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('swwarriorp')
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
    inst:AddTag("monster") 
	inst:AddTag("spiderwhisperer")
	inst:AddTag("spider_warrior")
	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.taunt = true
	inst.poisonimmune = true
	inst.isshiny = 0
	inst.getskins = getskins	
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeMediumBurnableCharacter(inst, body_symbol)	
	----------------------------------
	--Eater--
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	 ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("swwarriorp", prefabs, assets, common_postinit, master_postinit, start_inv)
