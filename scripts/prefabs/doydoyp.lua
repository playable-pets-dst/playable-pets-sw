local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
	Asset("ANIM", "anim/doydoy.zip"),
	Asset("ANIM", "anim/doydoy_adult_build.zip"),
}
local prefabs = 
{	
    
}
local start_inv = 
{
	
}

local getskins = {"1"}

local babyfoodprefs = {"SEEDS"}
local teenfoodprefs = {"SEEDS", "VEGGIE"}
local adultfoodprefs = {"MEAT", "VEGGIE", "SEEDS", "ELEMENTAL", "WOOD", "ROUGHAGE", "GENERIC"}
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 4,
	walkspeed = 4,
	damage = 40,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	
	bank = "doydoy",
	build = "doydoy_adult_build",
	scale = 1,
	stategraph = "SGdoydoyp",
	minimap = "doydoyp.tex",
}

if SW_PP_MODE == true then
	SetSharedLootTable('doydoyp',
	{
		{'drumstick',             1.00},
		{'meat',            	  1.00},
		{'drumstick',             1.00},
		{'doydoyfeather',         1.00},
		{'doydoyfeather',         1.00},
   
	})
else
	SetSharedLootTable('doydoyp',
	{
		{'drumstick',             1.00},
		{'meat',            	  1.00},
		{'drumstick',             1.00},
	})
end

local function onload(inst, data)
    if data ~= nil then
		inst.isshiny = data.isshiny
    end
end

local function onsave(inst, data)
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon( "doydoyp.tex" )

	inst:DoTaskInTime(0, function()
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

end

local master_postinit = function(inst) 
     ------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC002/creatures/doy_doy/hit")
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('doydoyp')
	----------------------------------
	--Tags--
	
    inst:AddTag("animal")
	inst:RemoveTag("scarytoprey")
	
	inst.taunt = true	
	inst.mobsleep = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.components.locomotor.fasteronroad = false
	inst.getskins = getskins
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,1,1)
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	inst.components.eater.caneat = adultfoodprefs
	inst.components.eater.preferseating = adultfoodprefs
	
	inst.components.eater:SetCanEatHorrible()
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(2.75, 1)
    inst.Transform:SetFourFaced()
	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = onsave
    inst.OnLoad = onload
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    	
  	inst.components.grue:AddImmunity("mobplayer")	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("doydoyp", prefabs, assets, common_postinit, master_postinit, start_inv)
