require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "attack"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICK, "eat"),
	ActionHandler(ACTIONS.PICKUP, "eat"),
	ActionHandler(ACTIONS.DROP, "feed"),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.altattack then
			return "jump_attack"
		else
			return "attack"
		end
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"),
	--ActionHandler(ACTIONS.TIGERSHARK_JUMP, "action"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local JUMP_SPEED = 75
local JUMP_LAND_OFFSET = 3

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "sharkitten"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "sharkitten"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "sharkitten"}
	end
end

--might be useful as a PP UTIL function, will do when felt necessary.
local function CanSwimInWorld()
    return TheNet:GetServerGameMode() ~= "lavaarena" and not TheWorld:HasTag("cave")
end

local events =
{
    CommonHandlers.OnSleep(),
	EventHandler("attacked", function(inst) 
		if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("busy") then 
			inst.sg:GoToState("hit")
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
	CommonHandlers.OnFreeze(),
	EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("attack", data.target) end end),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
      end),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
	end
end

local function ShakeIfCloseBig(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, 1.2, .08, .9, inst, 40)
	end
end

local function ShakeIfClose_Footstep(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function DoFootstep(inst)
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
	ShakeIfClose_Footstep(inst)
end	

local function ShouldSwitchToWater(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
	local is_on_land = TheWorld.Map:IsPassableAtPoint(x, y, z)
    return not is_on_land
end

local states =
{
	
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/idle")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },

    State{
    	name = "eat",
    	tags = {"busy", "canrotate"},

    	onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre")
            inst.AnimState:PushAnimation("eat_loop")
            inst.AnimState:PushAnimation("eat_pst", false)
    	end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/eat")
                inst:PerformBufferedAction()
            end)
        },

    	events =
    	{
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end)
    	},
	},

    State{
        name = "feed",
        tags = {"busy", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hork")
        end,

        timeline =
        {
            TimeEvent(13*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/meat_hork")
            end),
            TimeEvent(19*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
			
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
				ShakeIfClose(inst) 
			end),
			TimeEvent(20*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
				ShakeIfClose(inst) 
			end),
			
        },
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
		},
    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run")
        end,
		
		timeline =
        {
			TimeEvent(2*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
				ShakeIfClose(inst) 
			end),
			TimeEvent(4*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
				ShakeIfClose(inst) 
			end),			
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

			{
				run = "run",
			}	
    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    State{
        name = "jump_attack",
        tags = {"busy", "specialattack", "nointerrupt"},

        onenter = function(inst, tar)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and target:IsValid() then
				inst.jump_target = target
				inst:FacePoint(target:GetPosition())
			end

            inst.Physics:Stop()
			inst.components.health:SetInvincible(true)
            inst.AnimState:PlayAnimation("ground_launch_up_pre")
            inst.AnimState:PushAnimation("ground_launch_up_loop", true)
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/land_explode")
            end),

            TimeEvent(23*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/jump_attack")
	            inst.components.locomotor.disable = true
				inst.Physics:Stop()
	            inst.Physics:SetMotorVelOverride(0,JUMP_SPEED,0)
    		end),

            TimeEvent(38*FRAMES, function(inst)
                --moved the jump-to-water logic here, as switching stategraphs with a state with an
                --onupdate, for some reason, causes it to run even after the stategraph changes.
                if inst.jump_target and inst.jump_target:IsValid() then
                    local targetpos = inst.jump_target:GetPosition()
                    inst.Transform:SetPosition(targetpos.x, 45, targetpos.z)
                else
                    local mypos = inst:GetPosition()
                    inst.Transform:SetPosition(mypos.x, 45, mypos.z)
                end

				if ShouldSwitchToWater(inst) then
                    inst:SetWater()
                end
                inst.sg:GoToState("fallwarn", inst.jump_target)
            end)
        },
    },

    State{
        name = "fallwarn",
        tags = {"busy", "specialattack"},

        onenter = function(inst, target)
            inst.sg:SetTimeout(34*FRAMES)
            inst.Physics:Stop()
            inst:Hide()

            inst.AnimState:PlayAnimation("ground_launch_up_loop", true)

            local shadow = SpawnPrefab("tigersharkshadowp")
            shadow:shrink()
            local heading = TheCamera:GetHeading()
            local rotation = 180 - heading

            if inst.AnimState:GetCurrentFacing() == FACING_LEFT then
                rotation = rotation + 180
            end

            if rotation < 0 then
                rotation = rotation + 360
            end

            shadow.Transform:SetRotation(rotation)
            local x,y,z = inst:GetPosition():Get()
            shadow.Transform:SetPosition(x,0,z)
        end,

        onexit = function(inst)
            inst:Show()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fall")
        end,
    },

    State{
        name = "fall",
        tags = {"busy", "specialattack", "falling"},

        onenter = function(inst)
            ChangeToCharacterPhysics(inst)
	        inst.components.locomotor:StopMoving()
            inst.Physics:SetMotorVel(0,-JUMP_SPEED,0)
            inst.AnimState:PlayAnimation("ground_launch_down_loop", true)
            inst.Physics:SetCollides(false)
            inst.sg:SetTimeout(5)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/dive_attack")
        end,

        onupdate = function(inst)
            inst.Physics:SetMotorVel(0,-JUMP_SPEED,0)
            local pt = Point(inst.Transform:GetWorldPosition())
            if pt.y <= .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.Physics:SetCollides(true)
				if CanSwimInWorld() then
					inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
				end
                inst.sg:GoToState("fallpost")
            end
        end,

        ontimeout = function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            local vx, vy, vz = inst.Physics:GetMotorVel()
            pt.y = 0
            inst.Physics:Stop()
            inst.Physics:Teleport(pt.x,pt.y,pt.z)
            inst.Physics:SetCollides(true)
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
			end
            inst.sg:GoToState("fallpost")
        end,
    },

    State{
        name = "fallpost",
        tags = {"busy", "specialattack"},

        onenter = function(inst)
            if not PlayablePets.IsAboveLand(inst:GetPosition()) then
                inst:SetWater()
                inst.sg:GoToState("fallpost")
                return
            end
            inst.Physics:Stop()
            inst.components.locomotor.disable = false
            inst.AnimState:PlayAnimation("ground_launch_down_pst")
			inst.components.combat.notags = {"sharkitten"}
            inst.components.groundpounder:GroundPound()
			inst.components.combat.areahitdamagepercent = TheNet:GetServerGameMode() == "lavaarena" and 3 or 2.5
			inst.components.combat:DoAreaAttack(inst, 8, nil, nil, "explosive", GetExcludeTags(inst))
			inst.components.combat.areahitdamagepercent = 0.8
			
			--[[
			if not TheWorld.ismastersim then
			inst.player_classified._detachcam:set(false)
			inst.player_classified:PushEvent("tigershark_detachcamdirty")
			end]]
			
			local isname = inst:GetDisplayName()
			local selfpos = inst:GetPosition()
			print("LOGGED: "..isname.." jumped at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
			
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/land_explode")
			--inst:PerformBufferedAction()
			inst.components.combat.notags = {}		
        end,

        onupdate = function(inst)

        end,

        ontimeout = function(inst)

        end,

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
			inst.altattack = false
			inst.components.combat:SetRange(5,5)
				
			if inst.altattack_task then
				inst.altattack_task:Cancel()
				inst.altattack_task = nil
			end
			inst.altattack_task = inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PPSW_FORGE.TIGERSHARKP.JUMP_CD or 10, function(inst)
				inst.components.combat:SetRange(30, 0)
				inst.altattack = true
			end)
			
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },


    State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/land_attack_rear")
            end),
            TimeEvent(23*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/land_attack")
                PlayablePets.DoWork(inst, 8)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "hit",
        tags = {"hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/hit")
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/sleep") 
				PlayablePets.SleepHeal(inst)
			end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/death_land")
				
            end),
			TimeEvent(37*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
				--DoFootstep(inst)
				ShakeIfCloseBig(inst)
				
            end),
        },
		
		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },

	State{
    	name = "special_atk1",
    	tags = {"busy"},

    	onenter = function(inst)
    		inst.Physics:Stop()
    		inst.AnimState:PlayAnimation("taunt")
    	end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_land")
            end),
        },

    	events =
    	{
    		EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
    	},
	},
	
	State{
    	name = "special_atk2",
    	tags = {"busy"},

    	onenter = function(inst)
    		inst.Physics:Stop()
    		inst.AnimState:PlayAnimation("walk_pst")
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
    	end,

        timeline =
        {

        },

    	events =
    	{
    		EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
    	},
	},
	
    State{
    	name = "taunt",
    	tags = {"busy", "nointerrupt"},

    	onenter = function(inst)
    		inst.Physics:Stop()
    		inst.AnimState:PlayAnimation("taunt")
			
    	end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_land")
				inst:PerformBufferedAction()
            end),
        },

    	events =
    	{
    		EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
    	},
	},
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(states,
{
    starttimeline =
    {		
		 TimeEvent(0*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
			DoFootstep(inst)
			ShakeIfClose(inst)
        end),
    },
    walktimeline =
    {
        TimeEvent(0*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
			DoFootstep(inst)
			ShakeIfClose(inst)
        end),
        TimeEvent(20*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
			DoFootstep(inst)
			ShakeIfClose(inst)
        end),
    },
	
	 endtimeline =
    {
        TimeEvent(0*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
			DoFootstep(inst)
			ShakeIfClose(inst)
        end),
    },
	

	
})
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "taunt", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/death_land")
				
            end),
			TimeEvent(37*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/footstep")
				ShakeIfClose(inst)				
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_land")
            end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle")
local simpleanim = "run_pst"
local simpleidle = "idle"
local simplemove = "run"

CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")

PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle",
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove.."_loop",
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "taunt",
	
	castspelltime = 10,
})


return StateGraph("tigersharkp", states, events, "idle", actionhandlers)