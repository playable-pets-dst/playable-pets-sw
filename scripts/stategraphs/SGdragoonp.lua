require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "attack"
local shortaction = "action"
local workaction = "attack"
local otheraction = "attack"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "attack"),
	ActionHandler(ACTIONS.PICK, "attack"),
	ActionHandler(ACTIONS.HARVEST, "attack"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SpawnMoveFx(inst, offset)
	local pos = inst:GetPosition()
	local angle = inst.Transform:GetRotation()*DEGREES
	local chargefx = SpawnPrefab("dragoon_charge_fx")
	--local vec = Vector3(math.cos(angle), 0, -math.sin(angle))
	--local perp = Vector3(-vec.z, 0, vec.x)
	local scale = 0.4

	local rand_offset = Vector3(math.random(-1, 1) * 0.2, 0, math.random(-1, 1) * 0.2)
	
	chargefx.Transform:SetPosition((pos + rand_offset):Get())
end

local function SpawnFireFx(inst)
	local pos = inst:GetPosition()
	local rand_offset = Vector3(math.random(-1, 1) * 0.2, 0, math.random(-1, 1) * 0.2)
	if MOBFIRE== "Disable" then
	local fire = SpawnPrefab("dragoonfire")
		fire.Transform:SetPosition((pos + rand_offset):Get())
	end
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				inst.sg:GoToState("idle")
				
            
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{
	
	State{
		name = "idle",
		tags = {"idle", "canrotate"},
		onenter = function(inst, playanim)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/idle")
			inst.Physics:Stop()
			inst.AnimState:PushAnimation("idle_loop")
		end,
		
		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},

	},

	State{
        name = "home",
        tags = {"busy", "silentmorph", "invisible" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local target = inst:GetBufferedAction().target
            --local siesta = HasTag("siestahut")
            local failreason =
               -- (siesta ~= TheWorld.state.isday and
                    --(siesta
                    --and (TheWorld:HasTag("cave") and "ANNOUNCE_NONIGHTSIESTA_CAVE" or "ANNOUNCE_NONIGHTSIESTA")
                    --or (TheWorld:HasTag("cave") and "ANNOUNCE_NODAYSLEEP_CAVE" or "ANNOUNCE_NODAYSLEEP"))
               -- )or
			   (target.components.burnable ~= nil and
                    target.components.burnable:IsBurning() and
                    "ANNOUNCE_NOSLEEPONFIRE") 
                --or (IsNearDanger(inst) and "ANNOUNCE_NODANGERSLEEP")
                -- you can still sleep if your hunger will bottom out, but not absolutely
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            inst.AnimState:PlayAnimation("eat")
            inst.sg:SetTimeout(5 * FRAMES)

            SetSleeperSleepState(inst)
        end,

        ontimeout = function(inst)
            local bufferedaction = inst:GetBufferedAction()
            if bufferedaction == nil then
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
                return
            end
            local home = bufferedaction.target
            if home == nil or
                not home:HasTag("dragoonhouse") or
                --home:HasTag("hassleeper") or
                --home:HasTag("siestahut") ~= TheWorld.state.isday or
                (home.components.burnable ~= nil and home.components.burnable:IsBurning()) then
                --Edge cases, don't bother with fail dialogue
                --Also, think I will let smoldering pass this one
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                --inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
            else
                inst:PerformBufferedAction()
                inst.components.health:SetInvincible(true)
                inst:Hide()
                if inst.Physics ~= nil then
                    inst.Physics:Teleport(inst.Transform:GetWorldPosition())
                end
                if inst.DynamicShadow ~= nil then
                    inst.DynamicShadow:Enable(false)
                end
                inst.sg:AddStateTag("sleeping")
				inst.sg:AddStateTag("home")
                inst.sg:RemoveStateTag("busy")
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
            end
        end,

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst:Show()
            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(true)
            end
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    },
	
	State{
		name = "attack",
		tags = {"attack", "busy"},

		onenter = function(inst, target)
			inst.sg.statemem.target = target
			inst.Physics:Stop()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("atk")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/attack")
		end,

		timeline=
		{   

			--.inst:ForceFacePoint(self.target:GetPosition())
			
			TimeEvent(8*FRAMES, function(inst) 
				if inst.components.combat.target then 
					inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
				end 
			end),

			TimeEvent(15*FRAMES, function(inst) 
				inst:PerformBufferedAction()
				if inst.components.combat.target then 
					inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
				end 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/attack_strike")
			end),

			TimeEvent(20*FRAMES, function(inst) 
				if inst.components.combat.target then 
					inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
				end 
			end),

			TimeEvent(27*FRAMES, function(inst) 
				if inst.components.combat.target then 
					inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				end 
			end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "special_atk1",
		tags = {"busy"},
		
		onenter = function(inst)
			-- print("snake spit")
				if inst.taunt2 == true then
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.taunt2 = false
				inst.AnimState:PlayAnimation("spit")
				inst.vomitfx2 = SpawnPrefab("vomitfire_fx")
				inst.vomitfx2.Transform:SetPosition(inst.Transform:GetWorldPosition())
				inst.vomitfx2.Transform:SetRotation(inst.Transform:GetRotation())
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/hork")
				
				else
					inst.sg:GoToState("idle")
				end
		end,

		onexit = function(inst)
			if MOBFIRE== "Disable" then
				inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			end

			if inst.vomitfx2 then 
				inst.vomitfx2:Remove() 
			end
			inst.vomitfx2 = nil
		end,
		
		events=
		{
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("idle")
			end),
		},

		timeline=
		{
			TimeEvent(37*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/spit")
				
				inst:PerformBufferedAction()
				inst.last_target = inst.target
				inst.target = nil
				inst.spit_interval = math.random(20,30)
				inst.last_spit_time = GetTime()
			end),

			TimeEvent(39*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/fireball")
				inst.vomitfx = SpawnPrefab("dragoonspit")--SpawnPrefab(vomitfire_fx")
				inst.vomitfx.Transform:SetPosition(inst.Transform:GetWorldPosition())
				inst.vomitfx.Transform:SetRotation(inst.Transform:GetRotation())
			end),
		},
	},
	
	State{
		name = "hit",
		tags = {"busy", "hit"},

		onenter = function(inst, cb)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/hit")
		end,

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	
	State{
		name = "special_atk2",
		tags = {"busy"},

	 	onenter = function(inst, cb)
	 		inst.Physics:Stop()
	 		inst.AnimState:PlayAnimation("walk_pst")
	 		if inst.canrage then
				inst.canrage = false
			else
				inst.canrage = true
			end
	 	end,

		events=
		{
	 		EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
	 	},
	 },
	
	State{
		name = "special_atk3sw",
		tags = {"busy"},

	 	onenter = function(inst, cb)
	 		inst.Physics:Stop()
	 		inst.AnimState:PlayAnimation("taunt")
	 		inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/taunt")
	 	end,

		events=
		{
	 		EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
	 	},
	 },

	State{
		name = "charge_pre",
		tags = {"canrotate", "busy"},
		
		onenter = function(inst)
			inst.Physics:Stop()

			inst.AnimState:PlayAnimation("charge_pre")
		end,
		
		onupdate= function(inst)
			if not inst.components.locomotor:WantsToMoveForward() then
				inst.sg:GoToState("idle", "charge_pst")
			end
		end,

		events = {
            EventHandler("animover", function(inst)
            	inst:DoTaskInTime(1, function(inst)
            		if inst.sg:HasStateTag("charging") then
            			inst.sg:GoToState("idle", "charge_pst")
            		end
            	end)
            	inst.sg:GoToState("charge")
            end),
        }
	},

	State{
		name = "charge",
		tags = {"moving", "running", "charging"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("charge_loop")
			inst.components.locomotor:RunForward()

			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/charge")

			inst.sg.statemem.fire_timer = 5*FRAMES
			inst.sg.statemem.fire_time = 5*FRAMES

			inst.sg.statemem.move_timer = 2*FRAMES
			inst.sg.statemem.move_time = 2*FRAMES

			inst.sg.statemem.offset = 1
		end,
		
		onupdate= function(inst, dt)
			inst.sg.statemem.move_timer = inst.sg.statemem.move_timer - dt
			inst.sg.statemem.fire_timer = inst.sg.statemem.fire_timer - dt 

			if inst.sg.statemem.move_timer <= 0 then
				inst.sg.statemem.move_timer = inst.sg.statemem.move_time
				--SpawnMoveFx(inst, inst.sg.statemem.offset)
				inst.sg.statemem.offset = inst.sg.statemem.offset * -1 
			end

			if inst.sg.statemem.fire_timer <= 0 then
				inst.sg.statemem.fire_timer = inst.sg.statemem.fire_time
				SpawnFireFx(inst)
			end

			if not inst.components.locomotor:WantsToMoveForward() then
				inst.sg:GoToState("idle", "charge_pst")
			end
		end,

		timeline =
		{
			-- TimeEvent(0*FRAMES, SpawnMoveFx),
			-- TimeEvent(4*FRAMES, SpawnMoveFx),
			-- TimeEvent(8*FRAMES, SpawnMoveFx),
		},

		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("charge") end),
        }
	},

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

		timeline=
        {
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition()) 
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            --inst.components.locomotor:RunForward()
           -- inst.AnimState:PlayAnimation("run_pre")
		   if inst.canrage == true then
				--inst.sg:GoToState("run")
				inst.AnimState:PlayAnimation("charge_pre")
			else
				inst.AnimState:PushAnimation("walk_pre")
				inst.sg:GoToState("walk")
			end

        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("charge_loop")
        end,
		
		timeline =
        {	 --We use these instead of the timer because the locomote isn't run by a brain.
			 TimeEvent(0*FRAMES, SpawnMoveFx),
			 TimeEvent(0*FRAMES, PlayFootstep),
			 TimeEvent(4*FRAMES, SpawnMoveFx),
			 TimeEvent(4*FRAMES, function(inst) SpawnFireFx(inst) end),
			 TimeEvent(4*FRAMES, PlayFootstep),
			 TimeEvent(8*FRAMES, SpawnMoveFx),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			if inst.canrun == false then
			inst.AnimState:PlayAnimation("walk_pst")
			end
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "walk",
		tags = {"moving", "canrotate", "walking"},
		
		onenter = function(inst) 
			
			inst.AnimState:PlayAnimation("walk_loop")
			inst.components.locomotor:WalkForward()
			--inst.sg:SetTimeout(2*math.random()+.5)
		end,
		
		onupdate= function(inst)
			if not inst.components.locomotor:WantsToMoveForward() then
				inst.sg:GoToState("walk_stop")
			end
		end,

		timeline = {
			--TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/taunt") end),
			TimeEvent(0*FRAMES, PlayFootstep ),
			TimeEvent(15*FRAMES, PlayFootstep ),
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end),
        },
		
	},
	
	State{
		name = "walk_stop",
		tags = {"moving", "canrotate", "walking"},
		
		onenter = function(inst) 
			inst.AnimState:PlayAnimation("walk_pst")
		end,
		

		timeline = {

		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
		
	},
	
	State
    {
        name = "taunt",
        tags = { "idle" },

        onenter = function(inst)  
			inst.sg:GoToState("idle")
        end,


        events =
        {
            --EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
}
    
	
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/death")end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/dragoon/taunt") end)
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove.."_loop",
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "taunt",
	
	castspelltime = 10,
})    



    
return StateGraph("dragoonp", states, events, "idle", actionhandlers)

