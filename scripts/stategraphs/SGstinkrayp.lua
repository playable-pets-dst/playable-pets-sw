require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst) 
		if TheNet:GetServerGameMode() == "lavaarena" then
			local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
			return hands and "attack_forge" or "attack"
		else
			return "attack"
		end
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnLocomote(true,false),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "battlestandard", "structure", "wall"}
	else
		return {"notarget", "INLIMBO", "shadow", "battlestandard", "structure", "wall", "player", "companion"}
	end
end

local function DoPosionAoe(inst)
local posx, posy, posz = inst.Transform:GetWorldPosition()
local angle = -inst.Transform:GetRotation() * DEGREES
local offset = 3
local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, PPSW_FORGE.STINKRAYP.AOE_RANGE, {"locomotor"}, GetExcludeTags(inst))
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent))
			if not ent.poisonimmune then
				PlayablePets.SetPoison(inst, ent)
			end
		end
	end
end


 local states=
{
	
	State{
		name = "idle",
		tags = {"idle", "canrotate"},
		onenter = function(inst, playanim)
			inst.Physics:Stop()
			if inst.noactions then
				inst.AnimState:PlayAnimation("shadow", true)
			else
				inst.AnimState:PlayAnimation("fly_loop", true)
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/idle")
			end
		end,

		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end  end ),
			TimeEvent(10*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end end ),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "taunt",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt")
		end,

		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/taunt") end ),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("taunt")
			end
		end,

		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/taunt") end ),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "special_atk2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			if PlayablePets.CheckWaterTile(inst:GetPosition()) then
				if inst.noactions then
					inst.sg:GoToState("emerge")
				else
					inst.sg:GoToState("submerge")
				end
			else
				inst.sg:GoToState("idle")
			end
		end,

		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/taunt") end ),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
		name = "submerge",
		tags = {"busy", "swimming"},
		onenter = function(inst)
			inst.AnimState:PlayAnimation("submerge")
			inst.Physics:Stop()
		end,

		onexit = function(inst)
			inst.noactions = true
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
			inst.Transform:SetNoFaced()
			inst.DynamicShadow:Enable(false)
			inst:AddTag("notarget")
			inst:AddTag("NOCLICK")
			inst.Physics:CollidesWith(COLLISION.LIMITS)
		end,
		
		timeline =
        {
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_sml") end),
        },

		events=
		{
			EventHandler("animover", function(inst)
				inst.Transform:SetScale(inst.scale_water, inst.scale_water, inst.scale_water)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
		name = "emerge",
		tags = {"busy"},
		onenter = function(inst)
			inst.noactions = nil
			inst:RemoveTag("NOCLICK")
			inst:RemoveTag("notarget")
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
			inst.Transform:SetFourFaced()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("emerge")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_sml")
	        inst.DynamicShadow:Enable(true)
	        inst.Transform:SetScale(inst.scale_flying, inst.scale_flying, inst.scale_flying)
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end,

		onexit = function(inst)
			inst.taunt2 = false
			if inst.dive_task then
				inst.dive_task:Cancel()
				inst.dive_task = nil
			end
			inst.dive_task = inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end,

		events=
		{
			EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
		},
	},
	
	State{
        name = "frozen",
        tags = {"busy", "frozen"},

        onenter = function(inst)
	       --if GoToLocoState(inst, "fly") then
	            if inst.components.locomotor then
	                inst.components.locomotor:StopMoving()
	            end
	            inst.AnimState:PlayAnimation("frozen_loop", true)
	            inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")
	            inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
	        --end
        end,

        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events=
        {
            EventHandler("onthaw", function(inst) inst.sg:GoToState("thaw") end ),
        },
    },

    State{
        name = "thaw",
        tags = {"busy", "thawing"},

        onenter = function(inst)
        	--if GoToLocoState(inst, "fly") then
	            if inst.components.locomotor then
	                inst.components.locomotor:StopMoving()
	            end
	            inst.AnimState:PlayAnimation("frozen_loop_pst", true)
	            inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")
	            inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
	       -- end
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("thawing")
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events =
        {
            EventHandler("unfreeze", function(inst)
                --if inst.sg.sg.states.hit then
                    inst.sg:GoToState("hit")
                --else
                    --inst.sg:GoToState("idle")
                --end
            end ),
        },
    },

    State{
        name = "hit",
        tags = {"hit", "busy"},

        onenter = function(inst)
        	--if GoToLocoState(inst, "fly") then
	            inst.components.locomotor:StopMoving()
	            inst.AnimState:PlayAnimation("hit")
	       -- end
        end,

        timeline =
        {
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/hurt") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
			if inst.noaction or inst.poison_cd then
				inst.sg:GoToState("idle")
			else
        	--if GoToLocoState(inst, "fly") then
	            inst.components.locomotor:StopMoving()
	            inst.components.combat:StartAttack()
	            inst.AnimState:PlayAnimation("atk")
	        --end
			end
        end,

        timeline =
        {
        	TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
        	TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
			TimeEvent(8* FRAMES, function(inst)
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst.poison_cd = true
					DoPosionAoe(inst)
				else
					inst:PerformBufferedAction()
				end
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/attack")
			end),
        	TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
        },
		
		onexit = function(inst) 
			if TheNet:GetServerGameMode() == "lavaarena" then
				if inst.poison_cd_task then
					inst.poison_cd_task:Cancel()
					inst.poison_cd_task = nil
				end
				inst.poison_cd_task = inst:DoTaskInTime(PPSW_FORGE.STINKRAYP.SPECIAL_CD, function(inst) inst.poison_cd = nil end)
			end
		end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "attack_forge",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
			if inst.noaction then
				inst.sg:GoToState("idle")
			else
        	--if GoToLocoState(inst, "fly") then
	            inst.components.locomotor:StopMoving()
	            inst.components.combat:StartAttack()
	            inst.AnimState:PlayAnimation("atk")
				for i = 1, 4 do
					inst.AnimState:Hide("puff_fx"..tostring(i))
				end
				
				inst.AnimState:SetTime(7* FRAMES)
	        --end
			end
        end,
		
		onexit = function(inst)
			for i = 1, 4 do
				inst.AnimState:Show("puff_fx"..tostring(i))
			end
		end,

        timeline =
        {
        	TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
        	--TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
			TimeEvent(1* FRAMES, function(inst)
				inst:PerformBufferedAction()
				--inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/attack")
			end),
        	TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				if inst.components.locomotor ~= nil then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("sleep_pre")
			end
        end,

        timeline =
        {
        	TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/sleep") end),
        	-- TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/sleep") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline =
        {
        	TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end ),
        },


        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.noactions = nil
			inst:RemoveTag("NOCLICK")
			inst:RemoveTag("notarget")
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
			inst.Transform:SetFourFaced()
		
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/death") end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation(inst.noactions and "shadow" or "fly_loop")

        end,
		
		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end  end ),
			TimeEvent(10*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation(inst.noactions and "shadow" or "fly_loop")
        end,
		
		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end  end ),
			TimeEvent(10*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end end ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()           
        end,
		
		timeline =
		{
			TimeEvent(1*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end  end ),
			TimeEvent(10*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/wingflap") end end ),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
    
local function GetActionAnim(inst)
	return inst.noactions and "shadow" or "fly_loop"
end
	
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	GetActionAnim, nil, nil, "fly_loop", "fly_loop") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Stinkray/taunt") end ),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)

PP_CommonStates.AddJumpInStates(states, nil, "fly_loop")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
PP_CommonStates.AddSailStates(states, {}, "fly_loop", "fly_loop")
local simpleanim = "fly_loop"
local simpleidle = "fly_loop"
local simplemove = "fly"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_loop",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "fly_loop",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "fly_loop",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("stinkrayp", states, events, "idle", actionhandlers)

