require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if (target and not IsOceanTile(TheWorld.Map:GetTileAtPoint(target:GetPosition():Get()))) or TheWorld.Map:GetPlatformAtPoint(target:GetPosition():Get()) then
			return "throw"
		else
			return "attack"
		end	
	end),	
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function ShakeIfClose(inst)
	--ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 2, inst, SHAKE_DIST)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, 1, inst, 30)
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (   not inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("caninterrupt")
            ) and
            (   inst.sg.mem.last_hit_time == nil or
                inst.sg.mem.last_hit_time + inst.hit_recovery < GetTime()
            ) then
            inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
			if inst.noactions == false then
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.AnimState:PlayAnimation("idle_loop", true)
			else
				inst.components.locomotor.disable = false
			end
        end,
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/hit")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
			if inst.noactions == false then
				inst.AnimState:PlayAnimation("taunt")
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/taunt") end),
			TimeEvent(21*FRAMES, ShakeIfClose)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "special_atk2",
        tags = {"busy", "move"},

        onenter = function(inst, pos)
			if not inst.noactions then
				inst.components.health:SetInvincible(true)
				inst.AnimState:PlayAnimation("exit", false)
				inst.noactions = true
			
				--inst.sg.statemem.pos = pos
				inst.components.minionspawner:DespawnAll()
				inst.components.minionspawner.minionpositions = nil
			else
				inst.sg:GoToState("special_exit")
			end
        end,

        timeline =
        {            
            TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/exit")
            end),

            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/quacken_submerge")
                inst.SoundEmitter:KillSound("quacken_lp_1")
                inst.SoundEmitter:KillSound("quacken_lp_2")
            end),
        },

        onexit = function(inst)
			inst.noactions = true
			inst:Hide()
			inst:AddTag("notarget")
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },

	State{
        name = "sleep",
        tags = {"busy"},
        
        onenter = function(inst)
			if inst.noactions == false then
				inst.AnimState:PlayAnimation("taunt")
				if inst.isactive then
					inst.components.minionspawner:DespawnAll()
				else
					inst.components.minionspawner:SpawnAll()
				end
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/taunt") end),
			TimeEvent(21*FRAMES, ShakeIfClose)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "throw",
        tags = {"attack", "busy"},

        onenter = function(inst)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				--inst.components.combat:StartAttack()
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				inst.components.combat:SetTarget(target)
				inst.components.combat:StartAttack()
				if target ~= nil then
					if target:IsValid() then
						inst:FacePoint(target:GetPosition())
						inst.sg.statemem.attacktarget = target
					end
				end
				inst.sg.statemem.target = target
				inst.AnimState:PlayAnimation("spit")
			end
        end,

        timeline=
        {

            TimeEvent(13*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/spit_puke")
            end),                
            TimeEvent(56*FRAMES, function(inst) 
                --inst.components.combat:DoAttack(inst.sg.statemem.target) 
				inst:PerformBufferedAction()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/spit")
            end),
            TimeEvent(57*FRAMES, function(inst) inst.canspit = false end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
		
		onexit = function(inst)
			if inst.canspit == false then
				if inst.spit_task then
					inst.spit_task:Cancel()
					inst.spit_task = nil
				end
				inst.spit_task = inst:DoTaskInTime(10, function(inst) inst.canspit = true end) --incase interupted	
			end
		end
    },	
	
	State{
        name = "attack",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
			if not inst.noactions then
				if inst.canspit == true then
					inst.sg:GoToState("throw")
				else
					local buffaction = inst:GetBufferedAction()
					local target = buffaction ~= nil and buffaction.target or nil
					inst.components.combat:SetTarget(target)
					if target ~= nil then
						if target:IsValid() then
							inst:FacePoint(target:GetPosition())
							inst.sg.statemem.attacktarget = target
						end
					end
					inst.AnimState:PlayAnimation("taunt")
					inst.components.combat:StartAttack()
					if inst.components.combat.target ~= nil then
						local tentacle = SpawnPrefab("quacken_tentacle_p")
						if inst.isshiny ~= 0 then
							tentacle.AnimState:SetBuild("quacken_tentacle_shiny_build_0"..inst.isshiny)
						end
						local pos = inst.components.combat.target:GetPosition() or nil
						--local x, y, z = pos
						if inst.components.combat.target ~= nil then
							tentacle.Transform:SetPosition(pos:Get())
						end
						tentacle.setbehaviour = 2
						tentacle.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPSW_FORGE.QUACKEN.MINION_DAMAGE or 100 * MOBPVP)
						tentacle.components.combat:SetRange(5, 5)
						tentacle.components.combat:SetAttackPeriod(1)
						tentacle.components.combat:SetTarget(inst.components.combat.target) --maybe won't crash if returned nil
						tentacle:DoTaskInTime(6, function(inst) inst.sg:GoToState("despawn") end)		
					end
				end
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/taunt") end),
			TimeEvent(21*FRAMES, ShakeIfClose)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "special_exit",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst.taunt2 = false
			inst.noactions = false
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/enter")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/quacken_emerge")
            
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/head_drone_rnd_LP", "quacken_lp_1")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/head_drone_LP", "quacken_lp_2")

            inst.AnimState:PlayAnimation("enter")
        end,

        timeline =
        {
            --TimeEvent(5*FRAMES, function(inst) inst.components.minionspawner:SpawnAll() end),
            TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/enter") end),
			--TimeEvent(50*FRAMES, ShakeIfClose)
        },

        events =
        {
            EventHandler("animover", function(inst) inst:DoTaskInTime(13, function(inst) inst.taunt2 = true end) inst.sg:GoToState("idle") end)
        },
    },

	State{
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.noactions == true then
				inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.noactions == true then
            inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.sg:GoToState("idle")
        end,
    },
	
	State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving() --doesn't move but might as well check.
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/hit")
        end,

        timeline =
        {
            
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst:Show() --incase death happens while underwater.
            inst.AnimState:PlayAnimation("death")
			inst.components.minionspawner:DespawnAll()
            inst.components.minionspawner.minionpositions = nil
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,

		 timeline =
        {
            TimeEvent(2*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/death")
            end),

            TimeEvent(38*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/quacken_submerge")
                inst.SoundEmitter:KillSound("quacken_lp_1")
                inst.SoundEmitter:KillSound("quacken_lp_2")
            end),

            TimeEvent(90*FRAMES, function(inst)
                --inst.components.lootdropper:DropLoot()
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
				inst.components.inventory:DropEverything(true)
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
}
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"hit", nil, nil, "idle_loop", "hit") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(2*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/death")
            end),

            TimeEvent(38*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/quacken_submerge")
                inst.SoundEmitter:KillSound("quacken_lp_1")
                inst.SoundEmitter:KillSound("quacken_lp_2")
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/transform_VO") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/foley") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/foley") end),
            TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/foley") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "hit"
local simpleidle = "idle_loop"
local simplemove = "hit"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "hit",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle_loop",
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle_loop",
	
	castspelltime = 10,
})
    
return StateGraph("quackenp", states, events, "special_exit", actionhandlers)

