require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GoToLocoState(inst, state)
    if inst:IsLocoState(state) then
        return true
    end
    inst.sg:GoToState("goto"..string.lower(state), {endstate = inst.sg.currentstate.name})
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnLocomote(true,false),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{	
	State{
        name = "gotobelow",
        tags = {"busy"},
        onenter = function(inst, data)
            inst.AnimState:PlayAnimation("submerge")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
            inst.Physics:Stop()
            inst.sg.statemem.endstate = data.endstate
        end,

        onexit = function(inst)
            inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
            inst.Transform:SetNoFaced()
            inst:SetLocoState("below")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState(inst.sg.statemem.endstate)
            end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
				inst.AnimState:SetSortOrder( 0 )
				inst.Transform:SetFourFaced()
				inst.issurfaced = true --RUN!
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "gotoabove",
        tags = {"busy"},
        onenter = function(inst, data)            
            inst.Physics:Stop()
            inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
            inst.Transform:SetFourFaced()
            inst.AnimState:PlayAnimation("emerge")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med")
            inst.sg.statemem.endstate = data.endstate

        end,

        onexit = function(inst)
            inst:SetLocoState("above")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState(inst.sg.statemem.endstate)
            end),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
                inst.sg.statemem.target = target
                inst.Physics:Stop()
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
				inst.Transform:SetFourFaced()
                inst.components.combat:StartAttack()
                inst.AnimState:PlayAnimation("atk_pre")
                inst.AnimState:PushAnimation("atk", false)
        end,

        timeline =
        {
            TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/swordfish/attack_pre") end),
            TimeEvent( 6*FRAMES, function(inst)
                if inst.components.combat.target then 
                    inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
                end 
            end),
            TimeEvent(11*FRAMES, function(inst)
                if inst.components.combat.target then 
                    inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
                end 
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/swordfish/attack")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/swordfish_sword")
            end),
            TimeEvent(16*FRAMES, function(inst)
                if inst.components.combat.target then 
                    inst:ForceFacePoint(inst.components.combat.target:GetPosition()) 
                end 
                inst:PerformBufferedAction()
            end),
        },

        events =
        {
            EventHandler("animqueueover",  function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder( 3 )
            inst.AnimState:PlayAnimation("shadow", true)                               
        end,
    },

     State{
        name = "action2",
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("shadow_hooked_loop", true)
            inst.sg:SetTimeout(2)
        end,
        
        ontimeout= function(inst)
            inst:PerformBufferedAction()
            inst.sg:GoToState("idle")
        end,
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt")

        end,

        timeline=
        {
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt") end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            if inst.issurfaced ~= nil and inst.issurfaced == false then
				inst.issurfaced = true
			elseif inst.issurfaced ~= nil and inst.issurfaced == true then
				inst.issurfaced = false
			end
			inst.sg:GoToState("idle")
        end,
    },

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			--inst.issurfaced = true
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst)
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
				inst.Transform:SetFourFaced()
			end)
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst, 0.5)
			inst.AnimState:PlayAnimation("sleep_loop")
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) 
				if inst.issurfaced == false then
					inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
					inst.Transform:SetNoFaced()
					inst.AnimState:SetLayer(LAYER_BACKGROUND)
					inst.AnimState:SetSortOrder( 3 )
				end
				inst.sg:GoToState("idle") 
			end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/swordfish/death")
            --inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(10*FRAMES, function(inst) 
                PlayablePets.DoDeath(inst)
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if MOBGHOST== "Enable" then
						inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
					else
						TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
					end
                end
            end),
        },
    },
	
	State{
        name = "frozen",
        tags = {"busy", "frozen"},

        onenter = function(inst)
            if GoToLocoState(inst, "above") then
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("frozen", true)
                inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")
                inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
            end
        end,

        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events=
        {
            EventHandler("onthaw", function(inst) inst.sg:GoToState("thaw") end ),
        },
    },

    State{
        name = "thaw",
        tags = {"busy", "thawing"},

        onenter = function(inst)
            if GoToLocoState(inst, "above") then
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("frozen_loop_pst", true)
                inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")
                inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
            end
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("thawing")
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events =
        {
            EventHandler("unfreeze", function(inst)
                if inst.sg.sg.states.hit then
                    inst.sg:GoToState("hit")
                else
                    inst.sg:GoToState("idle")
                end
            end ),
        },
    },

	 State{
        name = "walk_start",
        tags = {"moving", "canrotate", "swimming"},
        onenter = function(inst) 
            if inst.issurfaced == false then
				inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
				inst.Transform:SetNoFaced()
				inst.AnimState:SetLayer(LAYER_BACKGROUND)
				inst.AnimState:SetSortOrder( 3 )
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.components.locomotor:WalkForward()
				 if inst.SoundEmitter:PlayingSound("runsound") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
					inst.SoundEmitter:KillSound("runsound")
                end
			else
				inst.sg:GoToState("run_start")
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
        },
    },

    State{
        name = "walk",
        tags = {"moving", "canrotate", "swimming"},
        
       
        onenter = function(inst) 
            if GoToLocoState(inst, "below") then
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.components.locomotor:WalkForward()
            end
        end,     

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
        },
    },

    State{
        name = "walk_stop",
        tags = {"moving", "canrotate", "swimming"},
        
       
        onenter = function(inst) 
            inst.sg:GoToState("idle")
        end,
    },

    State{
        name = "run_start",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
				inst.AnimState:PlayAnimation("emerge")
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med")
                inst.sg:GoToState("run_start2")
			else
				inst.sg:GoToState("walk_start")
            end
        end,
        
        timeline=
        {
            --TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med") end),
            --TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/emerge") end),
        },
            
        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },
	
	State{
        name = "run_start2",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
                inst.AnimState:PlayAnimation("fishmed", true)
                inst.components.locomotor:RunForward()
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
				inst.Transform:SetFourFaced()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/swordfish/swim", "run")
			else
				inst.sg:GoToState("walk_start")
            end
        end,
        
        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med") end),
            --TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/emerge") end),
        },
            
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },

    State{
        name = "run",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst)
            if inst.issurfaced == true then
				--inst.SetLocoState(inst, "above")
                inst.components.locomotor:RunForward()
                inst.AnimState:PlayAnimation("fishmed")
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
			else
				inst.sg:GoToState("walk_start")
            end
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),
        },

        onexit = function(inst)
            -- inst.SoundEmitter:KillSound("runsound")
        end,
    },

    State{
        name = "run_stop",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
                inst.SoundEmitter:KillSound("run")
			else
				inst.sg:GoToState("idle")
            end
        end,
        
        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
	
}

local function GetActionAnim(inst)
	return inst.issurfaced and "shadow_flap_loop" or "shadow"
end

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
			if inst.issurfaced == true and inst.SoundEmitter:PlayingSound("runsound") then
				inst.AnimState:PlayAnimation("shadow_flap_loop")
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
				inst.SoundEmitter:KillSound("runsound")
			else
				inst.AnimState:PlayAnimation("shadow")   
			end
        end),
	}, 
	GetActionAnim, nil, nil, GetActionAnim, GetActionAnim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/swordfish/death")
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
				inst.issurfaced = true
			end),
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "hit"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "shadow")
PP_CommonStates.AddOpenGiftStates(states, "shadow")
PP_CommonStates.AddSailStates(states, {}, "shadow", "shadow")
local simpleanim = "shadow_flap_loop"
local simpleidle = "shadow"
local simplemove = "shadow"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "shadow_flap_loop",
	
	castspelltime = 10,
})

    
return StateGraph("swordfishp", states, events, "idle", actionhandlers)

