require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GoToLocoState(inst, state)
    if inst:IsLocoState(state) then
        return true
    end
    inst.sg:GoToState("goto"..string.lower(state), {endstate = inst.sg.currentstate.name})
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{	
	State{
        name = "gotobelow",
        tags = {"busy"},
        onenter = function(inst, data)

            --local splash = SpawnPrefab("splash_water_drop")
            --local pos = inst:GetPosition()
            --splash.Transform:SetPosition(pos.x, pos.y, pos.z)

            inst.AnimState:PlayAnimation("submerge")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
            inst.Physics:Stop()
            inst.sg.statemem.endstate = data.endstate
        end,

        onexit = function(inst)
            inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
            inst.Transform:SetNoFaced()
            inst:SetLocoState("below")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState(inst.sg.statemem.endstate)
            end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
				inst.AnimState:SetSortOrder( 0 )
				inst.Transform:SetFourFaced()
				inst.issurfaced = true --RUN!
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("hit")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/hit")	
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "gotoabove",
        tags = {"busy"},
        onenter = function(inst, data)

            --local splash = SpawnPrefab("splash_water_drop")
           -- local pos = inst:GetPosition()
           -- splash.Transform:SetPosition(pos.x, pos.y, pos.z)
            
            inst.Physics:Stop()
            inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
            inst.Transform:SetFourFaced()
            inst.AnimState:PlayAnimation("emerge")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med")
            inst.sg.statemem.endstate = data.endstate

        end,

        onexit = function(inst)
            inst:SetLocoState("above")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState(inst.sg.statemem.endstate)
            end),
        },
    },

    State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder( 3 )
            inst.AnimState:PlayAnimation("shadow", true)                               
        end,
    },

     State{
        name = "action2",
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("shadow_hooked_loop", true)
            inst.sg:SetTimeout(2)
        end,
        
        ontimeout= function(inst)
            inst:PerformBufferedAction()
            inst.sg:GoToState("idle")
        end,
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt")

        end,

        timeline=
        {
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/sharx/taunt") end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            if inst.issurfaced ~= nil and inst.issurfaced == false then
				inst.issurfaced = true
			elseif inst.issurfaced ~= nil and inst.issurfaced == true then
				inst.issurfaced = false
			end
			inst.sg:GoToState("idle")
        end,
    },

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst)
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default)
				inst.Transform:SetFourFaced()
			end)
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
		local hungerpercent = inst.components.hunger:GetPercent()
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
			end,

		timeline=
        {
			--TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) 
				if inst.issurfaced == false then
					inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
					inst.Transform:SetNoFaced()
					inst.AnimState:SetLayer(LAYER_BACKGROUND)
					inst.AnimState:SetSortOrder( 3 )
				end
				inst.sg:GoToState("idle") 
			end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/death")
			inst.AnimState:SetLayer(LAYER_WORLD)
			inst.AnimState:SetSortOrder( 0 )
            --inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(10*FRAMES, function(inst) 
                PlayablePets.DoDeath(inst)
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	 State{
        name = "walk_start",
        tags = {"moving", "canrotate", "swimming"},
        onenter = function(inst) 
            if inst.issurfaced == false then
				--inst.SetLocoState(inst, "below")
				inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
				inst.Transform:SetNoFaced()
				inst.AnimState:SetLayer(LAYER_BACKGROUND)
				inst.AnimState:SetSortOrder( 3 )
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.components.locomotor:WalkForward()
				 if inst.SoundEmitter:PlayingSound("runsound") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
					inst.SoundEmitter:KillSound("runsound")
                end
			else
				inst.sg:GoToState("run_start")
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
        },
    },

    State{
        name = "walk",
        tags = {"moving", "canrotate", "swimming"},
        
       
        onenter = function(inst) 
            if GoToLocoState(inst, "below") then
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.components.locomotor:WalkForward()
            end
        end,     

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
        },
    },

    State{
        name = "walk_stop",
        tags = {"moving", "canrotate", "swimming"},
        
       
        onenter = function(inst) 
            inst.sg:GoToState("idle")
        end,
    },

    State{
        name = "run_start",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
				inst.AnimState:PlayAnimation("emerge")
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med")
                inst.sg:GoToState("run_start2")
			else
				inst.sg:GoToState("walk_start")
            end
        end,
        
        timeline=
        {
            --TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med") end),
            --TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/emerge") end),
        },
            
        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },
	
	State{
        name = "run_start2",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
                inst.AnimState:PlayAnimation("fishmed", true)
                inst.components.locomotor:RunForward()
				inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
				inst.Transform:SetFourFaced()
                if not inst.SoundEmitter:PlayingSound("runsound") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_swimemerged_med_LP", "runsound")
                end
			else
				inst.sg:GoToState("walk_start")
            end
        end,
        
        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_med") end),
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/emerge") end),
        },
            
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },

    State{
        name = "run",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst)
            if inst.issurfaced == true then
				--inst.SetLocoState(inst, "above")
                inst.components.locomotor:RunForward()
                inst.AnimState:PlayAnimation("fishmed")
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
                if not inst.SoundEmitter:PlayingSound("runsound") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_swimemerged_med_LP", "runsound")
                end
			else
				inst.sg:GoToState("walk_start")
            end
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),
        },

        onexit = function(inst)
            -- inst.SoundEmitter:KillSound("runsound")
        end,
    },

    State{
        name = "run_stop",
        tags = {"moving", "running", "canrotate"},
        
        onenter = function(inst) 
            if inst.issurfaced == true then
                inst.AnimState:PlayAnimation("shadow_flap_loop")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
                inst.SoundEmitter:KillSound("runsound")
			else
				inst.sg:GoToState("idle")
            end
        end,
        
        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },
	
	--Keeping for reference.
	--[[
	State
    {
        name = "action",
        tags = { "busy" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst:PerformBufferedAction()
			if inst.issurfaced == true and inst.SoundEmitter:PlayingSound("runsound") then
				inst.AnimState:PlayAnimation("shadow_flap_loop")
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
				inst.SoundEmitter:KillSound("runsound")
			else
				inst.AnimState:PlayAnimation("shadow")   
			end
        end,


        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },]]
	
}
--[[	
CommonStates.AddFrozenStates(states, 
{
    frozentimeline = 
    {
        TimeEvent(FRAMES*1, function(inst)  
            inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
            inst.Transform:SetFourFaced()
        end)
    },
})    
]]
local function GetActionAnim(inst)
	return inst.issurfaced and "shadow_flap_loop" or "shadow"
end

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
			if inst.issurfaced == true and inst.SoundEmitter:PlayingSound("runsound") then
				--inst.AnimState:PlayAnimation("shadow_flap_loop")
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_med")
				inst.SoundEmitter:KillSound("runsound") 
			end
        end),
	}, 
	GetActionAnim, nil, nil, GetActionAnim, GetActionAnim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/Dogfish/death") 
				inst.AnimState:SetLayer(LAYER_WORLD)
				inst.AnimState:SetSortOrder( 0 )
				inst.issurfaced = true
			end),
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "hit"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "shadow")
PP_CommonStates.AddOpenGiftStates(states, "shadow")
PP_CommonStates.AddSailStates(states, {}, "shadow", "fishmed")
local simpleanim = "fishmed"
local simpleidle = "shadow"
local simplemove = "fishmed"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "fishmed",
	
	castspelltime = 10,
})


    
return StateGraph("dogfishp", states, events, "idle", actionhandlers)

