require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "ambush_attack_pre"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{
	
	State{

        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
			inst.hidingmob = false
            inst.AnimState:PushAnimation("idle", true)
        end,
		
		events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }

    },

    State{
        name = "aggressivehop",
        tags = {"moving", "canrotate", "hopping", "running"},

        timeline=
        {
            TimeEvent(4*FRAMES, function(inst)
                inst.components.locomotor:RunForward()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/jump")
            end ),
            TimeEvent(15*FRAMES, function(inst)
                inst.Physics:Stop()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/land")
            end ),
        },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("jump_pre")
            inst.AnimState:PushAnimation("jump")
            inst.AnimState:PushAnimation("jump_pst", false)
        end,

        events=
        {
            EventHandler("animqueueover", function (inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hop",
        tags = {"moving", "canrotate", "hopping"},


        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("jump_pre")
            inst.AnimState:PushAnimation("jump")
            inst.AnimState:PushAnimation("jump_pst", false)
        end,

        timeline=
        {
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/jump")
                inst.components.locomotor:WalkForward()
            end ),
            TimeEvent(15*FRAMES, function(inst)
                inst.Physics:Stop()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/land")
            end ),
        },

        events=
        {
            EventHandler("animqueueover", function (inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack",
        tags = {"attack"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.hidingmob = false
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)
        end,

        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/attack") end),
            TimeEvent(16*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },


    State{
        name = "special_atk1",
        tags = {"ambusher", "busy", "canrotate"},

        onenter = function(inst)
			if inst.hidingmob == false then
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
			inst.hidingmob = true
            inst.AnimState:PlayAnimation("hide")
            inst:PerformBufferedAction()
			else
			inst.sg:GoToState("hide_loop")
			end
			
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
			inst.hidingmob = true
            --ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("hide_loop") end)
        },
    },

    State{
        name = "hide_loop",
        tags = {"idle", "ambusher", "invisible"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst:Hide()
            inst.sg:SetTimeout(5)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("ground_pre")
        end,

        onexit = function(inst)
            inst:Show()
            inst.DynamicShadow:Enable(true)
			inst.hidingmob = false
            --ChangeToCharacterPhysics(inst)
        end,
    },

    State{
        name = "look_pre",
        tags = {"ambusher", "invisible"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("look_pre")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/eye_ball")
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
			--inst.hidingmob = false
            --ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("look_loop") end)
        },
    },

    State{
        name = "look_loop",

        tags = {"invisible"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            local animnum = 1 --math.random(1,2)
            inst.AnimState:PlayAnimation("look_loop"..animnum, true)

            inst.blinktask = inst:DoTaskInTime(8*FRAMES, function()
                inst.blinktask:Cancel()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/blink")
                inst.blinktask = inst:DoPeriodicTask(59*FRAMES, function()
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/blink")
                end)
            end)
			inst.sg:SetTimeout(8)
        end,

        ontimeout = function(inst)
            --if inst.sg.timeinstate > .75 and inst.components.combat:TryAttack() then
               -- inst.sg:GoToState("hidden_ambush_attack_pre")
            --elseif inst.components.combat.target == nil then
                inst.sg:GoToState("look_pst")
            end,

        onexit = function(inst)
            if inst.blinktask then
                inst.blinktask:Cancel()
            end
            inst.DynamicShadow:Enable(true)
            --ChangeToCharacterPhysics(inst)
        end,
    },

    State{
        name = "look_pst",
        tags = {"invisible"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("look_pst")
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
            --ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("hide_loop") end)
        },
    },

    State{
        name = "ground_pre",
        tags = {"ambusher", "invisible", "idle"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("ground_pre")
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
            --ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("ground_loop") end)
        },
    },

    State{
        name = "ground_loop",
        tags = {"ambusher", "invisible", "idle"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("ground_loop", true)
            inst.sg:SetTimeout(math.random() * 3 + 2)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("ground_pst")
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
            --ChangeToCharacterPhysics(inst)
        end,
    },

    State{
        name = "ground_pst",
        tags = {"ambusher", "invisible", "idle"},

        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PushAnimation("ground_pst", false)
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
			
            --ChangeToCharacterPhysics(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("look_pre") end)
        },
    },

    State{
        name = "hidden_ambush_attack_pre",
        tags = {"attack", "canrotate", "busy", "jumping"},

        onenter = function(inst, cb)
            inst.DynamicShadow:Enable(false)
            --ChangeToInventoryPhysics(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("jump_atk_ground_pre")
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
            --ChangeToCharacterPhysics(inst)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("ambush_attack") end),
        },
    },

    State{
        name = "ambush_attack_pre",
        tags = {"attack", "canrotate", "busy", "jumping"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("jump_atk_pre")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("ambush_attack") end),
        },
    },

    State{
        name = "ambush_attack",
        tags = {"attack", "canrotate", "busy", "jumping"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)

            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("jump_atk")
            inst.AnimState:PushAnimation("jump_atk_pst", false)
        end,

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) 
                inst.Physics:SetMotorVelOverride(10,0,0)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/jump")
            end),
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/attack") end),
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
            TimeEvent(17*FRAMES,
                function(inst)
                    inst.Physics:ClearMotorVelOverride()
                    inst.components.locomotor:Stop()
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/land")
                end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hit",
        tags = {"busy", "hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/hurt")
			inst.hidingmob = false
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "sleep_hidden",
        tags = {"ambusher", "busy", "sleeping"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("look_pst")
            inst.DynamicShadow:Enable(false)
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping_hidden") end ),        
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake_hidden") end),
        },
    },

    State{
            
        name = "sleeping_hidden",
        tags = {"ambusher", "busy", "sleeping"},
        
        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
        end,
        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping_hidden") end ),        
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake_hidden") end),
        },
    },       

    State{
            
        name = "wake_hidden",
        tags = {"ambusher", "busy", "waking"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
            if inst.components.sleeper and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            inst.DynamicShadow:Enable(false)
        end,

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("ground_pre") end ),        
        },
    },

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.hidingmob = false
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				PlayablePets.SleepHeal(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

		timeline=
        {
			TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{

		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            --inst.components.locomotor:RunForward()
            --inst.AnimState:PlayAnimation("run_pre")
			inst.hidingmob = false
			inst.sg:GoToState("aggressivehop")

        end,

        events =
        {
            --EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline =
        {
            TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/move") end),
			TimeEvent(4, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/move") end),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			--inst.AnimState:PlayAnimation("run_pst")            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
}
    
	
CommonStates.AddFrozenStates(states)
    PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"jump_pst", nil, nil, "idle", "jump_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/flup/death") end)
		},
		
		corpse_taunt =
		{
			
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "jump_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "jump_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "jump_pst", "idle")
local simpleanim = "jump_pst"
local simpleidle = "idle"
local simplemove = "jump"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove, pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove,
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle",
	
	castspelltime = 10,
})    



    
return StateGraph("flupp", states, events, "idle", actionhandlers)

