require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "sharkitten"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "sharkitten"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "sharkitten"}
	end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function LightningStrike(inst)
	local rad = math.random(0,3)
	local angle = math.random() * 2 * PI
	--local offset = Vector3(rad * math.cos(angle), 0, -rad * math.sin(angle))
	local offset = Vector3(math.random(0,200), 0, math.random(0,200))
	local storm = math.random(1,100)	
	local pos = inst:GetPosition() + offset
	--local pos = Vector3(math.random() ,0, math.random())
	storm = math.random(1,100)
	if storm >= 70 and not TheWorld:HasTag("cave") then
	if inst.canrage == true and storm <= 40 then
	else
	TheWorld:PushEvent("ms_sendlightningstrike", pos)
	end
	end
	TheWorld:PushEvent("ms_forceprecipitation", true)
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "wall"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping")) and
            (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() then
				inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("hit")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{
	
	State{
        name = "idle",
        tags = {"idle"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
			if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
				inst.components.vacuump:SpitItem()
			end
			
			if inst.userstorm == true then
				LightningStrike(inst)
			end
            PlayablePets.SleepHeal(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    State{
        name = "hit",
        tags = {"hit"}, --should prevent stunlock.
        
        onenter = function(inst, cb)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/hit")
        end,
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack",
        tags = {"attack", "busy", "canrotate"},
        
        onenter = function(inst)
            inst.SoundEmitter:SetParameter("wind_loop", "intensity", 1)

            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        onexit = function(inst)
            inst.SoundEmitter:SetParameter("wind_loop", "intensity", 0)
        end,

        timeline =
        {
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/attack_pre")
            end),

            TimeEvent(31*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/attack_swipe")
            end),

            TimeEvent(33*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/attack_hit")
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) 
                
                    inst.sg:GoToState("idle")  
                
            end),
        },
    },

	--This might go unused should test it.
    State{
        name = "vacuum_antic_pre",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:SetParameter("wind_loop", "intensity", 1)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("vacuum_antic_pre")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_antic_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("vacuum_pre") end),
        },
    },

    State{
        name = "special_atk1",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
			if inst.taunt2 == false then --Check incase RPC doesn't see it as false.
				inst.sg:GoToState("idle")
			end
			inst.taunt2 = false
			inst.SoundEmitter:SetParameter("wind_loop", "intensity", 1)
			inst.AnimState:PlayAnimation("vacuum_antic_pre")
            inst.AnimState:PushAnimation("vacuum_antic_loop", true)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_antic_pre")
            inst.sg:SetTimeout(5)
			
			if PP_FORGE_ENABLED then
				inst.components.combat:AddDamageBuff("twister_defensedebuff", 1.8, true)
			end

            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_antic_LP", "vacuum_antic_loop")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("vacuum_antic_loop")
			if PP_FORGE_ENABLED then
				inst.components.combat:RemoveDamageBuff("twister_defensedebuff")
			end
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("vacuum_pre")
        end,
    },

    State{
        name = "vacuum_pre",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("vacuum_pre")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_pre")
        end,
		
		onexit = function(inst)
		inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_hit")
		inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_LP", "vacuum_loop")
		end,
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("vacuum_loop") end),
        },
    },

    State{
        name = "vacuum_loop",
        tags = {"attack", "busy", "specialattack"},
        
        onenter = function(inst)
			
			inst.components.vacuump.vacuumradius = 30
            inst.components.vacuump.ignoreplayer = false
			PlayablePets.StartSpecialAttack(inst)
			--inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/vacuum_LP", "vacuum_loop")
			--local vacuumhit = 0
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
			if inst.vacuumhit < 8 then
				--inst.CanVacuum = false
				inst.AnimState:PlayAnimation("vacuum_loop")
				inst.components.combat:SetAreaDamage(30, 1)
				inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPSW_FORGE.TWISTERP.ALT_DAMAGE or 100) 
				inst.components.combat.areahitdamagepercent = 1
				inst.components.combat.playerdamagepercent = 2 * MOBPVP
				inst.components.combat:DoAreaAttack(inst, 30, nil, nil, "electric", GetExcludeTags(inst))

				inst.vacuumhit = inst.vacuumhit + 1
			else
				inst.components.vacuump.vacuumradius = 5
				inst.vacuumhit = 0
				inst.SoundEmitter:KillSound("vacuum_loop")
				inst.sg:GoToState("vacuum_pst")
			end
        end,

        onexit = function(inst)
			inst.components.combat.areahitdamagepercent = 0.8
			inst.components.combat:SetAreaDamage(6, 0.8)
			inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPSW_FORGE.TWISTERP.DAMAGE or inst.mob_table.damage)
			inst.components.combat.playerdamagepercent = MOBPVP
        end,
		
		timeline =
        {

        },

        ontimeout = function(inst)
            inst.sg:GoToState("vacuum_pst")
        end,
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("vacuum_loop") end),
        },
    },

    State{
        name = "vacuum_pst",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:SetParameter("wind_loop", "intensity", 0)

            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("vacuum_pst")
			inst.components.combat.areahitdamagepercent = 0.8
			inst.components.combat:SetAreaDamage(6, 0.8)
			inst.components.combat:SetDefaultDamage(150)
			inst.components.combat.playerdamagepercent = MOBPVP
        end,

        events =
        {
            EventHandler("animover", function(inst) 
			inst:DoTaskInTime(20, function(inst) inst.taunt2 = true end)
			inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			inst.components.vacuump:TurnOff()

			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/death")
			inst.SoundEmitter:KillSound("wind_loop")
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

			timeline =
        {
            TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/seal_fly")
            end),

            TimeEvent(40*FRAMES, function(inst)
                inst.components.inventory:DropEverything(true, true)
                --inst.components.lootdropper:DropLoot()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/seal_groundhit")
            end),

            TimeEvent(50*FRAMES, function(inst)
               local seal = SpawnPrefab("sealp")
				seal.AnimState:SetBuild(inst.AnimState:GetBuild())
				seal.Transform:SetPosition(inst:GetPosition():Get())
				seal.sg:GoToState("dizzy")
            end),
        },
		
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			if inst.userstorm == true then
				LightningStrike(inst)
			end
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
           TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/walk") end)
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
            inst.components.combat.lastwasattackedtime = GetTime()
			inst.AnimState:PlayAnimation("walk_pst")         
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("charge_pre")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/charge_roar")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/run_charge_up")
			
			if inst.userstorm == true then
			LightningStrike(inst)
			end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("charge_roar_loop")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/run_charge_up")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/charge_roar")
			
			if inst.userstorm == true then
				LightningStrike(inst)
			end
			
			if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
				inst.components.vacuump:SpitItem()
			end
        end,
		
		timeline =
        {
           TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/walk") end)
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
			inst.SoundEmitter:SetParameter("wind_loop", "intensity", 0)
            inst.components.combat.lastwasattackedtime = GetTime()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("charge_pst")       
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "special_atk2",
        tags = { "busy" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")         
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 8)
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/death")
				inst.SoundEmitter:KillSound("wind_loop")
			end),
			
			TimeEvent(4*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/seal_fly")
            end),

            TimeEvent(40*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/seal_groundhit")
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/twister/twister_active", "wind_loop")
				inst.SoundEmitter:SetParameter("wind_loop", "intensity", 0)
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle_loop"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove.."_loop",
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle",
	
	castspelltime = 10,
}) 
    



    
return StateGraph("twisterp", states, events, "idle", actionhandlers)

