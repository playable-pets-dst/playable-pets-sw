require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "attack_pre"
local shortaction = "action"
local workaction = "attack_pre"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "eat"),
	ActionHandler(ACTIONS.PICK, "eat"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local JUMP_SPEED = 75
local JUMP_LAND_OFFSET = 3

local events =
{
    CommonHandlers.OnSleep(),
	EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
	CommonHandlers.OnFreeze(),
	EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("attack", data.target) end end),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
      end),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
	end
end

local function ShakeIfCloseBig(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, 1.2, .08, .9, inst, 40)
	end
end

local function ShakeIfClose_Footstep(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function DoFootstep(inst)
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
	ShakeIfClose_Footstep(inst)
end	

local states =
{
	
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("water_idle")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },

   State{
        name = "eat",
        tags = {"busy", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            --inst.components.rowboatwakespawner:StopSpawning()
            inst.AnimState:PlayAnimation("water_eat_pre")
            inst.AnimState:PushAnimation("water_eat_pst", false)
        end,

        timeline =
        {

            TimeEvent(0*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg")
            end),

            TimeEvent(14*FRAMES, function(inst)
                inst:PerformBufferedAction()
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/eat")
            end),

            TimeEvent(31*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg")
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end)
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("water_run")
        end,

		timeline =
        {
			
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("walk")
			end ),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("water_charge_pre")
        end,

		timeline =
        {
			
			--TimeEvent(0, function(inst) SpawnWaves(inst, 2, 160) end),
			TimeEvent(9*FRAMES, function(inst) 
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/run_down") 
			end),
			
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("run") 
			end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("water_charge")
        end,
		
		timeline =
        {
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/thrust")  end),			
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},
    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("water_charge_pst")       
			inst.SoundEmitter:KillSound("walk_loop")
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
            
        name = "walk",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("water_run")
            --inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_swimemerged_lrg_LP", "walk_loop")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("walk_loop")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("walk") end),
        },

        timeline = 
        {

        },
    },      
    
    State{
        name = "walk_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.SoundEmitter:KillSound("walk_loop")
            inst.sg:GoToState("idle")
        end,

        timeline =
        {
		
        },
    },
	
	 State{
        name = "dive",
        tags = {"busy", "specialattack"},

        onenter = function(inst)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            inst.AnimState:PlayAnimation("submerge")
            inst.Physics:Stop()
            inst.components.health:SetInvincible(true)
        end,

        events =
        {
            EventHandler("animover", function(inst) inst:Hide() end),
        },

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg") end),
            TimeEvent(30*FRAMES, function(inst) inst.sg:GoToState("jumpwarn", inst.sg.statemem.attacktarget) end),
        },
    },

    State{
        name = "jumpwarn",
        tags = {"busy", "specialattack"},

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            inst.Physics:Stop()

			local tar = inst.sg.statemem.attacktarget
            --ChangeToUnderwaterCharacterPhysics(inst)

            local old_pt = inst:GetPosition()
			
			if SW_PP_MODE == true then
				if tar and inst:GroundTypesMatch(tar) then
					tar.y = 45
					local pos = tar:GetPosition()
					inst.Transform:SetPosition(pos.x, 0, pos.z)
				end
			else	
				inst.Transform:SetPosition(tar:Get())
			end

            local shadow = SpawnPrefab("tigersharkshadowp")
            shadow:Water_Jump()
            shadow.Transform:SetPosition(inst:GetPosition():Get())
        end,

        onexit = function(inst)
            inst:Show()
        end,

        timeline=
        {
            TimeEvent(90*FRAMES, function(inst) 
                inst.sg:GoToState("jump", inst.sg.statemem.attacktarget) 
            end),
        },
    },
	
    State{
        name = "jump_attack",
        tags = {"busy", "specialattack", "nointerrupt"},

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            inst.Physics:Stop()
            inst.components.locomotor.disable = true

			if SW_PP_MODE == true then
				local splash = SpawnPrefab("splash_water")
				local pos = inst:GetPosition()
				splash.Transform:SetPosition(pos.x, pos.y, pos.z)
			end
			
			--[[
			--Cam Stuff, provided by Electroely--
			local playerpos = inst:GetPosition()
			local targetpos = inst.sg.statemem.attacktarget:GetPosition()
			local xoffset = targetpos.x - playerpos.x
			local zoffset = targetpos.z - playerpos.z
			inst.player_classified._campos.x:set(xoffset)
			inst.player_classified._campos.z:set(zoffset)
			inst.player_classified._detachcam:set(true)
			inst.player_classified:PushEvent("tigershark_detachcamdirty")
			---
			]]
            
            inst:Show()

            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/jump_attack")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/roar")

            inst.AnimState:PlayAnimation("launch_up_pre")
            inst.AnimState:PushAnimation("launch_up_loop", true)
			
            inst.components.combat:DoAreaAttack(inst, 6)
			
			
			--SpawnWaves(inst, 9, 360)
        end,

        timeline =
        {
            TimeEvent(3*FRAMES, function(inst)
                inst.Physics:SetMotorVelOverride(0,JUMP_SPEED,0)
            end),

            TimeEvent(15*FRAMES, function(inst)
				if SW_PP_MODE and SW_PP_MODE == true then
					if inst.sg.statemem.attacktarget and not inst:GroundTypesMatch(inst.sg.statemem.attacktarget) then
						inst:SetGround() --GoToGroundState
					end
				end	
                inst.sg:GoToState("fallwarn", inst.sg.statemem.attacktarget)
            end)
        },
    },

    State{
        name = "fallwarn",
        tags = {"busy", "specialattack"},

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
            inst.sg:SetTimeout(34*FRAMES)

            inst.Physics:Stop()

           
			local pos2 = inst:GetPosition()

            --pos.y = 45
			if inst.sg.statemem.attacktarget == nil then 
				pos2.y = 45
				inst.Transform:SetPosition(pos2:Get())
			else
			--inst.sg.statemem.attacktarget:GetPosition().y = 45
				local pos = inst.sg.statemem.attacktarget:GetPosition()
				pos.y = 45
				inst.Transform:SetPosition(pos:Get())
			end

            local shadow = SpawnPrefab("tigersharkshadowp")
            shadow:Water_Fall()
            local heading = TheCamera:GetHeading()
            local rotation = 180 - heading

            if inst.AnimState:GetCurrentFacing() == FACING_LEFT then
                rotation = rotation + 180
            end

            if rotation < 0 then
                rotation = rotation + 360
            end

            shadow.Transform:SetRotation(rotation)
            local x,y,z = inst:GetPosition():Get()
            shadow.Transform:SetPosition(x,0,z)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fall")
        end,

        onexit = function(inst)
			--inst.components.inventory:DropEverything(true)
            --inst:Show()
        end,
    },

    State{
        name = "fall",
        tags = {"busy", "specialattack"},

        onenter = function(inst)
		
		if SW_PP_MODE == true then	
           if not inst:GetIsOnWater() then
                inst:SetGround()
                local pos = inst:GetPosition()
				pos.y = 45
                inst.Transform:SetPosition(pos:Get())
                --inst.sg:GoToState("fall")
           end
		end  

            ChangeToCharacterPhysics(inst)
	        inst.components.locomotor:StopMoving()
            inst.Physics:SetMotorVel(0,-JUMP_SPEED,0)
            inst.AnimState:PlayAnimation("launch_down_loop", true)
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/breach_attack")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/dive_attack")
            inst.Physics:SetCollides(false)
            inst.sg:SetTimeout(JUMP_SPEED/45 + 0.2)
        end,

        onupdate = function(inst)
            inst.Physics:SetMotorVel(0,-JUMP_SPEED,0)
            local pt = Point(inst.Transform:GetWorldPosition())
            if pt.y <= .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.CanFly = false
                inst.Physics:SetCollides(true)
                inst.sg:GoToState("fallpost")
            end
        end,

        ontimeout = function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            local vx, vy, vz = inst.Physics:GetMotorVel()
            if pt.y <= .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.CanFly = false
                inst.Physics:SetCollides(true)
                inst.sg:GoToState("fallpost")
            end
        end,
    },

    State{
        name = "fallpost",
        tags = {"busy", "specialattack"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.components.locomotor.disable = false
            inst.AnimState:PlayAnimation("launch_down_pst")
			inst.components.combat:SetDefaultDamage(600)
			inst.components.combat:DoAreaAttack(inst, 6)
            --inst.components.groundpounder:GroundPound()
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/splash_explode")
			inst:PerformBufferedAction()
			inst.components.combat:SetDefaultDamage(200)
			inst.components.combat:SetRange(6,6)
			inst.rageattack = false			
			inst.userfunctions.StartTimer(inst, inst.charge_time + 20)
			
			--inst.player_classified._detachcam:set(false)
			--inst.player_classified:PushEvent("tigershark_detachcamdirty")
			
        end,

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
			inst.components.combat:SetDefaultDamage(200)
            inst:Show()
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	 State{
        name = "attack_pre",
        tags = {"attack", "busy", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("water_atk_pre")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/pounce_pre")
            
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("attack") end),
        },
    },

    State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("water_atk")
            inst.AnimState:PushAnimation("water_atk_pst", false)
			if inst.mob_scale > 0.75 then
				inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/roar")
			else
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/pounce")
			end	
        end,

        timeline =
        {
            TimeEvent(15*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_attack")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/swipe_whoosh")
				end
				inst:PerformBufferedAction()
            end),

            TimeEvent(27*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_submerge_lrg")
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                    inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("water_hit")
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/hit")
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("water_sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = {TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg") end)},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("water_sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/sleep") PlayablePets.SleepHeal(inst) end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("water_sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		waketimeline = {TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg") end)},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("water_death")
            --inst.components.rowboatwakespawner:StopSpawning()
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

       timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_lrg")
            end),
            TimeEvent(11*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/death_sea")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
            end),
        },
		
		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },

	State{
        name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("water_taunt")
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg")
            end),
            TimeEvent(0*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(33*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(60*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
	
	State{
        name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("water_taunt")
        end,

        timeline =
        {
            TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg")
            end),
            TimeEvent(0*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(33*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(60*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("water_idle")
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
        end,

        timeline =
        {
			
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"water_charge_pst", nil, nil, "water_idle", "water_charge_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(1*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/water_emerge_lrg")
            end),
            TimeEvent(11*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/death_sea")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/seacreature_movement/splash_large")
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(6*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_emerge_lrg")
            end),
            TimeEvent(0*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(33*FRAMES, function(inst)
				if inst.mob_scale > 0.75 then
					inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/taunt_sea")
				else
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss")
				end
            end),
            TimeEvent(60*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/tiger_shark/water_submerge_lrg")
            end),
		},
	
	},
	--anims = 
	{
		corpse = "water_death",
		corpse_taunt = "water_taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "water_charge_pst")
PP_CommonStates.AddOpenGiftStates(states, "water_taunt")
PP_CommonStates.AddSailStates(states, {}, "water_charge_pst", "water_idle")
local simpleanim = "run_pst"
local simpleidle = "water_idle"
local simplemove = "water_charge"

CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove, pst = simplemove.."_pst"}, nil, "death")

PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "water_charge_pst",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "water_idle",
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove,
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "water_taunt",
	
	castspelltime = 10,
})


return StateGraph("tigersharkteenp_water", states, events, "idle", actionhandlers)