require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "attack_pre"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"boarrior_pet", "notarget", "INLIMBO", "shadow", "battlestandard"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "boarrior_pet", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end

local function ShakeIfClose(inst)
	--ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 2, inst, SHAKE_DIST)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, 1, inst, 30)
end

local function SpawnMoveFx(inst)
    local fx = SpawnPrefab("mole_move_fx")
	fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	fx.Transform:SetScale(1.5, 1.5, 1.5)
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (   not inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("caninterrupt")
            ) and
            (   inst.sg.mem.last_hit_time == nil or
                inst.sg.mem.last_hit_time + inst.hit_recovery < GetTime()
            ) then
            inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	--PP_CommonHandlers.OnKnockback(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
			if inst._isdiving == false then
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.AnimState:PlayAnimation("idle_spike", true)
			else
				inst.components.locomotor.disable = false
			end
        end,
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit_spike")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
	{
		name = "attack_pre",
		tags = {"attack", "busy"},
		onenter = function(inst)
			if inst._isdiving == false then
			inst.AnimState:PlayAnimation("attack_pre")
			else
			inst.sg:GoToState("idle")
			end
		end,

		timeline = 
		{
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/attack_pre") end),
		},
		
		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("attack") end),
		},
	},

	State
	{
		name = "attack",
		tags = {"attack", "busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("attack")
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/attack")
		end,

		timeline=
		{
			TimeEvent(2*FRAMES, function(inst) 
				if inst.components.combat then
					inst.components.combat:StartAttack()
					inst:PerformBufferedAction()
				end
			end),
			TimeEvent(1.5, function(inst) 
				inst.sg:GoToState("grow_spike")
			end),
		},

		events=
		{
			--EventHandler("animover", function(inst) inst.sg:GoToState("grow_spike") end),
		},
	},

	State
	{
		name = "grow_spike",
		tags = {"busy"},
		onenter = function(inst)
			inst.AnimState:PlayAnimation("grow_spike")
		end,

		timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/grow_pre") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/grow_spike") end),
		},
		
		events=
		{
			EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
			end),
		},
	},

	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
			if inst._isdiving == false then
            inst.AnimState:PlayAnimation("shake")
			else
			inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
            --TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/quacken/taunt") end),
			--TimeEvent(21*FRAMES, ShakeIfClose)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "special_atk2",
        tags = {"busy", "move"},

        onenter = function(inst, pos)
			if inst._isdiving == false then
				inst.components.health:SetInvincible(true)
				--inst.AnimState:PlayAnimation("taunt")
				inst.AnimState:PlayAnimation("death", false)
				inst._isdiving = true
			elseif inst._isdiving == true then
				inst.sg:GoToState("special_exit")
			end
        end,

        timeline =
        {            
           TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/death") end),
		   TimeEvent(2*FRAMES,  SpawnMoveFx),
        },

        onexit = function(inst)
			inst._isdiving = true
			inst:Hide()
			inst:AddTag("notarget")
			inst:AddTag("noplayerindicator")
			inst.DynamicShadow:Enable(false)
			if PP_FORGE_ENABLED then
				inst.divingtask = inst:DoTaskInTime(10, function(inst) if inst._isdiving == true then inst.sg:GoToState("special_exit") end end)
			end
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },

	State
	{
		name = "dead_to_empty",
		tags = {"busy"},
		onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/dead_to_empty")
			inst.AnimState:PlayAnimation("dead_to_empty")
		end,
		
		events=
		{
			EventHandler("animover", function(inst)
				inst.sg:GoToState("grow_spike")
			end),
		},
	},
	
    State{
        name = "special_exit",
        tags = {"busy"},

        onenter = function(inst)
			if inst.divingtask then
				inst.divingtask:Cancel()
				inst.divingtask = nil
			end
			local grow_start = 0.1
			inst.Transform:SetScale(grow_start, grow_start, grow_start)
			inst.components.sizetweener:StartTween(1, 0.5)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst:RemoveTag("noplayerindicator")
			inst._isdiving = false
			inst.taunt2 = false
            inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/dead_to_empty")
			inst.AnimState:PlayAnimation("dead_to_empty")
        end,

		onexit = function(inst)
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst:RemoveTag("noplayerindicator")
			inst._isdiving = false
			inst.taunt2 = false
			inst:DoTaskInTime(PP_FORGE_ENABLED and PPSW_FORGE.CACTUSP.SPECIAL_CD or 10, function(inst) inst.taunt2 = true end)
		end,
		
        timeline =
        {
            TimeEvent(0*FRAMES,  SpawnMoveFx),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("grow_spike") end)
        },
    },

	State{
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst._isdiving == true then
				inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst._isdiving == true then
            inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.sg:GoToState("idle")
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst:Show() --incase death happens while underwater.
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		 timeline =
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/death") end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },	
}

PP_CommonStates.AddKnockbackState(states, nil, "hit_spike") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"hit_spike", nil, nil, "idle_spike", "hit_spike") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/grow_pre") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/volcano_cactus/grow_spike") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "grow_spike"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle_spike")
PP_CommonStates.AddOpenGiftStates(states, "idle_spike")
PP_CommonStates.AddSailStates(states, {}, "idle_spike", "idle_spike")
local simpleanim = "hit_spike"
local simpleidle = "idle_spike"
local simplemove = "idle_spike"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle_spike",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle_spike",
	
	castspelltime = 10,
})


    
return StateGraph("cactusp", states, events, "special_exit", actionhandlers)

