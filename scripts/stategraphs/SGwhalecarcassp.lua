require("stategraphs/commonstates")

local actionhandlers = {}

local events = 
{
	-- EventHandler("lightningstrike", function(inst) 
	--     if not inst.EggHatched then
	--         inst.sg:GoToState("crack")
	--     end
	-- end),
}


local states =
{   
	State{
		name = "bloat1_pre",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_pre")
		end,

		-- timeline = {},

		events = {
			EventHandler("animover", function(inst) inst.sg:GoToState("bloat1") end)
		},
	},

	State{
		name = "bloat1",
		tags = {"idle"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_bloat1")
		end,

		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("bloat1") end),
        }
	},

	State{
		name = "bloat2_pre",
		tags = {"busy"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_trans1_2")
			inst.SoundEmitter:PlaySound(inst.sounds.bloated1)
		end,

		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("bloat2") end),
        }
	},

	State{
		name = "bloat2",
		tags = {"idle"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_bloat2")
		end,

			-- inst.SoundEmitter:PlaySound(inst.sounds.stinks, "whalestinks")
		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("bloat2") end),
        }
	},

	State{
		name = "bloat3_pre",
		tags = {"busy"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_trans2_3")
			inst.SoundEmitter:PlaySound(inst.sounds.bloated2)
		end,

		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("bloat3") end),
        }
	},

	State{
		name = "bloat3",
		tags = {"idle"},
		
		onenter = function(inst)
			inst.AnimState:PlayAnimation("idle_bloat3")
		end,

		timeline = 
		{
			TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stinks) end),
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stinks) end),
			TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.stinks) end),
		},

		events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("bloat3") end),
        }
	},

	State{
		name = "explode",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("explode", false)
			inst.SoundEmitter:PlaySound(inst.sounds.explosion)
		end,
		
		timeline =
		{
			TimeEvent(7*FRAMES, function (inst)
					inst.components.explosive:OnBurnt()
				end ),

				TimeEvent(8*FRAMES, function(inst)

					local i = 1
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 3 + (math.random() * 8)
						local loot = GetRandomItem(lootsp[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 20+(math.random() * 5), vz)
					end
				end),
				TimeEvent(9*FRAMES, function(inst)

					local i = 2
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 4 + (math.random() * 8)
						local loot = GetRandomItem(loots[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 25+(math.random() * 5), vz)
					end
				end),
				TimeEvent(10*FRAMES, function(inst)

					local i = 3
					for ii = 1, i+1 do
						inst.components.lootdropper.speed = 6 + (math.random() * 8)
						local loot = GetRandomItem(loots[i])
						local newprefab = inst.components.lootdropper:SpawnLootPrefab(loot)
						local vx, vy, vz = newprefab.Physics:GetVelocity()
						newprefab.Physics:SetVel(vx, 30+(math.random() * 5), vz)
					end

					--inst.components.lootdropper:DropLoot(inst:GetPosition())
				end),

				
		},
		
		events = 
		{
            EventHandler("animqueueover", function(inst) inst.components.lootdropper:DropLoot(inst:GetPosition()) inst:Remove() end),
        }
	},
}
	
return StateGraph("whale_carcassp", states, events, "bloat1_pre", actionhandlers)
