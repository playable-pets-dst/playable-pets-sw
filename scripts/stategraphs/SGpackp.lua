require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "open"
local shortaction = "open"
local workaction = "open"
local otheraction = "swallow"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst, action)
		if inst.skinn == 1 then
			return "fireattack" 
		else 
			if TheNet:GetServerGameMode() == "lavaarena" then
				return "open"
			else
				return "idle"
			end
		end		
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and not (inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("hit")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnLocomote(true,false),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{
	
	State{
		name = "idle",
		tags = {"idle", "canrotate"},

		onenter = function(inst, pushanim)
			inst.landed = nil
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop")
		end,

		timeline=
		{
			TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			-- TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
   },
   
   State{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			if inst.landed then
				inst.sg:GoToState("takeoff")
			else
				inst.Physics:Stop()
				inst.landed = true
				inst.AnimState:PlayAnimation("land")
			end
		end,

		timeline=
		{

		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("land_idle") end),
		},
   },
   
   State{
		name = "land_idle",
		tags = {"idle", "canrotate"},

		onenter = function(inst)
			inst.Physics:Stop()
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				LandFlyingCreature(inst)
			end
			inst.AnimState:PlayAnimation("idle_loop_ground", true)
		end,

		timeline=
		{

		},

		events=
		{
			
		},
		
		onexit = function(inst)
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				RaiseFlyingCreature(inst)
			end
		end,
   },
   
   State{
		name = "takeoff",
		tags = {"idle", "canrotate"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("takeoff")
			inst.landed = nil
		end,

		timeline=
		{

		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
		
		onexit = function(inst)
			inst.landed = nil
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				RaiseFlyingCreature(inst)
			end
		end,
   },

	State{
        name = "open",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("open")
            if inst.SoundEmitter:PlayingSound("hutchMusic") then
                inst.SoundEmitter:SetParameter("hutchMusic", "intensity", 1)
            end 
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("close") end ),
        },

        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( inst.sounds.open ) inst:PerformBufferedAction() end),
        },        
    },

    State{
        name = "open_idle",
        tags = {"busy", "open"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle_loop_open")
            
            if not inst.sg.mem.pant_ducking or inst.sg:InNewState() then
				inst.sg.mem.pant_ducking = 1
			end
            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("open_idle") end ),
        },

        timeline=
        {
        
        
            TimeEvent(3*FRAMES, function(inst) 
				inst.sg.mem.pant_ducking = inst.sg.mem.pant_ducking or 1
				inst.SoundEmitter:PlaySound( inst.sounds.pant , nil, inst.sg.mem.pant_ducking) 
				if inst.sg.mem.pant_ducking and inst.sg.mem.pant_ducking > .35 then
					inst.sg.mem.pant_ducking = inst.sg.mem.pant_ducking - .05
				end
			end),
        },        
    },

    State{
        name = "close",
        tags = {"busy"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("closed")
        end,

        onexit = function(inst)
            if not inst.sg.statemem.muffled and inst.SoundEmitter:PlayingSound("hutchMusic") then
                inst.SoundEmitter:SetParameter("hutchMusic", "intensity", 0)
            end 
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },

        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( inst.sounds.close ) end),
            TimeEvent(4*FRAMES, function(inst)
                if inst.SoundEmitter:PlayingSound("hutchMusic") then
                    inst.sg.statemem.muffled = true
                    inst.SoundEmitter:SetParameter("hutchMusic", "intensity", 0)
                end 
            end)
        },        
    },

	State{
		name = "swallow",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("swallow")
		end,

		events=
		{
			EventHandler("animover", function(inst)
				if inst.skinn == 0 and inst.birdfood >= 30 then
				inst.skinn = 2
				inst.sg:GoToState("transform")
				else
				inst.sg:GoToState("idle")
				end
			end ),
		},

		timeline=
		{
			TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.swallow) end),
			TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			-- TimeEvent( 8*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent( 9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},
	},

	State{
		name = "transform",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			if inst.skinn == 1 then
				inst.sg.mem.swallowed = true
				inst.AnimState:PlayAnimation("swallow", false) 
				inst.AnimState:PushAnimation("transform", false) 
				inst.AnimState:PushAnimation("transform_pst", false)
				inst.SoundEmitter:PlaySound(inst.sounds.swallow)
			elseif inst.skinn == 2 then
				inst.AnimState:PlayAnimation("transform", false)
				inst.AnimState:PushAnimation("transform_pst", false)
				inst.SoundEmitter:PlaySound(inst.sounds.transform)
				inst.SoundEmitter:PlaySound(inst.sounds.trasnform_stretch)
				
			end
		end,

		events=
		{

			EventHandler("animqueueover", function(inst) if inst.skinn == 2 then
			inst.userfunctions.MorphFatPackim(inst)
			end
			inst.sg:GoToState("idle") end),
		},

		timeline = 
		{
			TimeEvent(30*FRAMES, function(inst) --add the fx's later
				if inst.skinn == 2 then
					inst.userfunctions.MorphFatPackim(inst)
					inst.SoundEmitter:PlaySound(inst.sounds.transform_pop)
				end				
			end),
			TimeEvent(71*FRAMES, function(inst) --add the fx's later
				if inst.skinn == 0 then
					inst.AnimState:SetBuild("packim_build")
				elseif inst.skinn == 2 then
					inst.userfunctions.MorphFatPackim(inst)
				elseif inst.skinn == 1 then
					inst.userfunctions.MorphFirePackim(inst)
				end

				inst.SoundEmitter:PlaySound(inst.sounds.transform_pop)
			end)
		},

	},

	State{
		name = "fireattack",
		tags = {"attack", "busy", "canrotate", "throwing"},

		onenter = function(inst)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("fireball")		
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
		end,

		timeline =
		{
			TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			-- TimeEvent( 3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent( 6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(21*FRAMES, function(inst)
				inst.FireProjectile(inst, inst.sg.statemem.attacktarget)	
			end),
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			-- TimeEvent(31*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State{
		name = "hit",
		tags = {"busy"},

		onenter = function(inst)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("hit")
		end,

		timeline =
		{
			TimeEvent( 6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline =
		{
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(32*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent( 1*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(45*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			-- TimeEvent(61*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(34*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep) end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly_sleep) end),
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(34*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{

		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")

        end,
		
		timeline = 
		{
			TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
            TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent( 3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

		timeline = 
		{
			TimeEvent( 0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bounce) end),
			TimeEvent( 2*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
			TimeEvent( 6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.fly) end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
		
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "takeoff"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)    

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleidle,
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle_loop",
	
	castspelltime = 10,
})        
	
--CommonStates.AddFrozenStates(states)
    



    
return StateGraph("packp", states, events, "idle", actionhandlers)

