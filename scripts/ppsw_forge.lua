
local GIANT_HP_REDUCTION = 4

return	{
	
	CRABBITP = {
		NAME = "crabbitp",
		HEALTH = 50,
	
		DAMAGE = 10,
		ALT_DAMAGE = 10,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 1.5,
		HIT_RANGE = 2,
	
		RUNSPEED = 6,
		WALKSPEED = 2,
		
		SPECIAL_CD = 10,
		
		DIFFICULTY = 3,	
		ITEMS = {"featheredtunic"}
	},
	
	STINKRAYP = {
		NAME = "stinkrayp",
		HEALTH = 150,
	
		DAMAGE = 10,
		ALT_DAMAGE = 10,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 5,
		AOE_RANGE = 4,
	
		RUNSPEED = 6,
		WALKSPEED = 6,
		
		SPECIAL_CD = 5,
		
		DIFFICULTY = 1,	
		ITEMS = {"forgedarts", "featheredtunic"}
	},
	
	TIGERSHARKP = {
		NAME = "tigersharkp",
		HEALTH = 5000/GIANT_HP_REDUCTION,
	
		DAMAGE = 100,
		ALT_DAMAGE = 300,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 5,
	
		RUNSPEED = 9,
		WALKSPEED = 4,
		
		SPECIAL_CD = 20,
		
		DIFFICULTY = 1,	
		ITEMS = {}
	},
	
	TWISTERP = {
		NAME = "twisterp",
		HEALTH = 6000/GIANT_HP_REDUCTION,
	
		DAMAGE = 75,
		ALT_DAMAGE = 100,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 5,
	
		RUNSPEED = 9,
		WALKSPEED = 4,
		
		SPECIAL_CD = 20,
		
		DIFFICULTY = 1,		
		ITEMS = {}
	},
	
	CROCODOGP = {
		NAME = "crocodogp",
		HEALTH = 200,
	
		DAMAGE = 30,
		ALT_DAMAGE = 0,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 9,
		WALKSPEED = 4,
		
		SPECIAL_CD = 20,
		
		DIFFICULTY = 2,	
		ITEMS = {"forge_woodarmor"}
	},
	
	CACTUSP = {
		NAME = "cactusp",
		HEALTH = 250,
	
		DAMAGE = 50,
		ALT_DAMAGE = 0,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 9,
		WALKSPEED = 4,
		
		SPECIAL_CD = 20,
		
		DIFFICULTY = 2,	
		ITEMS = {"forge_woodarmor"}
	},
	
	FLY2P = {
		NAME = "fly2p",
		HEALTH = 75,
	
		DAMAGE = 20,
		ALT_DAMAGE = 20,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 10,
		WALKSPEED = 10,
		
		SPECIAL_CD = 20,
		
		DIFFICULTY = 2,	
		ITEMS = {}
	},
	
	KELPYP = {
		NAME = "kelpyp",
		HEALTH = 325,
	
		DAMAGE = 80,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 5,
		WALKSPEED = 4,
		
		SPECIAL_CD = 8,
		
		DIFFICULTY = 1,
		ITEMS = {"forge_woodarmor"}
	},
}