--Creates & Launches "throwable" items.
local Throwerp = Class(function(self, inst)
	self.inst = inst
	self.throwable_prefab = "coconadep"
	self.range = 15
	self.onthrowfn = nil
end)

function Throwerp:CanThrowAtPoint(pt)
	if self.canthrowatpointfn then
		return self.canthrowatpointfn(self.inst, pt)
	end

	return true
end

function Throwerp:GetThrowPoint()
	--For use with controller.
	local owner = self.inst.components.inventoryitem.owner
	if not owner then return end
	local pt = nil
	local rotation = owner.Transform:GetRotation()*DEGREES
	local pos = owner:GetPosition()

	for r = self.range, 1, -1 do
        local numtries = 2*PI*r
		pt = FindValidPositionByFan(rotation, r, numtries, function() return true end) --TODO: #BDOIG Might not need to be walkable?
		if pt then
			return pt + pos
		end
	end
end

function Throwerp:CollectPointActions(doer, pos, actions, right)
    if right then
    	if self.target_position then
    		pos = self.target_position
    	end
		if self:CanThrowAtPoint(pos) then
			table.insert(actions, ACTIONS.LAUNCH_THROWABLE)
		end
	end
end

function Throwerp:CollectEquippedActions(doer, target, actions, right)
	if right and self:CanThrowAtPoint(target:GetPosition()) then
		table.insert(actions, ACTIONS.LAUNCH_THROWABLE)
	end
end

function Throwerp:ThrowDefensive(pt)
	local thrown = SpawnPrefab(self.throwable_prefab)
	--local pt = self.inst:GetPosition()
	if thrown ~= nil then
	thrown.components.throwablep.speed = 3 --gets thrown around the thrower.
	thrown.Transform:SetPosition(self.inst:GetPosition():Get())
	thrown.components.throwablep:Throw(pt, self.inst)
	end

	if self.onthrowfn then
		self.onthrowfn(self.inst, thrown, pt)
	end
end

function Throwerp:Throw(pt)
	local thrown = SpawnPrefab(self.throwable_prefab)
	
	if thrown ~= nil then
	
	thrown.Transform:SetPosition(self.inst:GetPosition():Get())
	thrown.components.throwablep:Throw(pt, self.inst)
	end

	if self.onthrowfn then
		self.onthrowfn(self.inst, thrown, pt)
	end
end

return Throwerp