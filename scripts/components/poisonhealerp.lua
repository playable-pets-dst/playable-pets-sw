--This is just basically a copy/pasta to make this work in multiplayer.
--If its too much trouble then remove it.

local Poisonhealerp = Class(function(self, inst)
	self.inst = inst
	self.enabled = true --Used for snakeoil. We don't want it to actually do anything.
end)

function Poisonhealerp:CollectInventoryActions(doer, actions)
	if doer.components.poisonable then
		table.insert(actions, ACTIONS.CUREPOISON)
	end
end

function Poisonhealerp:Cure(target)
	if target.components.poisonable then
		if self.enabled then

			if self.oncure then
				self.oncure(self.inst, target)
			end

			target.components.poisonable:Cure(self.inst)
		end
		return true
	end
end

function Poisonhealerp:CollectUseActions(doer, target, actions)
	--if target.components.health and target.components.health.canheal then
	if target.components.poisonable then
		table.insert(actions, ACTIONS.CUREPOISON)
	end
end

return Poisonhealerp
