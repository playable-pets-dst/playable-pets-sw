local DEFAULT_SCALE = 0.20
local Puppets = {	
	--SW
	kelpyp = {bank = "kelpy", build = "kelpy_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "kelpy", uiscale = 0.15 },
	snakep = {bank = "snake", build = "snake_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "snake", uiscale = 0.25 },
	snake_poisonp = {bank = "snake", build = "snake_yellow_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "snake", uiscale = 0.25 },
	flupp = {bank = "flup", build = "flup_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "flup", uiscale = 0.27 },
	parrotp = {bank = "crow", build = "parrot_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "parrot" },
	toucanp = {bank = "crow", build = "toucan_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "toucan" },
	seagullp = {bank = "seagull", build = "seagull_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "seagull" },
	swwarriorp = {bank = "spider", build = "spider_tropical_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "spider_tropical" },
	stinkrayp = {bank = "stinkray", build = "stinkray", anim = "fly_loop", scale = DEFAULT_SCALE, shinybuild = "stinkray", y_offset = -50 },
	hogp = {bank = "pigman", build = "wildbore_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "wildbore", hide = {"HAT"}},
	merm2p = {bank = "pigman", build = "merm_fisherman_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "merm_fisherman" },
	ghost2p = {bank = "ghost", build = "ghost_pirate_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "ghost_pirate", uiscale = 0.15, y_offset = -10},
	fly2p = {bank = "mosquito", build = "mosquito_yellow_build", anim = "walk_loop", scale = DEFAULT_SCALE, shinybuild = "mosquito_yellow"},
	houndvenomp = {bank = "hound", build = "hound_venom_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "hound_venom"},
	tigersharkp = {bank = "tigershark", build = "tigershark_ground_build", anim = "idle", scale = 0.075, shinybuild = "tigershark", uiscale = 0.05 },
	sharkittenp = {bank = "sharkitten", build = "sharkitten_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "sharkitten", uiscale = 0.15 },
	twisterp = {bank = "twister", build = "twister_build", anim = "idle_loop", scale = 0.1, shinybuild = "twister", uiscale = 0.05 },
	doydoyp = {bank = "doydoy", build = "doydoy_adult_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "doydoy" },
	smalldoydoyp = {bank = "doydoy_baby", build = "doydoy_baby_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "doydoy_baby" },
	apep = {bank = "kiki", build = "junglekiki_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "junglekiki", sixfaced = true },
	crabbitp = {bank = "crabbit", build = "crabbit_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "crabbit" },
	packp = {bank = "packim", build = "packim_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "packim", sixfaced = true, y_offset = -30 },
	oxp = {bank = "ox", build = "ox_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "ox", uiscale = 0.14 },
	palmtreep = {bank = "treeguard", build = "treeguard_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.06, shinybuild = "treeguard", uiscale = 0.08 },
	dragoonp = {bank = "dragoon", build = "dragoon_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "dragoon" },
	jellyfishp = {bank = "jellyfish", build = "jellyfish", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "jellyfish", y_offset = 20 },
	rainbowjellyfishp = {bank = "rainbowjellyfish", build = "rainbowjellyfish", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "rainbowjellyfish", y_offset = 20 },
	sharxp = {bank = "sharx", build = "sharx_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "sharx", uiscale = 0.15 },
	cactusp = {bank = "cactus_volcano", build = "cactus_volcano", anim = "idle_spike", scale = DEFAULT_SCALE, shinybuild = "cactus_volcano", uiscale = 0.125 },
	crocodogp = {bank = "crocodog", build = "crocodog", anim = "idle", scale =  0.15, shinybuild = "crocodog" },
	poisoncrocodogp = {bank = "crocodog", build = "crocodog_poison", anim = "idle", scale =  0.15, shinybuild = "crocodog_poison" },
	watercrocodogp = {bank = "crocodog", build = "crocodog_water", anim = "idle", scale =  0.15, shinybuild = "crocodog_water" },
	ballphinp = {bank = "ballphin", build = "ballphin", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "ballphin", y_offset = 20 },
	clockwork4p = {bank = "knightboat", build = "knightboat_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "knightboat" },
	crabbitp = {bank = "crabbit", build = "crabbit_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "crabbit" },
	dogfishp = {bank = "dogfish", build = "fish_dogfish", anim = "fishmed", scale = DEFAULT_SCALE, shinybuild = "dogfish", y_offset = 20 },
	swordfishp = {bank = "swordfish", build = "fish_swordfish", anim = "fishmed", scale = DEFAULT_SCALE, shinybuild = "swordfish", uiscale = 0.15, y_offset = 20 },
	whalep = {bank = "whale", build = "whale_blue_build", anim = "idle", scale = DEFAULT_SCALE - 0.05, shinybuild = "whale", uiscale = 0.13 },
	whale2p = {bank = "whale", build = "whale_moby_build", anim = "idle", scale = DEFAULT_SCALE - 0.05, shinybuild = "whale_moby", uiscale = 0.13 },
	shadow3p = {bank = "shadowseacreature", build = "shadow_insanity_water1", anim = "idle", scale = DEFAULT_SCALE - 0.075, shinybuild = "shadow3", sixfaced = true, uiscale = 0.1 },
	quackenp = {bank = "quacken", build = "quacken", anim = "idle_loop", scale = DEFAULT_SCALE - 0.125, shinybuild = "quacken", uiscale = 0.05 },
	jungletreeguardp = {bank = "jungletreeguard", build = "jungletreeguard_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.08, shinybuild = "treeguard", uiscale = 0.06 },
}

return Puppets