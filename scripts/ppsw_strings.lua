
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

--STRINGS.RECIPE_DESC.CITYPIGHOUSEP = "A house for the classiest pigs"
------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
----------------------------------------------
--Recipe Desc--
STRINGS.NAMES.SHARKHOME = "Sharkitten Den"
STRINGS.NAMES.DRAGOONHOME = "Dragoon Den"
STRINGS.NAMES.CRABHOME = "Crabbit Hole"
STRINGS.NAMES.QUACKEN_TENTACLE_P = "Tentacle"
STRINGS.NAMES.WHALE_BLUE_CARCASSP = "Whale Carcass"
STRINGS.NAMES.WHALE_WHITE_CARCASSP = "Whale Carcass"
STRINGS.NAMES.SEALP = "Seal"

STRINGS.NAMES.MERM_SPEAR = "Fishing Spear"
STRINGS.RECIPE_DESC.MERM_SPEAR = "Give a man this and he'll eat for a life time."

STRINGS.NAMES.ANTIDOTE_P = "Antidote"
STRINGS.RECIPE_DESC.ANTIDOTE_P = "A simple solution to a deadly problem."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.ANTIDOTE_P = 
{
	GENERIC = "A simple solution to a deadly problem.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}


STRINGS.NAMES.SHARKITTENDEN_P = "Sharkitten Den"
STRINGS.RECIPE_DESC.SHARKITTENDEN_P = "A sandbox for your killer kittens."

STRINGS.CHARACTERS.GENERIC.DESCRIBE.QUACKEN_TENTACLE_P = 
{
	GENERIC = "How many of these are there?",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WHALE_BLUE_CARCASSP = 
{
	GENERIC = "Smells really bad.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WHALE_WHITE_CARCASSP = 
{
	GENERIC = "Moby Dick is dead at long last",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SEALP = 
{
	GENERIC = "Its trying to act innocent.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SHARKITTENDEN_P = 
{
	GENERIC = "Something big must've made this.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.NAMES.DRAGOONDEN_P = "Dragoon Den"
STRINGS.RECIPE_DESC.DRAGOONDEN_P = "Do they even lift?"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SNAKEBUSH_P = 
{
	GENERIC = "Their homes look intense.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.NAMES.CRABBITHOLE_P = "Crabbit Hole"
STRINGS.RECIPE_DESC.CRABBITHOLE_P = "A home for spider rabbits."

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRABBITHOLE_P = 
{
	GENERIC = "A hole with seafood inside.",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.NAMES.SNAKEBUSH_P = "Snake Bush" --Berry Bush disguise maybe
STRINGS.NAMES.SNAKEHOME = "Snake Bush" --Berry Bush disguise maybe
STRINGS.RECIPE_DESC.SNAKEBUSH_P = "This bush would be perfect for your rivals."

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SNAKEBUSH_P = 
{
	GENERIC = "Was that bush always there?",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MERM_SPEAR = 
{
	GENERIC = "Simple, but sturdy",
	--BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.NAMES.MERMHUT_P = "Merm Hut"
STRINGS.RECIPE_DESC.MERMHUT_P = "Be the talk of the swamp."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MERMHUT_P = 
{
	GENERIC = "Somehow it smells worse than the others.",
	BURNT = "Now it smells just as bad as the others.",
}
STRINGS.NAMES.MERMHUT2_P = "FisherMerm Hut"
STRINGS.RECIPE_DESC.MERMHUT2_P = "Let the swamp know your passion."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MERMHUT2_P = 
{
	GENERIC = "Whoever built this loves to fish",
	BURNT = "I almost feel sorry for the guy who lived here.",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.MERMHUT2_P = 
{
	GENERIC = "Whoever built this loves to fish",
	BURNT = "We almost feel sorry for the guy who lived here.",
}
STRINGS.NAMES.HOGHOUSE_P = "Bore Hut"
STRINGS.RECIPE_DESC.HOGHOUSE_P = "Show the foreign pigs a real house."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOGHOUSE_P = 
{
	GENERIC = "Seems very out of place.",
	BURNT = "I'm sure the owner will be alright.",
}
----------------------------------------------
-- The character select screen lines
STRINGS.CHARACTER_TITLES.kelpyp = "Kelpy"
STRINGS.CHARACTER_NAMES.kelpyp = "KELPY"
STRINGS.CHARACTER_DESCRIPTIONS.kelpyp = "*Is kelpy\n*Is bulky\n*Is weak to fire"
STRINGS.CHARACTER_QUOTES.kelpyp = "He wants to eat you as much as you want to eat him"

STRINGS.CHARACTER_TITLES.snakep = "Snake"
STRINGS.CHARACTER_NAMES.snakep = "SNAKE"
STRINGS.CHARACTER_DESCRIPTIONS.snakep = "*Is a snake\n*Is quite slow\n*Can only eat meats"
STRINGS.CHARACTER_QUOTES.snakep = "Snake? Snake? SNAAAAAKE!!!"

STRINGS.CHARACTER_TITLES.snake_poisonp = "Snake"
STRINGS.CHARACTER_NAMES.snake_poisonp = "SNAKE"
STRINGS.CHARACTER_DESCRIPTIONS.snake_poisonp = "*Is a snake\n*Is quite slow\n*Is poisonous"
STRINGS.CHARACTER_QUOTES.snake_poisonp = "Snake? Snake? SNAAAAAKE!!!"

STRINGS.CHARACTER_TITLES.cactusp = "Elephant Cactus"
STRINGS.CHARACTER_NAMES.cactusp = "ELEPHANTCACTUS"
STRINGS.CHARACTER_DESCRIPTIONS.cactusp = "*Is a cactus\n*Is a prick\n*Is not a joke"
STRINGS.CHARACTER_QUOTES.cactusp = "What a huge prick"

STRINGS.CHARACTER_TITLES.shadow3p = "Swimming Horror"
STRINGS.CHARACTER_NAMES.shadow3p = "SWIMMING HORROR"
STRINGS.CHARACTER_DESCRIPTIONS.shadow3p = "*Is a shadow\n*Is immune to debuffs\n*Can swim"
STRINGS.CHARACTER_QUOTES.shadow3p = "SEE? IT TOTALLY EXISTS! IM NOT INSANE!"

STRINGS.CHARACTER_TITLES.clockwork4p = "Floaty Boaty Knight"
STRINGS.CHARACTER_NAMES.clockwork4p = "FLOATY BOATY KNIGHT"
STRINGS.CHARACTER_DESCRIPTIONS.clockwork4p = "*Is a clockwork\n*Can fire coconades to attack\n*Can only be in the ocean"
STRINGS.CHARACTER_QUOTES.clockwork4p = "STOP RIGHT THERE CRIMINAL SCUM!"

STRINGS.CHARACTER_TITLES.jellyfishp = "Jellyfish"
STRINGS.CHARACTER_NAMES.jellyfishp = "Jellfish"
STRINGS.CHARACTER_DESCRIPTIONS.jellyfishp = "*Is a jellyfish\n*Shocks melee attackers\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.jellyfishp = "No, it does not produce jelly."

STRINGS.CHARACTER_TITLES.rainbowjellyfishp = "Rainbow Jellyfish"
STRINGS.CHARACTER_NAMES.rainbowjellyfishp = "Rainbow Jellfish"
STRINGS.CHARACTER_DESCRIPTIONS.rainbowjellyfishp = "*Is a jellyfish\n*Shocks melee attackers\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.rainbowjellyfishp = "Great for rave parties."

STRINGS.CHARACTER_TITLES.swordfishp = "Swordfish"
STRINGS.CHARACTER_NAMES.swordfishp = "Swordfish"
STRINGS.CHARACTER_DESCRIPTIONS.swordfishp = "*Is a sword fish\n*Has quite the attack range\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.swordfishp = "STABBY STAB STAB"

STRINGS.CHARACTER_TITLES.ballphinp = "Ballphin"
STRINGS.CHARACTER_NAMES.ballphinp = "BALLPHIN"
STRINGS.CHARACTER_DESCRIPTIONS.ballphinp = "*Is adorable\n*Can do flips\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.ballphinp = "Bottle-Nosed Dolphin"

STRINGS.CHARACTER_TITLES.whalep = "Whale"
STRINGS.CHARACTER_NAMES.whalep = "Whale"
STRINGS.CHARACTER_DESCRIPTIONS.whalep = "*Is bulky\n*Has a big stomach\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.whalep = "Why so blue?"

STRINGS.CHARACTER_TITLES.whale2p = "White Whale"
STRINGS.CHARACTER_NAMES.whale2p = "White Whale"
STRINGS.CHARACTER_DESCRIPTIONS.whale2p = "*Is bulky\n*Is an angry whale\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.whale2p = "He does not want to be your friend"

STRINGS.CHARACTER_TITLES.packp = "Packim"
STRINGS.CHARACTER_NAMES.packp = "PACKIM"
STRINGS.CHARACTER_DESCRIPTIONS.packp = "*Is flying\n*Can transform\n*Loves fish"
STRINGS.CHARACTER_QUOTES.packp = "Packim up with stuff."

STRINGS.CHARACTER_TITLES.flupp = "Flup"
STRINGS.CHARACTER_NAMES.flupp = "FLUP"
STRINGS.CHARACTER_DESCRIPTIONS.flupp = "*Is a flup\n*Can hide\n*Can steal items"
STRINGS.CHARACTER_QUOTES.flupp = "Flip Flup"

STRINGS.CHARACTER_TITLES.crabbitp = "Crab"
STRINGS.CHARACTER_NAMES.crabbitp = "CRAB"
STRINGS.CHARACTER_DESCRIPTIONS.crabbitp = "*Is adorable\n*Can hide\n* Can't attack..."
STRINGS.CHARACTER_QUOTES.crabbitp = "Don't kill me please"

STRINGS.CHARACTER_TITLES.jungletreeguardp = "Jungle Tree Guard"
STRINGS.CHARACTER_NAMES.jungletreeguardp = "JUNGLE TREE GUARD"
STRINGS.CHARACTER_DESCRIPTIONS.jungletreeguardp = "*Is bulky\n*Is pretty large\n*Is very slow and weak to fire"
STRINGS.CHARACTER_QUOTES.jungletreeguardp = "..."

STRINGS.CHARACTER_TITLES.palmtreep = "Palm Tree Guard"
STRINGS.CHARACTER_NAMES.palmtreep = "PALM TREE GUARD"
STRINGS.CHARACTER_DESCRIPTIONS.palmtreep = "*Is bulky\n*Can throw coconuts\n*Is very slow and weak to fire"
STRINGS.CHARACTER_QUOTES.palmtreep = "..."

STRINGS.CHARACTER_TITLES.smalldoydoyp = "Baby DoyDoy"
STRINGS.CHARACTER_NAMES.smalldoydoyp = "BABY DOYDOY"
STRINGS.CHARACTER_DESCRIPTIONS.smalldoydoyp = "*Is a baby\n*Can grow by eating\n*Is helpless"
STRINGS.CHARACTER_QUOTES.smalldoydoyp = "Eeeeeeeeeh?"

STRINGS.CHARACTER_TITLES.doydoyp = "DoyDoy"
STRINGS.CHARACTER_NAMES.doydoyp = "DOYDOY"
STRINGS.CHARACTER_DESCRIPTIONS.doydoyp = "*Is a DoyDoy\n*Eats anything\n*Is not aware of threats"
STRINGS.CHARACTER_QUOTES.doydoyp = "Eeeeeeeeeh?"

--STRINGS.CHARACTER_TITLES.houndvenomp = "Venom Hound"
--STRINGS.CHARACTER_NAMES.houndvenomp = "VENOM HOUND"
--STRINGS.CHARACTER_DESCRIPTIONS.houndvenomp = "*Is a hound\n*Is very fast\n*Is poisonous"
--STRINGS.CHARACTER_QUOTES.houndvenomp = "Lots of venom in every bite."

STRINGS.CHARACTER_TITLES.fly2p = "Poison Mosquito"
STRINGS.CHARACTER_NAMES.fly2p = "POISON MOSQUITO"
STRINGS.CHARACTER_DESCRIPTIONS.fly2p = "*Is flying\n*Gains hunger and health on attack\n*Is poisonous"
STRINGS.CHARACTER_QUOTES.fly2p = "BZZZZZZZZZ"

STRINGS.CHARACTER_TITLES.ghost2p = "Ghost Pirate"
STRINGS.CHARACTER_NAMES.ghost2p = "GHOST"
STRINGS.CHARACTER_DESCRIPTIONS.ghost2p = "*Is a ghost\n*Can hide\n*Is immune to debuffs"
STRINGS.CHARACTER_QUOTES.ghost2p = "BOoOoOOoty"

STRINGS.CHARACTER_TITLES.hogp = "Wild Bore"
STRINGS.CHARACTER_NAMES.hogp = "WILD BORE"
STRINGS.CHARACTER_DESCRIPTIONS.hogp = "*Is pretty strong\n*Can ram faraway enemies\n*Not that smart"
STRINGS.CHARACTER_QUOTES.hogp = "Bore to be Wild"

STRINGS.CHARACTER_TITLES.merm2p = "Fisher Merm"
STRINGS.CHARACTER_NAMES.merm2p = "FISHER MERM"
STRINGS.CHARACTER_DESCRIPTIONS.merm2p = "*Is a merm\n*Loves fishing\n*Is pretty chill"
STRINGS.CHARACTER_QUOTES.merm2p = "Bruh"

STRINGS.CHARACTER_TITLES.dragoonp = "Dragoon"
STRINGS.CHARACTER_NAMES.dragoonp = "DRAGOON"
STRINGS.CHARACTER_DESCRIPTIONS.dragoonp = "*Is a dragoon\n*Has a high speed dash\n*Can spit lava"
STRINGS.CHARACTER_QUOTES.dragoonp = "BURN TO THE GROUND"

STRINGS.CHARACTER_TITLES.tigersharkp = "Tigershark"
STRINGS.CHARACTER_NAMES.tigersharkp = "TIGERSHARK"
STRINGS.CHARACTER_DESCRIPTIONS.tigersharkp = "*Is a Giant\n*Can jump on enemies\n*Can swim"
STRINGS.CHARACTER_QUOTES.tigersharkp = "King of the Sea"

STRINGS.CHARACTER_TITLES.oxp = "Ox"
STRINGS.CHARACTER_NAMES.oxp = "OX"
STRINGS.CHARACTER_DESCRIPTIONS.oxp = "*Is bulky \n*Can poop \n*Can swim"
STRINGS.CHARACTER_QUOTES.oxp = "Oxy Boxy"

STRINGS.CHARACTER_TITLES.stinkrayp = "Stink Ray"
STRINGS.CHARACTER_NAMES.stinkrayp = "STINK RAY"
STRINGS.CHARACTER_DESCRIPTIONS.stinkrayp = "*Is flying\n*Has poisonous ranged attacks \n*Can swim"
STRINGS.CHARACTER_QUOTES.stinkrayp = "Sting ray? More like Stink... oh"

STRINGS.CHARACTER_TITLES.sharkittenp = "Sharkitten"
STRINGS.CHARACTER_NAMES.sharkittenp = "SHARKITTEN"
STRINGS.CHARACTER_DESCRIPTIONS.sharkittenp = "*Is adorable \n*Loves meat \n* Can grow by eating"
STRINGS.CHARACTER_QUOTES.sharkittenp = "MEW"

STRINGS.CHARACTER_TITLES.twisterp = "Sealnado"
STRINGS.CHARACTER_NAMES.twisterp = "SEALNADO"
STRINGS.CHARACTER_DESCRIPTIONS.twisterp = "*Is a Giant \n*Constantly vacuums items around it\n*Can move across water"
STRINGS.CHARACTER_QUOTES.twisterp = "The wind is howling..."

STRINGS.CHARACTER_TITLES.apep = "Prime Ape"
STRINGS.CHARACTER_NAMES.apep = "PRIME APE"
STRINGS.CHARACTER_DESCRIPTIONS.apep = "*Is a monkey\n*Only eats veggies \n*Can throw its own poop"
STRINGS.CHARACTER_QUOTES.apep = "OooH AAH AHH"

STRINGS.CHARACTER_TITLES.swwarriorp = "Spider Warrior"
STRINGS.CHARACTER_NAMES.swwarriorp = "SPIDER WARRIOR"
STRINGS.CHARACTER_DESCRIPTIONS.swwarriorp = "*Is a spider\n*Is bulky for a spider \n*Is poisonous"
STRINGS.CHARACTER_QUOTES.swwarriorp = "SCREECH"

STRINGS.CHARACTER_TITLES.parrotp = "Parrot"
STRINGS.CHARACTER_NAMES.parrotp = "PARROT"
STRINGS.CHARACTER_DESCRIPTIONS.parrotp = "*Is a bird \n*Can fly \n*Can talk"
STRINGS.CHARACTER_QUOTES.parrotp = "CHUMP"

STRINGS.CHARACTER_TITLES.toucanp = "Toucan"
STRINGS.CHARACTER_NAMES.toucanp = "TOUCAN"
STRINGS.CHARACTER_DESCRIPTIONS.toucanp = "*Is a bird \n*Can fly \n*Is slightly faster than other birds"
STRINGS.CHARACTER_QUOTES.toucanp = "berd"

STRINGS.CHARACTER_TITLES.seagullp = "Seagull"
STRINGS.CHARACTER_NAMES.seagullp = "SEAGULL"
STRINGS.CHARACTER_DESCRIPTIONS.seagullp = "*Is a bird \n*Can fly \n*Can swim over the ocean"
STRINGS.CHARACTER_QUOTES.seagullp = "More like seaDULL amirite?"

--------------------------------------------
STRINGS.CHARACTER_TITLES.sharxp = "Sea Hound"
STRINGS.CHARACTER_NAMES.sharxp = "SEA HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.sharxp = "*Is a sea hound\n*Can only eat meat\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.sharxp = "Mini Sharks"

STRINGS.CHARACTER_TITLES.crocodogp = "Crocodog"
STRINGS.CHARACTER_NAMES.crocodogp = "CROCODOG"
STRINGS.CHARACTER_DESCRIPTIONS.crocodogp = "*Is friends with hounds\n*Can only eat meat \n*Is amphibious but can overheat and freeze"
STRINGS.CHARACTER_QUOTES.crocodogp = "Not recommended as a pet"

STRINGS.CHARACTER_TITLES.watercrocodogp = "Blue Crocodog"
STRINGS.CHARACTER_NAMES.watercrocodogp = "BLUE CROCODOG"
STRINGS.CHARACTER_DESCRIPTIONS.watercrocodogp = "*Is friends with hounds\n*Can only eat meat \n*Is amphibious but can overheat"
STRINGS.CHARACTER_QUOTES.watercrocodogp = "Not recommended as a pet"

STRINGS.CHARACTER_TITLES.poisoncrocodogp = "Yellow Crocodog"
STRINGS.CHARACTER_NAMES.poisoncrocodogp = "BLUE CROCODOG"
STRINGS.CHARACTER_DESCRIPTIONS.poisoncrocodogp = "*Is friends with hounds\n*Is poisonous \n*Is amphibious but can freeze"
STRINGS.CHARACTER_QUOTES.poisoncrocodogp = "Not recommended as a pet"

STRINGS.CHARACTER_TITLES.dogfishp = "Dog Fish"
STRINGS.CHARACTER_NAMES.dogfishp = "DOG FISH"
STRINGS.CHARACTER_DESCRIPTIONS.dogfishp = "*Is a dog fish \n*Can only swim in the ocean \n*Can't attack..."
STRINGS.CHARACTER_QUOTES.dogfishp = "WHOSE A GOOD DOG"

STRINGS.CHARACTER_TITLES.quackenp = "Quacken"
STRINGS.CHARACTER_NAMES.quackenp = "QUACKEN"
STRINGS.CHARACTER_DESCRIPTIONS.quackenp = "*Is a Giant \n*Can spawn many tentacles and spit ink\n*Can only swim in the ocean"
STRINGS.CHARACTER_QUOTES.quackenp = "RELEASE THE QUACKEN"
-- Name as appears in-game 
STRINGS.NAMES.MONKEYHOUSE2 = "Prime Ape Hut"
STRINGS.NAMES.MONKEYBARREL2_P = "Prime Ape Hut"


STRINGS.CHARACTERS.HOGP = require "speech_hogp"
--STRINGS.CHARACTERS.PARROTP = require "speech_parrotp"

STRINGS.NAMES.SWMONSTER_WPN = "Mysterious Power"
-- The default responses of examining the character
----------------------------------------------------

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.kelpyp = "*Is bulky\n*Can be revived at the normal rate\n*Is 2x weak to fire\n\nExpertise:\nMelee, Darts, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.apep = "*Is a monkey\n*Can taunt by pressing x\n*Can throw poop to gain aggro\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.crocodogp = "*Has powerful bites, lowers foe's defense by 10%\n*Can taunt by pressing x\n*Is pretty fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.watercrocodogp = "*Has powerful bites, lowers foe's defense by 10%\n*Can wet nearby enemies with C\n*Is pretty fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.poisoncrocodogp = "*Has weaker bites than its brothers\n*Can poison enemies\n*Is pretty fast\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.sharxp = "*Is very frail, but gets revived 2x faster\n*Gains attack buffs from hound-like enemies\n*Is pretty fast\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.seagullp = "*Is very frail, but gets revived 3x faster\n*Takes 20% less damage\n*Can fly with C\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.toucanp = "*Is very frail, but gets revived 3x faster\n*Moves faster than other birds\n*Can fly with C\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.parrotp = "*Is very frail, but gets revived 2x faster\n*Is smart enough to use staves\n*Can fly with C\n\nExpertise:\nMelee, Darts, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.swwarriorp = "*Is a spider warrior\n*Takes 20% less damage\n*Can poison enemies\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.stinkrayp = "*Is flying\n*Is immune to poison\n*Can do poison AoE when unarmed\n\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.oxp = "*Is bulky\n*Inflicts wet debuff on attackers\n*Has 25% fire resistance\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.dragoonp = "*Is immune to fire\n*Has a damaging dash attack\n*Can spit with X to deal passive damage\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.merm2p = "*Is not fond of attacking\n*10% lower cooldowns, but 20% more healing dealt\n*Can inflict attackers with wet debuff\n\nExpertise:\nMelee, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.hogp = "*Has a 50% attack buff\n*Can knockback enemies with his charge\n*Revives others 25% slower\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ghost2p = "*Is immune to debuffs\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.fly2p = "*Is frail, but always revives at 100% HP\n*Heals by attacking\n*Is poisonous\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.doydoyp = "*Is very dumb\n*Enemies won't bother with them\n*300% more healing dealt\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.smalldoydoyp = "*Is very dumb and can't attack\n*Enemies won't bother with them\n*300% more damage dealt\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.palmtreep = "*Is bulky, but very weak to fire\n*Heals more when there are more plant mobs\n*Has a ranged attack with coconuts\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.crabbitp = "*Is frail, but gets revived 3x faster\n*Can hide with X\n*Inflicts wet debuff on attackers\n\nExpertise:\nMelees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.flupp = "*Is frail, but gets revived 2x faster\n*Can hide with X\n*Can force mobs to drop more items\n\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.packp = "*Changes form dependant on playstyle\n*Can use any item, but deals 50% less damage.\n*Is pretty bulky\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.whalep = "*Is bulky\n*Recieves 20% more healing\n*Deals an AOE attack\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.whale2p = "*Is bulky\n*Recieves 20% less healing, but deals 50% more damage\n*Deals an AOE attack\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ballphinp = "*Is intelligent\n*Revives other twice as fast\n*Deals 50% less damage\n\nExpertise:\nMelees, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.jellyfishp = "*Is frail, but can be revived 2x as fast\n*Shocks melee attackers\n*Can attack, but not often\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rainbowjellyfishp = "*Is frail, but can be revived 2x as fast\n*Reflects half damage taken as shock damage\n*Can attack, but not often\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.clockwork4p = "*Has 90% armor\n*Has ranged coconade attacks\n*Can't be revived\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.cactusp = "*Recieves 25% more healing\n*Damages melee attackers\n*Can only move with C\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.shadow3p = "*Is immune to buffs and debuffs\n*Has health regen, but can't be healed by aura\n*Auto-revives\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.snakep = "*Is stealthy when above 50% health\n*Is slightly faster and deals more damage\n*Can be revived 2x faster\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.snake_poisonp = "*Is stealthy when above 50% health\n*Poisons foes\n*Can be revived 2x faster\n\nExpertise:\nMelee"


STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.sharkittenp = "*Is pretty fast, but 25% less healing recieved\n*Gains a 25% attack buff when near Tigersharks\n*X will taunt nearby mobs\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.tigersharkp = "*Is a Giant, but 25% less healing recieved\n*Can jump occasionally for high damage\n*Gains a heal buff when near kittens, but loses health when they die\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.twisterp = "*Is a Giant\n*Is immune to most debuffs\n*X will do a powerful aoe attack, but lowers self defense by 80% when charging\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.quackenp = "*Is a Giant\n*Can summon tentacles, however they share health\n*Can only move with C\n\nExpertise:\nNone"
------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

--STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOME = 
{
	GENERIC = "A rabbit must've dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOLE_P = 
{
	GENERIC = "There must be a rabbit around here.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOME = 
{
	GENERIC = "A mole probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOLE_P = 
{
	GENERIC = "I better keep an eye on my rocks.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHOME = 
{
	GENERIC = "A bee probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE_P = 
{
	GENERIC = "That one seems pretty quiet.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE2_P = 
{
	GENERIC = "Looks as angry as its owner.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FROGHOME = 
{
	GENERIC = "A frog probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.POND_P = 
{
	GENERIC = "Theres no fish in there.",

}


------------------------------------------------------------------
-- Formal skin strings
------------------------------------------------------------------

--STRINGS.SKIN_QUOTES.houndplayer_1 = "The Rare Varg Hound"
--STRINGS.SKIN_NAMES.houndplayer_1 = "Varg Hound"


------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS