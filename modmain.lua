------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppsw_strings")

GLOBAL.PPSW_FORGE = require("ppsw_forge")

local TUNING = require("ppsw_tuning")
------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

GLOBAL.MOB_TIGERSHARK = GetModConfigData("Tiger Shark")
GLOBAL.MOB_DOYDOY = GetModConfigData("DoyDoy")

PrefabFiles = {
	"swmonster_wpn", 
	"merm_spear",
	---------------
	--FX--
	"quacken_tentacle_p",
	"treeguard_coconutp",
	"coconade_p",
	"warningshadow",
	"tigersharkshadowp",
	"dragoonfire",
	"dragoonspit",
	"dragoon_charge_fx",
	"knightboat_cannonshotp",
	"whale_carcassp",
	"sealp",
	"kraken_projectilep",
	"poisonbubble2p",
	-----Homes-----
	"monkeybarrel2_p",
	"dragoonhome",
	"dragoonden_p",
	"mermhut_p",
	"mermhut2_p",
	"hoghouse_p",
	--"snakehome",
	--"snakebush_p",
	"crabhome",
	"crabbithole_p",
	"sharkhome",
	"sharkittenden_p",
	"monkeyhouse2",
	-------------------------
}	

GLOBAL.PPSW_MobCharacters = {
	snakep        = { fancyname = "Snake",           gender = "MALE",   mobtype = {}, skins = {} },
	snake_poisonp = { fancyname = "Yellow Snake",           gender = "MALE",   mobtype = {}, skins = {} },
	swwarriorp        = { fancyname = "Spider Warrior (SW)",           gender = "MALE",   mobtype = {}, skins = {} },
	apep        = { fancyname = "Prime Ape",           gender = "MALE",   mobtype = {}, skins = {} },
	hogp        = { fancyname = "Wild Bore",           gender = "MALE",   mobtype = {}, skins = {} },
	merm2p        = { fancyname = "Fisher Merm",           gender = "MALE",   mobtype = {}, skins = {} },
	oxp        = { fancyname = "Ox",           gender = "MALE",   mobtype = {}, skins = {} },
	ghost2p        = { fancyname = "Pirate Ghost",           gender = "MALE",   mobtype = {}, skins = {} },
	flupp        = { fancyname = "Flup",           gender = "MALE",   mobtype = {}, skins = {} },
	dragoonp        = { fancyname = "Dragoon",           gender = "MALE",   mobtype = {}, skins = {} },
	smalldoydoyp        = { fancyname = "Baby DoyDoy",           gender = "FEMALE",   mobtype = {}, skins = {} },
	doydoyp        = { fancyname = "DoyDoy",           gender = "FEMALE",   mobtype = {}, skins = {} },
	dragoonp        = { fancyname = "Dragoon",           gender = "MALE",   mobtype = {}, skins = {} },
	cactusp        = { fancyname = "Elephant Cactus",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true },
	crabbitp        = { fancyname = "Crab",           gender = "NEUTRAL",   mobtype = {}, skins = {} },
	crocodogp        = { fancyname = "Crocodog",           gender = "NEUTRAL",   mobtype = {}, skins = {} },
	watercrocodogp        = { fancyname = "Blue Crocodog",           gender = "NEUTRAL",   mobtype = {}, skins = {} },
	poisoncrocodogp        = { fancyname = "Yellow Crocodog",           gender = "NEUTRAL",   mobtype = {}, skins = {} },
	stinkrayp        = { fancyname = "Stinkray",           gender = "MALE",   mobtype = {}, skins = {}, forge = true },
	fly2p        = { fancyname = "Poison Mosquito",           gender = "MALE",   mobtype = {}, skins = {}, forge = true },
	packp        = { fancyname = "Packim",           gender = "MALE",   mobtype = {}, skins = {} },
	palmtreep        = { fancyname = "Palm Tree Guard",           gender = "MALE",   mobtype = {}, skins = {} },
	parrotp        = { fancyname = "Parrot",           gender = "MALE",   mobtype = {}, skins = {} },
	toucanp        = { fancyname = "Toucan",           gender = "MALE",   mobtype = {}, skins = {} },
	seagullp        = { fancyname = "Seagull",           gender = "MALE",   mobtype = {}, skins = {} },
	shadow3p        = { fancyname = "Swimming Horror",           gender = "NEUTRAL",   mobtype = {}, skins = {} },
	twisterp        = { fancyname = "Sealnado",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	tigersharkp        = { fancyname = "Tiger Shark",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true },
	sharkittenp        = { fancyname = "Sharkitten",           gender = "FEMALE",   mobtype = {}, skins = {} },
	--Ocean only--
	dogfishp        = { fancyname = "Dog Fish",           gender = "MALE",   mobtype = {}, skins = {}},
	swordfishp        = { fancyname = "Sword Fish",           gender = "MALE",   mobtype = {}, skins = {}},
	jellyfishp        = { fancyname = "Jellyfish",           gender = "MALE",   mobtype = {}, skins = {}},
	rainbowjellyfishp        = { fancyname = "Rainbow Jellyfish",           gender = "MALE",   mobtype = {}, skins = {}},
	ballphinp        = { fancyname = "Ballphin",           gender = "MALE",   mobtype = {}, skins = {} },
	sharxp        = { fancyname = "Sea Hound",           gender = "MALE",   mobtype = {}, skins = {} },
	whalep        = { fancyname = "Blue Whale",           gender = "MALE",   mobtype = {}, skins = {} },
	whale2p        = { fancyname = "White Whale",           gender = "MALE",   mobtype = {}, skins = {}},
	clockwork4p        = { fancyname = "Floaty Boaty Knight",           gender = "ROBOT",   mobtype = {}, skins = {}},
	quackenp        = { fancyname = "Quacken",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}},
	kelpyp        = { fancyname = "Kelpy",           gender = "MALE",   mobtype = {}, skins = {}},
	jungletreeguardp        = { fancyname = "Jungle Tree Guard",           gender = "MALE",   mobtype = {}, skins = {}},

}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPSW_Character_Order = {
	"kelpyp",
	"snakep",
	"snake_poisonp",
	"crabbitp",
	"parrotp",
	"toucanp",
	"seagullp",
	"apep",
	"swwarriorp",
	"hogp",
	"packp",
	"merm2p",
	"palmtreep",
	"jungletreeguardp",
	"oxp",
	"jellyfishp",
	"rainbowjellyfishp",
	"crocodogp",
	"watercrocodogp",
	"poisoncrocodogp",
	"stinkrayp",
	"dragoonp",
	"smalldoydoyp",
	"doydoyp",
	"ghost2p",
	"flupp",
	"fly2p",
	"cactusp",
	"sharxp",
	"dogfishp",
	"swordfishp",
	"ballphinp",
	"whalep",
	"whale2p",
	"clockwork4p",
	"shadow3p",
	"twisterp",
	"sharkittenp",
	"tigersharkp",
	"quackenp",
}

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {

-----------------------------------------------------
	--Asset("SOUNDPACKAGE", "sound/dontstarve_DLC001.fev"), 
	Asset("SOUNDPACKAGE", "sound/dontstarve_DLC002.fev"), --make sure to update this every time SW updates.
	Asset( "SOUND", "sound/dontstarve_shipwreckedSFX.fsb"), 
	--------------------------------------------------------------
	Asset("ANIM", "anim/ghost_monster_build.zip"), 
	Asset("ANIM", "anim/brain_coral.zip"), 
	Asset("ANIM", "anim/brain_coral_rock.zip"), 
-----------------------------HOMES--------------------------------
    Asset( "IMAGE", "images/inventoryimages/hoghome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/hoghome.xml" ),	
	Asset( "IMAGE", "images/inventoryimages/merm2home.tex" ),
    Asset( "ATLAS", "images/inventoryimages/merm2home.xml" ),
	Asset( "IMAGE", "images/inventoryimages/snakehome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/snakehome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/sharkhome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/sharkhome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/apehome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/apehome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/crabhome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/crabhome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/dragoonhome.tex" ),
    Asset( "ATLAS", "images/inventoryimages/dragoonhome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/merm_spear.tex" ),
    Asset( "ATLAS", "images/inventoryimages/merm_spear.xml" ),	
-----------------------------------------------------
	--Something Different Set: --TODO place them in their proper prefab files
	Asset("ANIM", "anim/whale_moby_shiny_build_01.zip"),
	Asset("ANIM", "anim/rainbowjellyfish_shiny_build_01.zip"),
	Asset("ANIM", "anim/cactus_volcano_shiny_build_01.zip"),
	Asset("ANIM", "anim/junglekiki_shiny_build_01.zip"),
	Asset("ANIM", "anim/ballphin_shiny_build_01.zip"),
	Asset("ANIM", "anim/knightboat_shiny_build_01.zip"),
	Asset("ANIM", "anim/crabbit_shiny_build_01.zip"),
	Asset("ANIM", "anim/dogfish_shiny_build_01.zip"),
	Asset("ANIM", "anim/doydoy_shiny_build_01.zip"),
	Asset("ANIM", "anim/dragoon_shiny_build_01.zip"),
	Asset("ANIM", "anim/flup_shiny_build_01.zip"),
	Asset("ANIM", "anim/mosquito_yellow_shiny_build_01.zip"),
	Asset("ANIM", "anim/ghost_pirate_shiny_build_01.zip"), --bugged, don't use.
	Asset("ANIM", "anim/wildbore_shiny_build_01.zip"),
	Asset("ANIM", "anim/jellyfish_shiny_build_01.zip"),
	Asset("ANIM", "anim/merm_fisherman_shiny_build_01.zip"),
	Asset("ANIM", "anim/ox_shiny_build_01.zip"),
	Asset("ANIM", "anim/packim_shiny_build_01.zip"),
	Asset("ANIM", "anim/packim_fat_shiny_build_01.zip"),
	Asset("ANIM", "anim/packim_fire_shiny_build_01.zip"),
	Asset("ANIM", "anim/palmtreeguard_shiny_build_01.zip"),
	Asset("ANIM", "anim/parrot_shiny_build_01.zip"),
	Asset("ANIM", "anim/quacken_tentacle_shiny_build_01.zip"),
	Asset("ANIM", "anim/quacken_shiny_build_01.zip"),
	Asset("ANIM", "anim/seagull_shiny_build_01.zip"),
	Asset("ANIM", "anim/sharkitten_shiny_build_01.zip"),
	Asset("ANIM", "anim/sharx_shiny_build_01.zip"),
	Asset("ANIM", "anim/doydoy_baby_shiny_build_01.zip"),
	Asset("ANIM", "anim/doydoy_teen_shiny_build_01.zip"),
	Asset("ANIM", "anim/snake_shiny_build_01.zip"),
	Asset("ANIM", "anim/snake_yellow_shiny_build_01.zip"),
	Asset("ANIM", "anim/stinkray_shiny_build_01.zip"),
	Asset("ANIM", "anim/swordfish_shiny_build_01.zip"),
	Asset("ANIM", "anim/spider_tropical_shiny_build_01.zip"),
	Asset("ANIM", "anim/tigershark_shiny_build_01.zip"),
	Asset("ANIM", "anim/tigershark_water_shiny_build_01.zip"),
	Asset("ANIM", "anim/toucan_shiny_build_01.zip"),
	Asset("ANIM", "anim/twister_shiny_build_01.zip"),
	Asset("ANIM", "anim/whale_moby_shiny_build_01.zip"),
	Asset("ANIM", "anim/whale_blue_shiny_build_01.zip"),
	Asset("ANIM", "anim/whale_moby_carcass_shiny_build_01.zip"),
	Asset("ANIM", "anim/whale_carcass_shiny_build_01.zip"),
	Asset("ANIM", "anim/crocodog_shiny_build_01.zip"),
	Asset("ANIM", "anim/crocodog_water_shiny_build_01.zip"),
	Asset("ANIM", "anim/crocodog_poison_shiny_build_01.zip"),
	
	--Pirate Set:
	Asset("ANIM", "anim/snake_shiny_build_02.zip"),
	Asset("ANIM", "anim/snake_yellow_shiny_build_02.zip"),
	
	--Shadow Set:
	Asset("ANIM", "anim/sharx_shiny_build_03.zip"),
	
	--Magmatic Set:
	Asset("ANIM", "anim/tigershark_shiny_build_08.zip"),
	Asset("ANIM", "anim/tigershark_water_shiny_build_08.zip"),
	Asset("ANIM", "anim/quacken_tentacle_shiny_build_08.zip"),
	Asset("ANIM", "anim/quacken_shiny_build_08.zip"),
	
	--Community:
	Asset("ANIM", "anim/crocodog_poison_shiny_build_06.zip"),
	
}



local Ingredient = GLOBAL.Ingredient
local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------
local Ingredient = GLOBAL.Ingredient
local AllRecipes = GLOBAL.AllRecipes

local hogrecipe = AddRecipe("hoghouse_p", {Ingredient("boards", 4),Ingredient("cutreeds", 9)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "hoghouse_p_placer", nil, nil, 1, "hogplayer", "images/inventoryimages/hoghome.xml", "hoghome.tex")
local merm2recipe = AddRecipe("mermhut_p", {Ingredient("boards", 4),Ingredient("cutreeds", 9)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "mermhut_p_placer", nil, nil, 1,"mermplayer", "images/inventoryimages/merm2home.xml", "merm2home.tex" )
local merm3recipe = AddRecipe("mermhut2_p", {Ingredient("boards", 4),Ingredient("cutreeds", 9)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "mermhut2_p_placer", nil, nil, 1,"mermfisher", "images/inventoryimages/merm2home.xml", "merm2home.tex" )
local spearrecipe = AddRecipe("merm_spear", {Ingredient("twigs", 6),Ingredient("rope", 1)}, GLOBAL.RECIPETABS.TOOLS, GLOBAL.TECH.NONE, nil, nil, nil, nil, "mermfisher", "images/inventoryimages/merm_spear.xml", "merm_spear.tex")

if GLOBAL.MOBHOUSE == "Enable2" or GLOBAL.MOBHOUSE == "Enable3" then
	local dragoonrecipe = AddRecipe("dragoonden_p", {Ingredient("cutstone", 8),Ingredient("monstermeat", 5)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "dragoonden_p_placer", nil, nil, 1, nil, "images/inventoryimages/dragoonhome.xml", "dragoonhome.tex" )
	local sharkrecipe = AddRecipe("sharkittenden_p", {Ingredient("turf_desertdirt", 9), Ingredient("fish", 6), Ingredient("shovel", 1)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "sharkittenden_p_placer", nil, nil, 1, nil, "images/inventoryimages/sharkhome.xml", "sharkhome.tex" )
	local crabrecipe = AddRecipe("crabbithole_p", {Ingredient("shovel", 1), Ingredient("fish", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "crabbithole_p_placer", nil, nil, 1, nil, "images/inventoryimages/crabhome.xml", "crabhome.tex" )
	local aperecipe = AddRecipe("monkeybarrel2_p", {Ingredient("poop", 6), Ingredient("twigs", 20), Ingredient("cutgrass", 10)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "monkeybarrel2_p_placer", nil, nil, 1, nil, "images/inventoryimages/apehome.xml", "apehome.tex" )
	--local snakerecipe = AddRecipe("snakebush_p", {Ingredient("twigs", 10), Ingredient("cutgrass", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "snakebush_p_placer", nil, nil, 1,nil, "images/inventoryimages/snakehome.xml", "snakehome.tex" )
end

spearrecipe.sortkey = -0.1
merm2recipe.sortkey = -6.0
merm3recipe.sortkey = -5.9
hogrecipe.sortkey = -6.1

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------
--[[
AddComponentPostInit("playeractionpicker", function(playeractionpicker)
	local old_GetRightClickActions = playeractionpicker.GetRightClickActions
	
	function playeractionpicker:GetRightClickActions(position, target)
		--for some reason, this only passes correctly once. Even though the position is always correct in this function, the action position always uses the first position used.
		if (actions == nil or #actions <= 0) and (target == nil or target:HasTag("walkableplatform")) and self.inst.can_aoe_anywhere then
			actions = self:GetPointSpecialActions(position, nil, true)
		end
		if actions == nil then
			return old_GetRightClickActions(self, position, target)
		else
			return actions
		end
	end
end)
]]

AddAction("TIGERSHARK_JUMP", "Jump to", function(act)
	local act_pos = act:GetActionPoint() or nil
	print(act_pos.x..", "..act_pos.z)
	if act.doer ~= nil and act.doer:HasTag("player") and act.doer.can_jump and act.doer.can_jump == true and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer.sg:GoToState(act.doer:HasTag("aquatic") and "dive" or "jump_attack", act.doer.jump_pos or act_pos)
		return true
	else
		print("ACTION FAILED")
		return false
	end
end)

GLOBAL.ACTIONS.TIGERSHARK_JUMP.distance = 40 
GLOBAL.ACTIONS.TIGERSHARK_JUMP.rmb = true 
-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppsw_puppets")
local MobSkins = require("ppsw_skins")
PlayablePets.RegisterPuppetsAndSkins(PPSW_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

-------------------------

AddComponentPostInit("minionspawner", function(minionspawner, inst)

 --minionspawner.oldupdatefn = hatchable.OnUpdate
 local function generatefreepositions(max)
	local pos_table = {}
	for num = 1, max do
		table.insert(pos_table, num)
	end
	return pos_table
end
 
 function minionspawner:RegenerateFreePositions()
	self.freepositions = generatefreepositions(self.maxminions * 1.2) --* 1.2
end

function minionspawner:ForceSpawnLocations() 
	local x, y, z = self.inst.Transform:GetWorldPosition()
    local ground = GLOBAL.TheWorld
    local maxpositions = self.maxminions * 1.2
    local useablepositions = {}
    for i = 1, 100 do
        local s = i / 32--(num/2) -- 32.0
        local a = math.sqrt(s * 512)
        local b = math.sqrt(s) * self.distancemodifier
        local pos = GLOBAL.Vector3(x + math.sin(a) * b, 0, z + math.cos(a) * b)
		if GLOBAL.TheNet:GetServerGameMode() ~= "lavaarena" then
		
			if not GLOBAL.TheWorld.Map:IsVisualGroundAtPoint(pos.x, 0, pos.z) and not GLOBAL.TheWorld.Map:GetPlatformAtPoint(pos.x, 0, pos.z) and
				--self:CheckTileCompatibility(ground.Map:GetTileAtPoint(pos:Get())) and
				--ground.Pathfinder:IsClear(x, 0, z, pos.x, 0, pos.z, { ignorewalls = true }) and
				#TheSim:FindEntities(pos.x, pos.y, pos.z, 7, { "tentacle" }) <= 0 then
				table.insert(useablepositions, pos)
				if #useablepositions >= maxpositions then
					return useablepositions 
				end
			end
		
		else
		
			if ground.Map:IsAboveGroundAtPoint(pos:Get()) and
				--self:CheckTileCompatibility(ground.Map:GetTileAtPoint(pos:Get())) and
				--ground.Pathfinder:IsClear(x, 0, z, pos.x, 0, pos.z, { ignorewalls = true }) and
				#TheSim:FindEntities(pos.x, pos.y, pos.z, 7, { "tentacle" }) <= 0 and
				not ground.Map:IsPointNearHole(pos) then
					table.insert(useablepositions, pos)
					if #useablepositions >= maxpositions then
						return useablepositions 
					end
			end
		end
    end
	
	
	--if it couldn't find enough spots for minions.
	--self.maxminions = #useablepositions
    self.freepositions = generatefreepositions(self.maxminions)
    return #useablepositions > 0 and useablepositions or nil
end

function minionspawner:ForceNewMinion(force)

	--if not self.minionpositions then
		self.minionpositions = self:ForceSpawnLocations()
	--end

	if (force or self.shouldspawn) and not self:MaxedMinions() and #self.freepositions > 0 then
		self.spawninprogress = false
		local minion = self:MakeMinion()
		if minion then
			if self.inst.isshiny ~= nil and self.inst.isshiny ~= 0 then
				minion.isshiny = self.inst.isshiny
				minion.AnimState:SetBuild("quacken_tentacle_shiny_build_0"..self.inst.isshiny)
				--This function will likely on be used on tentacles anyways.
			end
			minion.sg:GoToState("spawn")
			self:TakeOwnership(minion)
			local pos = self:GetSpawnLocation(minion.minionnumber)
			if pos then
				minion.Transform:SetPosition(pos.x, pos.y, pos.z)
				self:RemovePosition(minion.minionnumber)
				
				if self.onspawnminionfn then
					self.onspawnminionfn(self.inst, minion)
				end
			else
				--self.minionpositions = self:ForceSpawnLocations()
				--self:RemovePosition(minion.minionnumber)
				--print("Debug: No position, so I'm spawning somewhere random around master!")
				--With the new RoT update just make the tentacle itself deal with 
				local posx, y, posz = self.inst.Transform:GetWorldPosition()
				local newpos = {x = posx + math.random(-35, 35) , 0, z = posz + math.random(-35, 35)}
				if GLOBAL.TheNet:GetServerGameMode() ~= "lavaarena" and not GLOBAL.TheWorld.Map:IsVisualGroundAtPoint(newpos.x, 0, newpos.z) and not GLOBAL.TheWorld.Map:GetPlatformAtPoint(newpos.x, 0, newpos.z) then
					minion.Transform:SetPosition(newpos.x , 0, newpos.z) 
				elseif GLOBAL.TheNet:GetServerGameMode() == "lavaarena" and GLOBAL.TheWorld.Map:IsVisualGroundAtPoint(newpos.x, 0, newpos.z) and GLOBAL.TheWorld.Map:GetPlatformAtPoint(newpos.x, 0, newpos.z) then
					minion.Transform:SetPosition(newpos.x , 0, newpos.z) 
				else
					print("Quacken Debug: Invalid ground, removing tentacle")
					minion:Remove()
				end
				if self.onspawnminionfn then
					self.onspawnminionfn(self.inst, minion)
				end
			end
		end

		if not self:MaxedMinions() and self.shouldspawn then
			self:StartNextSpawn()
		end
	end
end

function minionspawner:DespawnAll()
	self.spawninprogress = false
	--self.minionpositions = nil
	for k,v in pairs(self.minions) do
		v:DoTaskInTime(math.random(), function() 
			if v:IsAsleep() then
				v:Remove()
			else
				v.sg:GoToState("despawn")
				v:PushEvent("despawn") 
				v:ListenForEvent("entitysleep", function() v:Remove() end)
			end
		end)
	end
	self.numminions = 0
	self.minionpositions = nil
	self.shouldspawn = false
	self.inst.isactive = nil
	self.minions = {}
end

function minionspawner:SpawnAll()
	self.inst.isactive = true
	for i = 1, self.maxminions do
		self.inst:DoTaskInTime(math.random(2,3) * math.random(), function()
			self:ForceNewMinion(true)
		end)
	end
end


end)
-----------------------------------
--Monkey Barrel's slots--
_G = GLOBAL
local params={} 
params.monkeybarrel2_p =
{
    widget =
    {
        slotpos = {},
        --animbank = "ui_chester_shadow_3x4",
        --animbuild = "ui_chester_shadow_3x4",
        pos = _G.Vector3(350, 350, 0),
        side_align_tip = 80, --160
    },
    type = "chest",
}
-- y and x make the slots. 0 counts as a row.
for y = 0, 5 do --6.5, -0.5, -1
    for x = 0, 5 do
        table.insert(params.monkeybarrel2_p.widget.slotpos, _G.Vector3(75 * x - 75 * 12 + 75, 75 * y - 75 * 6 + 75, 0))
    end
end

local containers = _G.require "containers"
containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, params.monkeybarrel2_p.widget.slotpos ~= nil and #params.monkeybarrel2_p.widget.slotpos or 0)
local old_widgetsetup = containers.widgetsetup
function containers.widgetsetup(container, prefab, data)
        local pref = prefab or container.inst.prefab
        if pref == "monkeybarrel2_p" then
                local t = params[pref]
                if t ~= nil then
                        for k, v in pairs(t) do
                                container[k] = v
                        end
                        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
                end
        else
                return old_widgetsetup(container, prefab)
    end
end




------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPSW_Character_Order) do
	local mob = GLOBAL.PPSW_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPSW_Character_Order) do
	local mob = GLOBAL.PPSW_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end